<?php

namespace App2Bundle\Repository\Bilans;

/**
 * BilanHabillagepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class BilanHabillagepository extends \Doctrine\ORM\EntityRepository
{
    public function findWithResultats($id)
    {    
        $query = $this->createQueryBuilder('b')
                    ->where('b.id = :identifier')
                    ->setParameter('identifier', $id)
                    ->innerJoin('b.resultats', 'r')
                    ->addSelect('r')
                    ->innerJoin('r.itemTest', 'i' )
                    ->addSelect('i');

        $resultat = $query->getQuery()->getResult();
        return $resultat;
    }
}
