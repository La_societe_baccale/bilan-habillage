<?php

namespace App2Bundle\Form\BilansCustom;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ItemTestType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nom', TextType::class, array(
            'label' => "Nom de l'item testé",
        ))
        ->add('typeValue', ChoiceType::class, array(
            'label' => "Quel est le format de réponse",
            'choices'  => [
                'nombre entier' => "integer",
                'texte' => "text",
            ],
            'required' => true,
            'attr' => [
                'class' => 'format-value-item'
            ],
        ))
        ->add('min', IntegerType::class, array(
            'label' => "Quel est la valeur minimum?",
            'required' => false,
            'attr' => [
                'class' => 'min-max-item min-item'
            ],
        ))
        ->add('max', IntegerType::class, array(
            'label' => "Quel est la valeur maximum?",
            'required' => false,
            'attr' => [
                'class' => 'min-max-item max-item'
            ],
        ))
        ->add('ordre', HiddenType::class, array(
        ))
           ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App2Bundle\Entity\BilansCustom\ItemTest'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app2bundle_item_test';
    }


}
