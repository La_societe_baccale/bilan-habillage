<?php

namespace App2Bundle\Form\BilansCustom;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App2Bundle\Form\BilansCustom\ItemTestType;
use App2Bundle\Form\BilansCustom\SubTotalType;
use App2Bundle\Form\BilansCustom\SubTextType;
use App2Bundle\Form\BilansCustom\SubPartType;


class BilanCustomSkeletonType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, array(
                'label' => "Nom du nouveau bilan",
            ))
            ->add('type_values', ChoiceType::class, array(
                'mapped' => false,
                'label' => "Les réponses des items testés sont-elle toutes du même format",
                'choices'  => [
                    'oui' => true,
                    'non' =>  false,
                ],
                'data' => false,
            ))
            ->add('format_values', ChoiceType::class, array(
                'mapped' => false,
                'label' => "Quel est le format de réponse",
                'choices'  => [
                    'nombre entier' => "integer",
                    'texte' => "text",
                ],
                'required' => false,
                'attr' => [
                    'class' => 'format-values'
                ],
            ))
            ->add('min', IntegerType::class, array(
                'label' => "Quel est la valeur minimum?",
                'required' => false,
                'mapped' => false,
                'attr' => [
                    'class' => 'min-max-all-items min-all-items'
                ],
            ))
            ->add('max', IntegerType::class, array(
                'label' => "Quel est la valeur maximum?",
                'required' => false,
                'mapped' => false,
                'attr' => [
                    'class' => 'min-max-all-items max-all-items'
                ],
            ))
            ->add('withCote', ChoiceType::class, array(
                'label' => "Les items doivent-ils être testé à gauche et à droite?",
                'choices'  => [
                    'oui' => true,
                    'non' =>  false,
                ],
            ))
            ->add('itemTests', CollectionType::class, array(
                'label' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => ItemTestType::class,
                'entry_options' => [
                    'label' => false,
                    'attr' => ['class' => 'item-box item-subtotal-box'],
                ],
                'prototype' => true
            ))
            ->add('subtotals', CollectionType::class, array(
                'label' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => SubTotalType::class,
                'entry_options' => [
                    'label' => false,
                    'attr' => ['class' => 'subtotal-box item-subtotal-box'],
                ],
                'prototype' => true
            ))
            ->add('subparts', CollectionType::class, array(
                'label' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => SubPartType::class,
                'entry_options' => [
                    'label' => false,
                    'attr' => ['class' => 'subpart-box item-subtotal-box'],
                ],
                'prototype' => true
            ))
            ->add('subtexts', CollectionType::class, array(
                'label' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => SubTextType::class,
                'entry_options' => [
                    'label' => false,
                    'attr' => ['class' => 'subtext-box item-subtotal-box'],
                ],
                'prototype' => true
            ))
            ;
            
           ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App2Bundle\Entity\BilansCustom\BilanCustomSkeleton'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app2bundle_bilan_custom_skeleton';
    }


}
