<?php

namespace App2Bundle\Form\BilansCustom;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\CallbackTransformer;
use App2Bundle\Entity\Tests\GesteTest;
use Doctrine\ORM\EntityManagerInterface;


class ResultatCustomType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('value', TextType::class, array(
                "label" => false
            ))
            ->add('cote', ChoiceType::class, [
                "label" => false,
                "required" => false,
                'attr' => [
                    'class' => 'resultat-cote'
                ],
                'choices'  => [
                    '1' => 1,
                    '0' => 0,
                ],
            ])
            ->add('itemTest', EntityType::class, [
                'class' => 'App2Bundle:BilansCustom\ItemTest',
                'choice_label' => 'id',
                'label' => false,
                'attr' => [
                    'class' => 'resultat-item-test'
                ],
            ]);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App2Bundle\Entity\BilansCustom\ResultatCustom'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app2bundle_resultats_custom';
    }


}
