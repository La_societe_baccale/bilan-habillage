<?php

namespace App2Bundle\Form\BilansCustom;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App2Bundle\Form\BilansCustom\ItemTestType;
use App2Bundle\Form\BilansCustom\SubTotalType;
use App2Bundle\Form\BilansCustom\SubTextType;
use App2Bundle\Form\BilansCustom\SubPartType;


class BilanCustomType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('resultats', CollectionType::class, array(
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference'=>false,
            'entry_type' => ResultatCustomType::class,
            'prototype' => true
        ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App2Bundle\Entity\BilansCustom\BilanCustom'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app2bundle_bilan_custom';
    }


}
