<?php

namespace App2Bundle\Form\Bilans;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App2Bundle\Form\Resultats\ResultatHabillageType;


class BilanHabillageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('resultats', CollectionType::class, array(
            'allow_add' => true,
            'by_reference'=>false,
            'entry_type' => ResultatHabillageType::class,
            'prototype' => true
        ));
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App2Bundle\Entity\Bilans\BilanHabillage'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app2bundle_bilans_bilanhabillage';
    }


}
