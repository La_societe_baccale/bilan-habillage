<?php

namespace App2Bundle\Form\Bilans;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App2Bundle\Form\Resultats\ResultatMusculaireType;


class BilanMusculaireType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('resultats', CollectionType::class, array(
            'allow_add' => true,
            'by_reference'=>false,
            'entry_type' => ResultatMusculaireType::class,
            'prototype' => true
        ));
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App2Bundle\Entity\Bilans\BilanMusculaire'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app2bundle_bilans_bilanmusculaire';
    }


}
