<?php

namespace App2Bundle\Form\Bilans;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App2Bundle\Form\Resultats\ResultatArticulaireType;
use App2Bundle\Entity\Bilans\BilanHabillage;
use App2Bundle\Entity\Bilans\BilanGesteTest;
use App2Bundle\Entity\Bilans\Bilan;
use Doctrine\ORM\EntityManagerInterface;




class BilanForCreateBeforeType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


        
    /**
     * {@inheritdoc}
     */ 
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //Options contient id_patient pour aller chercher les bilans précédents du patient qui ont déjà été fait
        //Options contient liste_bilans qui sont les type de bilans sous forme d'array ["geste-test","musculaire"] qui ont une implication potentielle sur ce bilan
        //On va chercher tous les bilans globaux du patients
        $bilans = $this->entityManager
            ->getRepository(Bilan::class)
            ->findBy(array('patient'=> $options["id_patient"]));
        
        //Si le bilan habillage fait partie des bilan avec des implication from 
        if(in_array("habillage",$options["liste_bilans"])){
            //On va chercher tous les bilans habillage du patient
            $bilansHabillage = $this->entityManager
                    ->getRepository(BilanHabillage::class)
                    ->findBy(array('patient'=> $options["id_patient"]));
            //On s'occupe ensuite des bilans habillage contenus dans des bilans globaux, pour chaque bilan global on regarde s'il dispose d'un bilan habillage
            //Si oui on l'ajoute à la liste bilanGlobauxWithHabillage
            $bilanGlobauxWithHabillage = array();
            foreach ($bilans as $bilan) {
                    $test = $bilan->getBilanHabillage();
                    if(!is_null($test)){
                        array_push($bilanGlobauxWithHabillage,$test);
                    }
                }
            //On crées l'array choiceHabillage qui va proposer deux types de bilans habillage, ceux venant d'un bilan global, 
            //ainsi que ceux venant d'un bilan habillage autre
            // chaque bilan sera sauvegardé sous la forme nomdubilan => idbilan
            $choicesHabillage = array(
                "Bilan Globaux" => array(),
                "Autres" => array()
            );

            foreach ($bilansHabillage as $bilan) {
                $choicesHabillage["Autres"] = array_merge($choicesHabillage["Autres"],array($bilan->getNom()=> $bilan->getId()));
            }
            foreach ($bilanGlobauxWithHabillage as $bilan) {
                    $choicesHabillage["Bilan Globaux"] = array_merge($choicesHabillage["Bilan Globaux"],array($bilan->getNom()=> $bilan->getId()));
            }
            //Si l'utilisateur n'a aucun ancuen bilan habillage à afficher on affiche pas l'input 
            if(count($choicesHabillage["Bilan Globaux"]) > 0 || count($choicesHabillage["Autres"]) > 0){
                $builder->add('bilan_habillage', ChoiceType::class, array(
                        'choices'  => $choicesHabillage,
                        'expanded' => false,
                        'multiple' => false,
                        'label' => "Bilan Habillage"
                        ))
                //La checkbox pour ne pas utiliser de bilans habillage 
                ->add('ignorer_habillage', CheckboxType::class, array(
                            'label' => "Ne pas utiliser de bilans habillage",
                            'required'=> false
                ));
            }
            //Pas de checkbox si pas de bilan à présenter
            // else{
            //     $builder
            //         //La checkbox pour ne pas utiliser de bilans habillage est à true obligatoirement
            //         ->add('ignorer_habillage', CheckboxType::class, array(
            //                     'label' => "Ne pas utiliser de bilans habillage (il n'y a pas de bilan disponible pour ce patient)",
            //                     'required'=> false,
            //                     'data'=> true
            //         ));
            // }
        }
        if(in_array("geste-test",$options["liste_bilans"])){
            $bilansGesteTest = $this->entityManager
                    ->getRepository(BilanGesteTest::class)
                    ->findBy(array('patient'=> $options["id_patient"]));
            $bilanGlobauxWithGesteTest = array();
            foreach ($bilans as $bilan) {
                    $test = $bilan->getBilanGesteTest();
                    if(!is_null($test)){
                        array_push($bilanGlobauxWithGesteTest,$test);
                    }
                }
            $choicesGesteTest = array(
                "Bilan Globaux" => array(),
                "Autres" => array()
            );
            foreach ($bilansGesteTest as $bilan) {
                $choicesGesteTest["Autres"] = array_merge($choicesGesteTest["Autres"],array($bilan->getNom()=> $bilan->getId()));
            }
            foreach ($bilanGlobauxWithGesteTest as $bilan) {
                    $choicesGesteTest["Bilan Globaux"] = array_merge($choicesGesteTest["Bilan Globaux"],array($bilan->getNom()=> $bilan->getId()));
            }
            if(count($choicesGesteTest["Bilan Globaux"]) > 0 || count($choicesGesteTest["Autres"]) > 0){
                $builder
                ->add('bilan_geste_test', ChoiceType::class, array(
                            'choices'  => $choicesGesteTest,
                            'expanded' => false,
                            'multiple' => false,
                            'label' => "Bilan Geste Test"
                            ))
                    ->add('ignorer_geste_test', CheckboxType::class, array(
                                'label' => "Ne pas utiliser de bilans geste-test",
                                'required'=> false
                    ));
            }
            //Pas de checkbox si pas de bilan à présenter
            // else{
            //     $builder
            //         ->add('ignorer_geste_test', CheckboxType::class, array(
            //                     'label' => "Ne pas utiliser de bilans geste-test (il n'y a pas de bilan disponible pour ce patient)",
            //                     'required'=> false,
            //                     'data'=> true
            //         ));
            // }
            
        }
       $builder->add('creer', SubmitType::class, array('label' => 'Créer'));

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        //Options contient id_patient pour aller chercher les bilans précédents du patient qui ont déjà été fait
        //Options contient liste_bilans qui sont les type de bilans sous forme d'array ["geste-test","musculaire"] qui ont une implication potentielle sur ce bilan
        $resolver->setDefaults(array(
            'liste_bilans' => [],
            'id_patient' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app2bundle_bilans_create_before';
    }


}
