<?php

namespace App2Bundle\Form\Resultats;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\CallbackTransformer;
use App2Bundle\Entity\Tests\Articulaire;
use App2Bundle\Entity\Causes\Cause;
use App2Bundle\Form\Causes\CauseType;
use App2Bundle\Entity\Tests\GesteTest;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;



class ResultatArticulaireType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function causeChoice()
    {
        $causes = $this->entityManager->getRepository(Cause::class)->findAll();
        $choices = array();
        foreach ($causes as $cause) {
            $choices[$cause->getNom()] = $cause->getId(); 
        }
        return $choices;
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('remarque', TextType::class, array())
            ->add('value', IntegerType::class, array())
            // ->add('value', ChoiceType::class, array(
            //     'choices'  => array(
                    //    0 => 0,
            //         1 => 1,
            //         2 => 2,
            //         2+ => 3,
            //         3 => 4,
            //         3+ => 5,
            //         4 => 6,
            //         4+ => 7,
            //         5 => 7,
            //     ),
            //     'expanded' => false,
            //     'multiple' => false
            // ))
            ->add('cote', TextType::class)
            ->add('itemTest', TextType::class)
            ->add('causes', CollectionType::class, array(
                'allow_add' => true,
                'by_reference'=>false,
                'prototype' => true,
                'entry_type' => EntityType::class,
                'entry_options'=> array(
                    'class' => 'App2Bundle:Causes\Cause',
                    'choice_label' => 'nom'
                )
            ));
            // ->add('causes', EntityType::class, array(
            //             // 'mapped' => false,
            //                             // 'by_reference'=>false,

            //             // 'allow_add' => true,
            //             'class' => 'App2Bundle:Causes\Cause',
            //             'choice_label' => 'nom',
            //             'multiple'=> true
            //     )
            // );

            $builder->get('cote')
                ->addViewTransformer(new CallbackTransformer(
                    function ($normalizedFormat) {
                        return $normalizedFormat;
                    },
                    function ($submittedFormat) {
                        return ( $submittedFormat === '0' ) ? false : true;
                    }
                ));
            $builder->get('itemTest')
                ->addViewTransformer(new CallbackTransformer(
                    function ($normalizedFormat) {
                        return $normalizedFormat;
                    },
                    function ($submittedFormat) {
                        return $this->entityManager->getRepository(Articulaire::class)
                        ->find($submittedFormat);
                    }
                ));
            $builder->get('causes')
                ->addViewTransformer(new CallbackTransformer(
                    function ($normalizedFormat) {
                        return $normalizedFormat;
                    },
                    function ($submittedFormat) {
                        // $person = $em->getReference("Person", $id);

                        $result = new ArrayCollection();
                        foreach ($submittedFormat as $data) {
                            if($data != ""){
                                $result->add($data);
                            }   
                        }
                        return $result;
                    }
                ));

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App2Bundle\Entity\Resultats\ResultatArticulaire'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app2bundle_resultats_resultatarticulaire';
    }


}
