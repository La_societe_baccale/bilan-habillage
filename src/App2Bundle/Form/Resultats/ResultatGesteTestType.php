<?php

namespace App2Bundle\Form\Resultats;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\CallbackTransformer;
use App2Bundle\Entity\Tests\GesteTest;
use Doctrine\ORM\EntityManagerInterface;





class ResultatGesteTestType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('remarque', TextType::class, array())
            ->add('value', CheckboxType::class, array())
            ->add('cote', TextType::class)
            ->add('itemTest', TextType::class);
            $builder->get('value')
                ->addViewTransformer(new CallbackTransformer(
                    function ($normalizedFormat) {
                        return $normalizedFormat;
                    },
                    function ($submittedFormat) {
                        return ( $submittedFormat === '__false' ) ? false : $submittedFormat;
                    }
                ));
            $builder->get('cote')
                ->addViewTransformer(new CallbackTransformer(
                    function ($normalizedFormat) {
                        return $normalizedFormat;
                    },
                    function ($submittedFormat) {
                        return ( $submittedFormat === '0' ) ? false : true;
                    }
                ));
            $builder->get('itemTest')
                ->addViewTransformer(new CallbackTransformer(
                    function ($normalizedFormat) {
                        return $normalizedFormat;
                    },
                    function ($submittedFormat) {
                        return $this->entityManager->getRepository(GesteTest::class)
                        ->find($submittedFormat);
                    }
                ));

    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App2Bundle\Entity\Resultats\ResultatGesteTest'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app2bundle_resultats_resultatgestetest';
    }


}
