<?php

namespace App2Bundle\Form\Resultats;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\CallbackTransformer;
use App2Bundle\Entity\Tests\Musculaire;
use Doctrine\ORM\EntityManagerInterface;

class ResultatMusculaireType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('remarque', TextType::class, array())
            ->add('value', ChoiceType::class, array(
                'choices'  => array(
                    "0" => 0,
                    "1" => 1,
                    "2" => 2,
                    "2+" => 3,
                    "3" => 4,
                    "3+" => 5,
                    "4" => 6,
                    "4+" => 7,
                    "5" => 8, 
                ),
                'expanded' => false,
                'multiple' => false,
                
            ))
            ->add('cote', TextType::class)
            ->add('itemTest', TextType::class);

            $builder->get('cote')
                ->addViewTransformer(new CallbackTransformer(
                    function ($normalizedFormat) {
                        return $normalizedFormat;
                    },
                    function ($submittedFormat) {
                        return ( $submittedFormat === '0' ) ? false : true;
                    }
                ));
            $builder->get('itemTest')
                ->addViewTransformer(new CallbackTransformer(
                    function ($normalizedFormat) {
                        return $normalizedFormat;
                    },
                    function ($submittedFormat) {
                        return $this->entityManager->getRepository(Musculaire::class)
                        ->find($submittedFormat);
                    }
                ));

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App2Bundle\Entity\Resultats\ResultatMusculaire'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app2bundle_resultats_resultatmusculaire';
    }


}
