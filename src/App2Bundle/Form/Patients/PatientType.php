<?php

namespace App2Bundle\Form\Patients;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;



class PatientType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('codePatient', TextType::class, array())
            ->add('organisation', EntityType::class, array(
                'disabled' => true,
                'class' => 'App2Bundle:Users\Organisation',
                'choice_label' => 'nom',
                'multiple' => false,
                'expanded' => false,
                'data' => $options['organisation'],
                'label' => 'Organisation'
            ))
            ->add('user', EntityType::class, array(
                'disabled' => true,
                'class' => 'App2Bundle:Users\User',
                'choice_label' => 'username',
                'multiple' => false,
                'expanded' => false,
                'data' => $options['user'],
                'label' => 'Utilisateur'
            ))
            ->add('sexe', ChoiceType::class, array(
                'label' => 'Sexe',
                'choices'  => array(
                    'F' => "F",
                    'M' => "M",
                 ),
            ))            
            ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App2Bundle\Entity\Patients\Patient'
        ));
        $resolver->setRequired('user');
        $resolver->setRequired('organisation');
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app2bundle_patients_patient';
    }


}
