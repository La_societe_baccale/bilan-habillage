<?php

namespace App2Bundle\Form\Causes;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App2Bundle\Entity\Causes\Cause;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\CallbackTransformer;
use Doctrine\ORM\EntityManagerInterface;



class CauseType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', EntityType::class, array(
            'class' => 'App2Bundle:Causes\Cause',
            'choice_label' => 'nom',
            'mapped'=>false,
            'label' => false
        ));
        // $builder->get('id')
        //         ->addViewTransformer(new CallbackTransformer(
        //             function ($normalizedFormat) {
        //                 return $normalizedFormat;
        //             },
        //             function ($submittedFormat) {
        //                 if((int)$submittedFormat == 0){
        //                     return $this->entityManager->getRepository(Cause::class)
        //                     ->find($submittedFormat);
        //                 }
        //                 else {
        //                     return null;
        //                 }
        //             }
        //         ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App2Bundle\Entity\Causes\Cause'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app2bundle_causes_cause';
    }


}
