<?php

// src/DataFixtures/AppFixtures.php
namespace App2Bundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App2Bundle\Entity\Users\Organisation;
use App2Bundle\Entity\Users\User;
use App2Bundle\Entity\Causes\Cause;
use App2Bundle\Entity\Tests\Articulaire;
use App2Bundle\Entity\Tests\Musculaire;
use App2Bundle\Entity\Tests\GesteTest;
use App2Bundle\Entity\Tests\KapandjiPosition;
use App2Bundle\Entity\Tests\HabillageTest;
use App2Bundle\Entity\Enfilage\ConditionBilan;
use App2Bundle\Entity\Bilans\Implication;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;




class AppFixtures extends Fixture
{

    private $encoder;

    public function load(ObjectManager $manager)
    {

        $organisation = new Organisation();
        $organisation->setNom('Clinique du Petit Colmoulins');
        $manager->persist($organisation);
        $manager->flush();


        $user1 = new User();
        $user1->setUsername('user1');
        $user1->setEmail('user1@pcm.fr');
        $user1->setPlainPassword('test1234');
        $user1->setEnabled(1);

        $user2 = new User();
        $user2->setUsername('user2');
        $user2->setEmail('user2@pcm.fr');
        $user2->setPlainPassword('test1234');
        $user2->setEnabled(1);

        $user3 = new User();
        $user3->setUsername('user3');
        $user3->setEmail('user3@pcm.fr');
        $user3->setPlainPassword('test1234');
        $user3->setEnabled(1);

        $user4 = new User();
        $user4->setUsername('user4');
        $user4->setEmail('user4@pcm.fr');
        $user4->setPlainPassword('test1234');
        $user4->setEnabled(1);

        $organisationTest = $manager->getRepository(Organisation::class)
            ->findOneByNom('Clinique du Petit Colmoulins');

        $user1->setOrganisation($organisationTest);
        $user2->setOrganisation($organisationTest);
        $user3->setOrganisation($organisationTest);
        $user4->setOrganisation($organisationTest);

        $manager->persist($user1);
        $manager->persist($user2);
        $manager->persist($user3);
        $manager->persist($user4);


        $testArticulaire = [['épaule','abduction',180,0],
                    ['épaule','adduction',50,0],
                    ['épaule','antépulsion',180,0],
                    ['épaule','rotation interne',90,0],
                    ['épaule','rotation externe',90,0],
                    ['épaule','rétropulsion',50,0],
                    ['coude','flexion',145,0],
                    ['coude','extension',0,1],
                    ['coude','pronation',80,0],
                    ['coude','supination',85,0]
                    ];

        for ($i = 0; $i < count($testArticulaire); $i++) {
            $articulaire = new Articulaire();
            $articulaire->setNom($testArticulaire[$i][1]);
            $articulaire->setArticulation($testArticulaire[$i][0]);
            $articulaire->setAngleMax($testArticulaire[$i][2]);
            $articulaire->setIsReverse($testArticulaire[$i][3]);
            $manager->persist($articulaire);
        }
        

        $causes = ["raideur",  "spasticité", "douleur", "autre"];
        for ($i = 0; $i < count($causes); $i++) {
            $cause = new Cause();
            $cause->setNom($causes[$i]);
            $manager->persist($cause);
        }


        $testMusculaire = [['épaule','abduction'],
                    ['épaule','adduction'],
                    ['épaule','antépulsion'],
                    ['épaule','rotation interne'],
                    ['épaule','rotation externe'],
                    ['épaule','rétropulsion'],
                    ['coude','flexion'],
                    ['coude','extension'],
                    ['coude','pronation'],
                    ['coude','supination']
                    ];

        for ($i = 0; $i < count($testMusculaire); $i++) {
            $musculaire = new Musculaire();
            $musculaire->setNom($testMusculaire[$i][1]);
            $musculaire->setArticulation($testMusculaire[$i][0]);
            $manager->persist($musculaire);
        }

       
        $gesteTests = ['main-tête','main-bouche', 'main-nuque','main-fesse','main-épaule opposée','serment','mendiant','salut militaire','avion', 'plongeur', 'main-dos',"main-épaule homolatérale"];
    
        for ($i = 0; $i < count($gesteTests); $i++) {
            $gesteTest = new GesteTest();
            $gesteTest->setNom($gesteTests[$i]);
            $gesteTest->setOrdre($i+1);
            $manager->persist($gesteTest);
        }

        $testKapandji = [['position 1','Position_1'],
                    ['position 2','Position_2'],
                    ['position 3','Position_3'],
                    ['position 4','Position_4'],
                    ['position 5','Position_5'],
                    ['position 6','Position_6'],
                    ['position 7','Position_7'],
                    ['position 8','Position_8'],
                    ['position 9','Position_9'],
                    ['position 10','Position_10'],
                    ['position 11','Position_11']];

        for ($i = 0; $i < count($testKapandji); $i++) {
            $kapandjiposition = new KapandjiPosition();
            $kapandjiposition->setNom($testKapandji[$i][0]);
            if (file_exists('./web/uploads/images/KapandjiPosition/'.$testKapandji[$i][1].'.jpg')) {
                $file = new UploadedFile('./web/uploads/images/KapandjiPosition/'.$testKapandji[$i][1].'.jpg', $testKapandji[$i][1].'.jpg', null, filesize('./web/uploads/images/KapandjiPosition/'.$testKapandji[$i][1].'.jpg'), false, true);
            }
            else{
                if (file_exists('./web/Temp/KapandjiPosition/'.$testKapandji[$i][1].'.jpg')) {
                    $file = new UploadedFile('./web/Temp/KapandjiPosition/'.$testKapandji[$i][1].'.jpg', $testKapandji[$i][1].'.jpg', null, filesize('./web/Temp/KapandjiPosition/'.$testKapandji[$i][1].'.jpg'), false, true);
                }
            }
            $kapandjiposition->setImageFile($file);
            $kapandjiposition->setImage($testKapandji[$i][1].'.jpg');
            $manager->persist($kapandjiposition);
        }

        $testsHabillage = [['item_0_A','Le patient peut-il enfiler seul le gilet?'],
        ['item_0_B','Le patient peut-il enlever seul le gilet?'],
        ['item_1_A','Mettre la main sur l\'epaule opposée'],
        ['item_1_B','Déboutonner l\'épaulette'],
        ['item_1_C','Tester la résistance du bouton'],
        ['item_1_D','Tester la résistance du bouton (+)'],
        ['item_1_E','Tester la résistance du bouton (++)'],
        ['item_2_A','Mettre la main sur la poitrine opposée'],
        ['item_2_B','Déboutonner la poche'],
        ['item_2_C','Ouvrir la poche (fermeture verticale)'],
        ['item_2_D','Ouvrir la poche (fermeture horizontale)'],
        ['item_3_A','Ajuster le vêtement au niveau des épaules: mettre la main sur l\'épaule'],
        ['item_3_B','Homolatérale et tirer languette'],
        ['item_4_A','Dégrafer une attache de soutien-gorge'],
        ['item_4_B','Mettre la main dans le dos'],
        ['item_4Bis_A','Chercher la seconde manche: mettre la main'],
        ['item_4Bis_B','Toucher les boutons dans le dos'],
        ['item_5_A','Desserer la martingale dans le dos'],
        ['item_5_B','Mettre la main sur les fesses'],
        ['item_6_A','Nouer un lacer au niveau des hanches'],
        ['item_6_B','Tenir le lacet'],
        ['item_6Bis_A','Fermer son bouton de pantalon'],
        ['item_6Bis_B','Tester la résistance au bouton'],
        ['item_6Bis_C','Tester la résistance au bouton (+)'],
        ['item_6Bis_D','Tester la résistance au bouton (++)'],
        ['item_7_A','Fermer le gilet avec la fermeture éclair'],
        ['item_7_B','Tenir la tirette'],
        ['item_7_C','Tenir la glissière'],
        ['item_8_A','Utiliser la capuche : mettre les mains derrière la nuque'],
        ['item_8_B','Attraper la capuche et la tenir'],
        ['item_8_C','Relever la capuche sur la tête'],
        ['item_9_A','Tirer les cordons de la capuche'],
        ['item_9_B','Tenir les cordons'],
        ['item_9_C','Tester la résistance aux cordons'],
        ['item_9_D','Tester la résistance aux cordons (+)'],
        ['item_9_E','Tester la résistance aux cordons (++)'],
        ['item_10_A','Tenir la ceinture en bas du blouson'],
        ['item_10_B','Glisser la ceinture dans les passants']]
        ;
        $fileHabillage = [];
        for ($i = 0; $i < count($testsHabillage); $i++) {
            
        $testHabillage = new HabillageTest();
        if (file_exists('./web/uploads/images/HabillageEtapeImage/'.$testsHabillage[$i][0].'.png')) {
            $fileHabillage[$i] = new UploadedFile('./web/uploads/images/HabillageEtapeImage/'.$testsHabillage[$i][0].'.png', $testsHabillage[$i][0].'.png', null, filesize('./web/uploads/images/HabillageEtapeImage/'.$testsHabillage[$i][0].'.png'), false, true);
        }
        else{
            if (file_exists('./web/Temp/HabillageEtapeImage/'.$testsHabillage[$i][0].'.png')) {
                $fileHabillage[$i] = new UploadedFile('./web/Temp/HabillageEtapeImage/'.$testsHabillage[$i][0].'.png', $testsHabillage[$i][0].'.png', null, filesize('./web/Temp/HabillageEtapeImage/'.$testsHabillage[$i][0].'.png'), false, true);
            }
        }
        if (isset($fileHabillage[$i])) {
            $testHabillage->setImageFile($fileHabillage[$i]);
            $testHabillage->setImage($testsHabillage[$i][1].'.png');
        }
        $testHabillage->setNom($testsHabillage[$i][0]);
        $testHabillage->setDescriptif($testsHabillage[$i][1]);
        $manager->persist($testHabillage);
        }

        $manager->flush();

        $liensGesteTestArticulation = [["main-épaule opposée",[['épaule','abduction'],['épaule','rotation interne'],['coude','flexion'],['coude','supination']]],
                                    ["main-dos",[['épaule','rétropulsion'],['épaule','rotation interne'],['coude','flexion'],['coude','pronation']]],
                                    ["main-fesse",[['épaule','rétropulsion'],['épaule','rotation interne'],['coude','extension'],['coude','pronation']]],
                                    ["avion",[['épaule','abduction'],['coude','extension'],['coude','pronation']]],
                                    ["main-épaule homolatérale",[['coude','flexion'],['coude','supination']]],
                                    ["main-bouche",[['épaule','rotation interne'],['coude','flexion'],['coude','supination']]],
                                    ["main-nuque",[['épaule','abduction'],['épaule','antépulsion'],['épaule','rotation interne'],['épaule','rotation externe'],['coude','flexion'],['coude','pronation']]],
                                    ["main-tête",[['épaule','abduction'],['épaule','antépulsion'],['épaule','rotation interne'],['coude','flexion'],['coude','supination']]],
                                    ["serment",[['épaule','abduction'],['épaule','rotation externe'],['coude','flexion'],['coude','pronation']]],
                                    ["salut militaire",[['épaule','abduction'],['épaule','antépulsion'],['coude','flexion'],['coude','pronation']]],
                                    ["plongeur",[['épaule','adduction'],['épaule','antépulsion'],['coude','extension'],['coude','pronation']]],
                                    ["mendiant",[[['coude','flexion'],['coude','supination']]]]
                                    ];

        for ($i = 0; $i < count($liensGesteTestArticulation); $i++) {
            $gesteTest = $manager 
                ->getRepository(GesteTest::class)
                ->findOneByNom($liensGesteTestArticulation[$i][0]);
                for ($j = 0; $j < count($liensGesteTestArticulation[$i][1]); $j++) {
                    $articulaire = $manager 
                        ->getRepository(Articulaire::class)
                        ->findOneBy(array('articulation' => $liensGesteTestArticulation[$i][1][$j][0],'nom' => $liensGesteTestArticulation[$i][1][$j][1]));
                    $articulaire->addGesteTest($gesteTest);
                    $manager->persist($articulaire);
                }
        }

        $manager->flush();

        $liensBilanHabillage = [ 
                                        [["habillage","item_1_A","=","1"],
                                                [
                                                    ["geste-test", "main-épaule opposée","=",1],
                                                    ["musculaire", "adduction",">",4,"épaule"],
                                                    ["musculaire", "rotation interne",">",4,"épaule"],
                                                    ["musculaire", "flexion",">",4,"coude"]
                                                ]
                                            ]
                                            ,
                                        [["habillage", "item_2_A","=","1"],
                                            [
                                                ["musculaire", "rotation interne",">",4,"épaule"],
                                            ]
                                        ],
                                        [["habillage", "item_3_A","=","1"],
                                            [
                                                ["geste-test", "main-épaule homolatérale","=",1],
                                                ["articulaire", "supination","=",85,"coude"],
                                                ["articulaire", "flexion","=",145,"coude"],
                                                ["articulaire", "rotation externe","=",90,"épaule"],
                                            ],
                                        ],
                                        [["habillage", "item_4_A","=","1"],
                                            [
                                                ["geste-test", "main-dos","=",1],
                                                ["articulaire", "rotation interne","=",90,"épaule"],
                                                ["articulaire", "rétropulsion","=",50,"épaule"],
                                                ["musculaire", "rotation interne",">",4,"épaule"],
                                                ["musculaire", "flexion",">",4,"coude"],
                                                ["musculaire", "rétropulsion",">",4,"épaule"],
                                                ["musculaire", "abduction",">",2,"épaule"],
                                            ],
                                        ],
                                        [["habillage", "item_4Bis_A","=","1"],
                                            [
                                                ["geste-test", "main-dos","=",1],
                                                ["articulaire", "rotation interne","=",90,"épaule"],
                                                ["articulaire", "rétropulsion","=",50,"coude"],
                                                ["musculaire", "rotation interne",">",4,"épaule"],
                                                ["musculaire", "flexion",">",4,"coude"],
                                                ["musculaire", "rétropulsion",">",4,"épaule"],
                                                ["musculaire", "abduction",">",2,"épaule"],
                                            ],
                                        ],
                                        [["habillage", "item_5_A","=","1"],
                                            [
                                                ["geste-test", "main-fesse","=",1],
                                                ["geste-test", "tourner une clé dans une serrure","=",1],
                                                ["articulaire", "extension coude","<",20,"épaule"],
                                                ["articulaire", "adduction",">",35,"épaule"],
                                                ["musculaire", "supination",">",5,"coude"],
                                                ["musculaire", "rotation interne",">",4,"épaule"],
                                                ["musculaire", "rétropulsion",">",5,"épaule"],
                                                ["musculaire", "flexion",">",2,"coude"],
                                                ["musculaire", "extension",">",2,"coude"],
                                                ["musculaire", "adduction",">",3,"épaule"],
                                            ],
                                        ],
                                        [["habillage", "item_6_A","=","1"],
                                            [
                                                ["geste-test", "serveur","=",1],
                                                ["geste-test", "tourner une clé dans une serrure","=",1],
                                                ["musculaire", "flexion",">",2,"coude"],
                                                ["musculaire", "pronation",">",2,"coude"],
                                                ["musculaire", "supination",">",4,"coude"],
                                                ["musculaire", "rotation interne",">",2,"épaule"],
                                            ],
                                        ],
                                        [["habillage", "item_6bis_A","=","1"],
                                            [
                                                ["geste-test", "tourner une clé dans une serrure","=",1],
                                                ["musculaire", "flexion",">",2,"coude"],
                                                ["musculaire", "pronation",">",4,"coude"],
                                                ["musculaire", "supination",">",4,"coude"],
                                                ["musculaire", "rotation interne",">",2,"épaule"],
                                            ],
                                        ],
                                        [["habillage", "item_7_A","=","1"],
                                            [
                                                ["geste-test", "tourner une clé dans une serrure","=",1],
                                                ["articulaire", "flexion","=",145,"coude"],
                                                ["musculaire", "flexion",">",4,"coude"],
                                                ["musculaire", "rotation interne",">",4,"épaule"],
                                            ],
                                        ],
                                        [["habillage", "item_8_A","=","1"],
                                            [
                                                ["geste-test", "avion","=",1],
                                                ["geste-test", "plongeur","=",1],
                                                ["geste-test", "salut militaire","=",1],
                                                ["geste-test", "main-tête","=",1],
                                                ["geste-test", "main-nuque","=",1],
                                                ["geste-test", "main-bouche","=",1],
                                                ["articulaire", "flexion","=",145,"coude"],
                                                ["articulaire", "abduction","=",180,"épaule"],
                                                ["articulaire", "rotation externe","=",90,"épaule"],
                                                ["articulaire", "supination","=",85,"coude"],
                                                ["musculaire", "abduction",">",4,"épaule"],
                                                ["musculaire", "flexion",">",4,"coude"],
                                                ["musculaire", "rotation externe",">",4,"épaule"],
                                                ["musculaire", "supination",">",4,"coude"],
                                                ["musculaire", "extension",">",2,"coude"],
                                                ["musculaire", "rotation interne",">",2,"épaule"],
                                                ["musculaire", "antépulsion",">",4,"épaule"]
                                            ],
                                        ],
                                        [["habillage", "item_9_A","=","1"],
                                            [
                                                ["geste-test", "serment","=",1],
                                                ["geste-test", "serveur","=",1],
                                                ["articulaire", "flexion","=",145,"coude"],
                                                ["musculaire", "pronation",">",4,"coude"],
                                                ["musculaire", "flexion",">",4,"coude"],
                                                ["musculaire", "rotation interne",">",2,"épaule"],
                                                ["musculaire", "extension",">",4,"coude"],
                                                ["musculaire", "antépulsion",">",2,"épaule"],
                                                ["musculaire", "abduction",">",2,"épaule"]
                                            ],
                                        ],
                                        [["habillage", "item_10_A","=","1"],
                                            [
                                                ["geste-test", "main-fesse","=",1],
                                                ["musculaire", "adduction",">",4,"épaule"],
                                                ["musculaire", "rotation interne",">",4,"épaule"],
                                                ["musculaire", "rétropulsion",">",4,"épaule"]
                                            ],
                                        ],
                               ];
        
        //On met toutes les conditions dans le même array, from et to en vérifiant de ne pas créer de doublon
        $listeconditionsBilan = [];
        for ($i = 0; $i < count($liensBilanHabillage); $i++) {
            if(!in_array($liensBilanHabillage[$i][0],$listeconditionsBilan)){
                $listeconditionsBilan[] = $liensBilanHabillage[$i][0];
            }
            for($j = 0; $j < count($liensBilanHabillage[$i][1]); $j++) {
                if(!in_array($liensBilanHabillage[$i][1][$j],$listeconditionsBilan)){
                    $listeconditionsBilan[] = $liensBilanHabillage[$i][1][$j];
                }
            }
        }
        //On ajoute les conditions dans la BDD
        for ($i = 0; $i < count($listeconditionsBilan); $i++) {  
            $condition_bilan = new ConditionBilan;
            $condition_bilan->setBilan($listeconditionsBilan[$i][0]);
            $condition_bilan->setNom($listeconditionsBilan[$i][1]);
            $condition_bilan->setOperateur($listeconditionsBilan[$i][2]);
            $condition_bilan->setValue($listeconditionsBilan[$i][3]);
            if($listeconditionsBilan[$i][0] === "musculaire" || $listeconditionsBilan[$i][0] === "articulaire"){
                $condition_bilan->setArticulation($listeconditionsBilan[$i][4]);
            }
            $manager->persist($condition_bilan);

        }
        $manager->flush();


        for ($i = 0; $i < count($liensBilanHabillage); $i++) {

            $implication = new Implication;
            if($liensBilanHabillage[$i][0][0] === "musculaire" || $liensBilanHabillage[$i][0][0] === "articulaire"){
                $conditionBilanFrom = $manager 
                            ->getRepository(conditionBilan::class)
                            ->findOneBy(array(
                                'bilan' => $liensBilanHabillage[$i][0][0],
                                'nom' => $liensBilanHabillage[$i][0][1],
                                'value' => $liensBilanHabillage[$i][0][3] ,
                                'operateur' => $liensBilanHabillage[$i][0][2],
                                'articulation' => $liensBilanHabillage[$i][0][4]));
            }
            else{
                $conditionBilanFrom = $manager 
                            ->getRepository(conditionBilan::class)
                            ->findOneBy(array(
                                'bilan' => $liensBilanHabillage[$i][0][0],
                                'nom' => $liensBilanHabillage[$i][0][1],
                                'value' => $liensBilanHabillage[$i][0][3] ,
                                'operateur' => $liensBilanHabillage[$i][0][2]));
                
            }
            $conditionBilanTo = [];
            for($j = 0; $j < count($liensBilanHabillage[$i][1]); $j++) {
                if($liensBilanHabillage[$i][1][$j][0] === "musculaire" || $liensBilanHabillage[$i][1][$j][0] === "articulaire"){
                    $conditionBilanTo[] = $manager 
                                ->getRepository(conditionBilan::class)
                                ->findOneBy(array(
                                    'bilan' => $liensBilanHabillage[$i][1][$j][0],
                                    'nom' => $liensBilanHabillage[$i][1][$j][1],
                                    'value' => $liensBilanHabillage[$i][1][$j][3] ,
                                    'operateur' => $liensBilanHabillage[$i][1][$j][2],
                                    'articulation' => $liensBilanHabillage[$i][1][$j][4]));
                    
                }
                else{
                    $conditionBilanTo[] = $manager 
                                ->getRepository(conditionBilan::class)
                                ->findOneBy(array(
                                    'bilan' => $liensBilanHabillage[$i][1][$j][0],
                                    'nom' => $liensBilanHabillage[$i][1][$j][1],
                                    'value' => $liensBilanHabillage[$i][1][$j][3] ,
                                    'operateur' => $liensBilanHabillage[$i][1][$j][2]));
                    
                }
            }
            $implication->addConditionsBilanFrom($conditionBilanFrom);
            for($k = 0; $k < count($conditionBilanTo); $k++) {
                $implication->addConditionsBilanTo($conditionBilanTo[$k]);
            }
            $manager->persist($implication);
            $manager->flush();


            // print_r("listeconditionsBilan");
            // print_r($listeconditionsBilan); 
            
            // print_r("conditionBilanFrom");
            // print_r($conditionBilanFrom); 
        }

    }

}