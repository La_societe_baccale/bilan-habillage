<?php

namespace App2Bundle\Entity\Bilans;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * BilanHabillage
 *
 * @ORM\Table(name="bilanHabillage")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Bilans\BilanHabillagepository")
 */
class BilanHabillage
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string")
     */
    private $nom;

   

   /**
    * @var \DateTime
    *
    * @ORM\Column(name="date_ajout", type="datetime")
    */
    private $dateAjout;

    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Patients\Patient", inversedBy="bilansHabillage")
    * @ORM\JoinColumn(nullable=true)
    */
    private $patient;
    
    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\Resultats\ResultatHabillage" ,mappedBy="bilan", cascade={"all"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $resultats;

    /**
     * 
     * @ORM\OneToOne(targetEntity="App2Bundle\Entity\Bilans\Bilan", inversedBy="bilanHabillage")
     * 
     */
    private $bilan;

    /**
     * @ORM\Column(name="total_g", type="decimal", scale=1)
     */
    private $totalG = 0;

    /**
     * @ORM\Column(name="total_d", type="decimal", scale=1)
     */
    private $totalD = 0;

    


     public function __construct()
    {
        $this->dateAjout = new \Datetime();
        $this->resultats = new ArrayCollection();
    }

    

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return BilanHabillage
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set dateAjout.
     *
     * @param \DateTime $dateAjout
     *
     * @return BilanHabillage
     */
    public function setDateAjout($dateAjout)
    {
        $this->dateAjout = $dateAjout;

        return $this;
    }

    /**
     * Get dateAjout.
     *
     * @return \DateTime
     */
    public function getDateAjout()
    {
        return $this->dateAjout;
    }

    /**
     * Set patient.
     *
     * @param \App2Bundle\Entity\Patients\Patient|null $patient
     *
     * @return BilanHabillage
     */
    public function setPatient(\App2Bundle\Entity\Patients\Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient.
     *
     * @return \App2Bundle\Entity\Patients\Patient|null
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Add resultat.
     *
     * @param \App2Bundle\Entity\Resultats\ResultatHabillage $resultat
     *
     * @return BilanHabillage
     */
    public function addResultat(\App2Bundle\Entity\Resultats\ResultatHabillage $resultat)
    {
        $this->resultats[] = $resultat;
        if($resultat->getValue() == 1){
            $nomItem = $resultat->getItemTest()->getNom();
            if( $nomItem == "item_1_D" || $nomItem == "item_1_E" ||
            $nomItem == "item_6Bis_C" || $nomItem == "item_6Bis_D" ||
            $nomItem == "item_9_D" || $nomItem == "item_9_E" ){
                if($resultat->getCote() == 0){
                    $this->totalG += 0.5;
                }
                else{
                    $this->totalD += 0.5;
                }
            }
            elseif ($nomItem !== "item_1_C" && $nomItem !== "item_6Bis_B" && $nomItem !== "item_9_C") {
                if($resultat->getCote() == 0){
                    $this->totalG += 1;
                }
                else{
                    $this->totalD += 1;
                }

            }
        }
        

        return $this;
    }

    /**
     * Remove resultat.
     *
     * @param \App2Bundle\Entity\Resultats\ResultatHabillage $resultat
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeResultat(\App2Bundle\Entity\Resultats\ResultatHabillage $resultat)
    {
        return $this->resultats->removeElement($resultat);
    }

    /**
     * Get resultats.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResultats()
    {
        return $this->resultats;
    }

    /**
     * Set bilan.
     *
     * @param \App2Bundle\Entity\Bilans\Bilan|null $bilan
     *
     * @return BilanHabillage
     */
    public function setBilan(\App2Bundle\Entity\Bilans\Bilan $bilan = null)
    {
        $this->bilan = $bilan;

        return $this;
    }

    /**
     * Get bilan.
     *
     * @return \App2Bundle\Entity\Bilans\Bilan|null
     */
    public function getBilan()
    {
        return $this->bilan;
    }

    /**
     * Remove all resultat
     *
     * @return BilanHabillage
     */
    public function removeAllResultats()
    {
        $this->resultats = [];
        $this->totalD = 0;
        $this->totalG = 0;
        return $this;
    }

    /**
     * Set totalG.
     *
     * @param int $totalG
     *
     * @return BilanHabillage
     */
    public function setTotalG($totalG)
    {
        $this->totalG = $totalG;

        return $this;
    }

    /**
     * Get totalG.
     *
     * @return int
     */
    public function getTotalG()
    {
        return $this->totalG;
    }

    /**
     * Set totalD.
     *
     * @param int $totalD
     *
     * @return BilanHabillage
     */
    public function setTotalD($totalD)
    {
        $this->totalD = $totalD;

        return $this;
    }

    /**
     * Get totalD.
     *
     * @return int
     */
    public function getTotalD()
    {
        return $this->totalD;
    }

    
}
