<?php

namespace App2Bundle\Entity\Bilans;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Bilan
 *
 * @ORM\Table(name="bilan")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Bilans\BilanRepository")
 */
class Bilan
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

   /**
    * @var \DateTime
    *
    * @ORM\Column(name="date_ajout", type="datetime")
    */
    private $dateAjout;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string")
     */
    private $nom;

    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Patients\Patient",inversedBy="bilans")
    * @ORM\JoinColumn(nullable=false)
    */
    private $patient;
    
    /**
    * @ORM\OneToOne(targetEntity="App2Bundle\Entity\Bilans\BilanGesteTest", mappedBy="bilan", cascade={"all"})
    */
    protected $bilanGesteTest;

    /**
    * @ORM\OneToOne(targetEntity="App2Bundle\Entity\Bilans\BilanKapandji", mappedBy="bilan", cascade={"all"})
    */
    protected $bilanKapandji;
    
    /**
    * @ORM\OneToOne(targetEntity="App2Bundle\Entity\Bilans\BilanMusculaire", mappedBy="bilan", cascade={"all"})
    */
    protected $bilanMusculaire;

    /**
    * @ORM\OneToOne(targetEntity="App2Bundle\Entity\Bilans\BilanArticulaire", mappedBy="bilan", cascade={"all"})
    */
    protected $bilanArticulaire;

    /**
    * @ORM\OneToOne(targetEntity="App2Bundle\Entity\Bilans\BilanHabillage", mappedBy="bilan", cascade={"all"})
    */
    protected $bilanHabillage;

    /**
    * @ORM\OneToMany(targetEntity="App2Bundle\Entity\BilansCustom\BilanCustom", mappedBy="bilan", cascade={"all"})
    */
    protected $bilanCustom;


     public function __construct()
    {
        $this->dateAjout = new \Datetime();
    }


    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateAjout
     *
     * @param \DateTime $dateAjout
     *
     * @return Bilan
     */
    public function setDateAjout($dateAjout)
    {
        $this->dateAjout = $dateAjout;

        return $this;
    }

    /**
     * Get dateAjout
     *
     * @return \DateTime
     */
    public function getDateAjout()
    {
        return $this->dateAjout;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Bilan
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set bilanGesteTest
     *
     * @param \App2Bundle\Entity\Bilans\BilanGesteTest $bilanGesteTest
     *
     * @return Bilan
     */
    public function setBilanGesteTest(\App2Bundle\Entity\Bilans\BilanGesteTest $bilanGesteTest = null)
    {
        $this->bilanGesteTest = $bilanGesteTest;

        return $this;
    }

    /**
     * Get bilanGesteTest
     *
     * @return \App2Bundle\Entity\Bilans\BilanGesteTest
     */
    public function getBilanGesteTest()
    {
        return $this->bilanGesteTest;
    }

    /**
     * Set bilanKapandji
     *
     * @param \App2Bundle\Entity\Bilans\BilanKapandji $bilanKapandji
     *
     * @return Bilan
     */
    public function setBilanKapandji(\App2Bundle\Entity\Bilans\BilanKapandji $bilanKapandji = null)
    {
        $this->bilanKapandji = $bilanKapandji;

        return $this;
    }

    /**
     * Get bilanKapandji
     *
     * @return \App2Bundle\Entity\Bilans\BilanKapandji
     */
    public function getBilanKapandji()
    {
        return $this->bilanKapandji;
    }

    /**
     * Set bilanMusculaire
     *
     * @param \App2Bundle\Entity\Bilans\BilanMusculaire $bilanMusculaire
     *
     * @return Bilan
     */
    public function setBilanMusculaire(\App2Bundle\Entity\Bilans\BilanMusculaire $bilanMusculaire = null)
    {
        $this->bilanMusculaire = $bilanMusculaire;

        return $this;
    }

    /**
     * Get bilanMusculaire
     *
     * @return \App2Bundle\Entity\Bilans\BilanMusculaire
     */
    public function getBilanMusculaire()
    {
        return $this->bilanMusculaire;
    }

    /**
     * Set bilanArticulaire
     *
     * @param \App2Bundle\Entity\Bilans\BilanArticulaire $bilanArticulaire
     *
     * @return Bilan
     */
    public function setBilanArticulaire(\App2Bundle\Entity\Bilans\BilanArticulaire $bilanArticulaire = null)
    {
        $this->bilanArticulaire = $bilanArticulaire;

        return $this;
    }

    /**
     * Get bilanArticulaire
     *
     * @return \App2Bundle\Entity\Bilans\BilanArticulaire
     */
    public function getBilanArticulaire()
    {
        return $this->bilanArticulaire;
    }

    /**
     * Set patient
     *
     * @param \App2Bundle\Entity\Patients\Patient $patient
     *
     * @return Bilan
     */
    public function setPatient(\App2Bundle\Entity\Patients\Patient $patient)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return \App2Bundle\Entity\Patients\Patient
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Set bilanHabillage.
     *
     * @param \App2Bundle\Entity\Bilans\BilanHabillage|null $bilanHabillage
     *
     * @return Bilan
     */
    public function setBilanHabillage(\App2Bundle\Entity\Bilans\BilanHabillage $bilanHabillage = null)
    {
        $this->bilanHabillage = $bilanHabillage;

        return $this;
    }

    /**
     * Get bilanHabillage.
     *
     * @return \App2Bundle\Entity\Bilans\BilanHabillage|null
     */
    public function getBilanHabillage()
    {
        return $this->bilanHabillage;
    }

    /**
     * Add bilanCustom.
     *
     * @param \App2Bundle\Entity\BilansCustom\BilanCustom $bilanCustom
     *
     * @return Bilan
     */
    public function addBilanCustom(\App2Bundle\Entity\BilansCustom\BilanCustom $bilanCustom)
    {
        $this->bilanCustom[] = $bilanCustom;

        return $this;
    }

    /**
     * Remove bilanCustom.
     *
     * @param \App2Bundle\Entity\BilansCustom\BilanCustom $bilanCustom
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeBilanCustom(\App2Bundle\Entity\BilansCustom\BilanCustom $bilanCustom)
    {
        return $this->bilanCustom->removeElement($bilanCustom);
    }

    /**
     * Get bilanCustom.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBilanCustom()
    {
        return $this->bilanCustom;
    }
}
