<?php

namespace App2Bundle\Entity\Bilans;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * BilanArticulaire
 *
 * @ORM\Table(name="bilan_articulaire")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Bilans\BilanArticulaireRepository")
 */
class BilanArticulaire
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string")
     */
    private $nom;



   /**
    * @var \DateTime
    *
    * @ORM\Column(name="date_ajout", type="datetime")
    */
    private $dateAjout;

    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Patients\Patient",inversedBy="bilansArticulaire")
    * @ORM\JoinColumn(nullable=true)
    */
    private $patient;
    
    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\Resultats\ResultatArticulaire", mappedBy="bilan", cascade={"all"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $resultats;

    /**
     * 
     * @ORM\OneToOne(targetEntity="App2Bundle\Entity\Bilans\Bilan", inversedBy="bilanArticulaire")
     * 
     */
    private $bilan;

    

     public function __construct()
    {
        $this->dateAjout = new \Datetime();
        $this->resultats = new ArrayCollection();
    }


   

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateAjout
     *
     * @param \DateTime $dateAjout
     *
     * @return BilanArticulaire
     */
    public function setDateAjout($dateAjout)
    {
        $this->dateAjout = $dateAjout;

        return $this;
    }

    /**
     * Get dateAjout
     *
     * @return \DateTime
     */
    public function getDateAjout()
    {
        return $this->dateAjout;
    }

    /**
     * Add resultat
     *
     * @param \App2Bundle\Entity\Resultats\ResultatArticulaire $resultat
     *
     * @return BilanArticulaire
     */
    public function addResultat(\App2Bundle\Entity\Resultats\ResultatArticulaire $resultat)
    {
        $this->resultats[] = $resultat;

        return $this;
    }

    /**
     * Remove resultat
     *
     * @param \App2Bundle\Entity\Resultats\ResultatArticulaire $resultat
     */
    public function removeResultat(\App2Bundle\Entity\Resultats\ResultatArticulaire $resultat)
    {
        $this->resultats->removeElement($resultat);
    }

    /**
     * Remove all resultat
     *
     * @return BilanArticulaire
     */
    public function removeAllResultats()
    {
        $this->resultats = [];
        return $this;
    }

    /**
     * Get resultats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResultats()
    {
        return $this->resultats;
    }

    /**
     * Set bilan
     *
     * @param \App2Bundle\Entity\Bilans\Bilan $bilan
     *
     * @return BilanArticulaire
     */
    public function setBilan(\App2Bundle\Entity\Bilans\Bilan $bilan = null)
    {
        $this->bilan = $bilan;

        return $this;
    }

    /**
     * Get bilan
     *
     * @return \App2Bundle\Entity\Bilans\Bilan
     */
    public function getBilan()
    {
        return $this->bilan;
    }

    /**
     * Set patient
     *
     * @param \App2Bundle\Entity\Patients\Patient $patient
     *
     * @return BilanArticulaire
     */
    public function setPatient(\App2Bundle\Entity\Patients\Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return \App2Bundle\Entity\Patients\Patient
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return BilanArticulaire
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set dateLancement.
     *
     * @param string $dateLancement
     *
     * @return BilanArticulaire
     */
    public function setDateLancement($dateLancement)
    {
        $this->dateLancement = $dateLancement;

        return $this;
    }

    /**
     * Get dateLancement.
     *
     * @return string
     */
    public function getDateLancement()
    {
        return $this->dateLancement;
    }
}
