<?php

namespace App2Bundle\Entity\Bilans;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Implication
 *
 * @ORM\Table(name="implication")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Bilans\ImplicationRepository")
 */
class Implication
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * 
     * @ORM\ManyToMany(targetEntity="App2Bundle\Entity\Enfilage\ConditionBilan", inversedBy="implicationsFrom")
     * @ORM\JoinTable(name="implications_conditions_From")
     *      
     */
    private $conditionsBilanFrom;

    /**
     * @ORM\ManyToMany(targetEntity="App2Bundle\Entity\Enfilage\ConditionBilan", inversedBy="implicationsTo")
     * @ORM\JoinTable(name="implications_conditions_To")
     *      
     */
    private $conditionsBilanTo;
    

     public function __construct()
    {
        $this->conditionsBilanTo = new ArrayCollection();
        $this->conditionsBilanFrom = new ArrayCollection();
    }


   


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add conditionsBilanFrom.
     *
     * @param \App2Bundle\Entity\Enfilage\ConditionBilan $conditionsBilanFrom
     *
     * @return Implication
     */
    public function addConditionsBilanFrom(\App2Bundle\Entity\Enfilage\ConditionBilan $conditionsBilanFrom)
    {
        $this->conditionsBilanFrom[] = $conditionsBilanFrom;

        return $this;
    }

    /**
     * Remove conditionsBilanFrom.
     *
     * @param \App2Bundle\Entity\Enfilage\ConditionBilan $conditionsBilanFrom
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeConditionsBilanFrom(\App2Bundle\Entity\Enfilage\ConditionBilan $conditionsBilanFrom)
    {
        return $this->conditionsBilanFrom->removeElement($conditionsBilanFrom);
    }

    /**
     * Get conditionsBilanFrom.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConditionsBilanFrom()
    {
        return $this->conditionsBilanFrom;
    }

    /**
     * Add conditionsBilanTo.
     *
     * @param \App2Bundle\Entity\Enfilage\ConditionBilan $conditionsBilanTo
     *
     * @return Implication
     */
    public function addConditionsBilanTo(\App2Bundle\Entity\Enfilage\ConditionBilan $conditionsBilanTo)
    {
        $this->conditionsBilanTo[] = $conditionsBilanTo;

        return $this;
    }

    /**
     * Remove conditionsBilanTo.
     *
     * @param \App2Bundle\Entity\Enfilage\ConditionBilan $conditionsBilanTo
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeConditionsBilanTo(\App2Bundle\Entity\Enfilage\ConditionBilan $conditionsBilanTo)
    {
        return $this->conditionsBilanTo->removeElement($conditionsBilanTo);
    }

    /**
     * Get conditionsBilanTo.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConditionsBilanTo()
    {
        return $this->conditionsBilanTo;
    }
}
