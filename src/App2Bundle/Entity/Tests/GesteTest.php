<?php

namespace App2Bundle\Entity\Tests;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * GesteTest
 *
 * @ORM\Table(name="geste_test")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Tests\GesteTestRepository")
 */
class GesteTest
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string")
     */
    private $nom;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordre", type="integer")
     */
    private $ordre;

    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\Resultats\ResultatGesteTest", mappedBy="itemTest")
     * @ORM\JoinColumn(nullable=true)
     */
    private $resultats;

    /**
     * 
     * @ORM\ManyToMany(targetEntity="App2Bundle\Entity\Tests\Articulaire", mappedBy="gesteTests")
     */
    private $articulaires;



     public function __construct()
    {
        $this->resultats = new ArrayCollection();
        $this->articulaires = new ArrayCollection();
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return GesteTest
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }


    /**
     * Add resultat
     *
     * @param \App2Bundle\Entity\Resultats\ResultatGesteTest $resultat
     *
     * @return GesteTest
     */
    public function addResultat(\App2Bundle\Entity\Resultats\ResultatGesteTest $resultat)
    {
        $this->resultats[] = $resultat;

        return $this;
    }

    /**
     * Remove resultat
     *
     * @param \App2Bundle\Entity\Resultats\ResultatGesteTest $resultat
     */
    public function removeResultat(\App2Bundle\Entity\Resultats\ResultatGesteTest $resultat)
    {
        $this->resultats->removeElement($resultat);
    }

    /**
     * Get resultats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResultats()
    {
        return $this->resultats;
    }

    /**
     * Add articulaire
     *
     * @param \App2Bundle\Entity\Tests\Articulaire $articulaire
     *
     * @return GesteTest
     */
    public function addArticulaire(\App2Bundle\Entity\Tests\Articulaire $articulaire)
    {
        $this->articulaires[] = $articulaire;

        return $this;
    }

    /**
     * Remove articulaire
     *
     * @param \App2Bundle\Entity\Tests\Articulaire $articulaire
     */
    public function removeArticulaire(\App2Bundle\Entity\Tests\Articulaire $articulaire)
    {
        $this->articulaires->removeElement($articulaire);
    }

    /**
     * Get articulaires
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticulaires()
    {
        return $this->articulaires;
    }

    /**
     * Set ordre.
     *
     * @param int $ordre
     *
     * @return GesteTest
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;

        return $this;
    }

    /**
     * Get ordre.
     *
     * @return int
     */
    public function getOrdre()
    {
        return $this->ordre;
    }
}
