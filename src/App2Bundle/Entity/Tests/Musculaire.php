<?php

namespace App2Bundle\Entity\Tests;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Musculaire
 *
 * @ORM\Table(name="musculaire")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Tests\MusculaireRepository")
 */
class Musculaire
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="articulation", type="string")
     */
    private $articulation;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string")
     */
    private $nom;

    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\Resultats\ResultatMusculaire", mappedBy="itemTest")
     * @ORM\JoinColumn(nullable=true)
     */
    private $resultats;



     public function __construct()
    {
        $this->resultats = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set articulation
     *
     * @param string $articulation
     *
     * @return Musculaire
     */
    public function setArticulation($articulation)
    {
        $this->articulation = $articulation;

        return $this;
    }

    /**
     * Get articulation
     *
     * @return string
     */
    public function getArticulation()
    {
        return $this->articulation;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Musculaire
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Add resultat
     *
     * @param \App2Bundle\Entity\Resultats\ResultatMusculaire $resultat
     *
     * @return Musculaire
     */
    public function addResultat(\App2Bundle\Entity\Resultats\ResultatMusculaire $resultat)
    {
        $this->resultats[] = $resultat;

        return $this;
    }

    /**
     * Remove resultat
     *
     * @param \App2Bundle\Entity\Resultats\ResultatMusculaire $resultat
     */
    public function removeResultat(\App2Bundle\Entity\Resultats\ResultatMusculaire $resultat)
    {
        $this->resultats->removeElement($resultat);
    }

    /**
     * Get resultats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResultats()
    {
        return $this->resultats;
    }
}
