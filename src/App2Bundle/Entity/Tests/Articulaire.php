<?php

namespace App2Bundle\Entity\Tests;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Articulaire
 *
 * @ORM\Table(name="articulaire")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Tests\ArticulaireRepository")
 */
class Articulaire
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="articulation", type="string")
     */
    private $articulation;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string")
     */
    private $nom;

    /**
     * @var integer
     *
     * @ORM\Column(name="angle_max", type="integer")
     */
    private $angleMax;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_reverse", type="boolean", options={"default" : 0})
     */
    private $isReverse;

     /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\Resultats\ResultatArticulaire", mappedBy="itemTest")
     * @ORM\JoinColumn(nullable=true)
     */
    private $resultats;

    /**
     * 
     * @ORM\ManyToMany(targetEntity="App2Bundle\Entity\Tests\GesteTest", inversedBy="articulaires")
     */
    private $gesteTests;

     public function __construct()
    {
        $this->resultats = new ArrayCollection();
        $this->gesteTests = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set articulation
     *
     * @param string $articulation
     *
     * @return Articulaire
     */
    public function setArticulation($articulation)
    {
        $this->articulation = $articulation;

        return $this;
    }

    /**
     * Get articulation
     *
     * @return string
     */
    public function getArticulation()
    {
        return $this->articulation;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Articulaire
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set angleMax
     *
     * @param integer $angleMax
     *
     * @return Articulaire
     */
    public function setAngleMax($angleMax)
    {
        $this->angleMax = $angleMax;

        return $this;
    }

    /**
     * Get angleMax
     *
     * @return integer
     */
    public function getAngleMax()
    {
        return $this->angleMax;
    }

    /**
     * Set isReverse
     *
     * @param boolean $isReverse
     *
     * @return Articulaire
     */
    public function setIsReverse($isReverse)
    {
        $this->isReverse = $isReverse;

        return $this;
    }

    /**
     * Get isReverse
     *
     * @return boolean
     */
    public function getIsReverse()
    {
        return $this->isReverse;
    }

    /**
     * Add resultat
     *
     * @param \App2Bundle\Entity\Resultats\ResultatArticulaire $resultat
     *
     * @return Articulaire
     */
    public function addResultat(\App2Bundle\Entity\Resultats\ResultatArticulaire $resultat)
    {
        $this->resultats[] = $resultat;

        return $this;
    }

    /**
     * Remove resultat
     *
     * @param \App2Bundle\Entity\Resultats\ResultatArticulaire $resultat
     */
    public function removeResultat(\App2Bundle\Entity\Resultats\ResultatArticulaire $resultat)
    {
        $this->resultats->removeElement($resultat);
    }

    /**
     * Get resultats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResultats()
    {
        return $this->resultats;
    }

    /**
     * Add gesteTest
     *
     * @param \App2Bundle\Entity\Tests\GesteTest $gesteTest
     *
     * @return Articulaire
     */
    public function addGesteTest(\App2Bundle\Entity\Tests\GesteTest $gesteTest)
    {
        $this->gesteTests[] = $gesteTest;

        return $this;
    }

    /**
     * Remove gesteTest
     *
     * @param \App2Bundle\Entity\Tests\GesteTest $gesteTest
     */
    public function removeGesteTest(\App2Bundle\Entity\Tests\GesteTest $gesteTest)
    {
        $this->gesteTests->removeElement($gesteTest);
    }

    /**
     * Get gesteTests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGesteTests()
    {
        return $this->gesteTests;
    }
}
