<?php

namespace App2Bundle\Entity\Patients;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Patient
 *
 * @ORM\Table(name="patient")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Patients\PatientRepository")
 */
class Patient
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Users\User")
    * @ORM\JoinColumn(nullable=false)
    */
    private $user;

    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Users\Organisation")
    * @ORM\JoinColumn(nullable=true)
    */
    private $organisation;

    /**
     * @var string
     *
     * @ORM\Column(name="code_patient", type="string")
     */
    private $codePatient;

    /**
    * @var \DateTime
    *
    * @ORM\Column(name="date_ajout", type="datetime")
    */
    private $dateAjout;

    /**
     * @var string
     *
     * @ORM\Column(name="sexe", type="string", nullable=true)
     */
    private $sexe;

    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\Bilans\Bilan", mappedBy="patient", cascade={"all"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $bilans;

    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\Bilans\BilanGesteTest", mappedBy="patient", cascade={"all"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $bilansGesteTest;

    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\Bilans\BilanKapandji", mappedBy="patient", cascade={"all"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $bilansKapandji;

    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\Bilans\BilanMusculaire", mappedBy="patient", cascade={"all"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $bilansMusculaire;

    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\Bilans\BilanArticulaire", mappedBy="patient", cascade={"all"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $bilansArticulaire;

    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\Bilans\BilanHabillage", mappedBy="patient", cascade={"all"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $bilansHabillage;

    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\BilansCustom\BilanCustom", mappedBy="patient", cascade={"all"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $bilansCustom;

     /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\BilansConseil\BilanConseil", mappedBy="patient", cascade={"all"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $bilansConseil;



     public function __construct()
    {
        $this->dateAjout = new \Datetime();
        $this->bilans = new ArrayCollection();
        $this->bilansGesteTest = new ArrayCollection();
        $this->bilansKapandji = new ArrayCollection();
        $this->bilansMusculaire = new ArrayCollection();
        $this->bilansArticulaire = new ArrayCollection();
        $this->bilansHabillage = new ArrayCollection();
        $this->bilansCustom = new ArrayCollection();
        $this->bilansConseil = new ArrayCollection();
    }


    /**
     * Set codePatient
     *
     * @param string $codePatient
     *
     * @return Patient
     */
    public function setCodePatient($codePatient)
    {
        $this->codePatient = $codePatient;

        return $this;
    }

    /**
     * Get codePatient
     *
     * @return string
     */
    public function getCodePatient()
    {
        return $this->codePatient;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     *
     * @return Patient
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }


    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Patient
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    

    /**
     * Set dateAjout
     *
     * @param \DateTime $dateAjout
     *
     * @return Patient
     */
    public function setDateAjout($dateAjout)
    {
        $this->dateAjout = $dateAjout;

        return $this;
    }

    /**
     * Get dateAjout
     *
     * @return \DateTime
     */
    public function getDateAjout()
    {
        return $this->dateAjout;
    }

    

    /**
     * Set organisation
     *
     * @param \App2Bundle\Entity\Users\Organisation $organisation
     *
     * @return Patient
     */
    public function setOrganisation(\App2Bundle\Entity\Users\Organisation $organisation = null)
    {
        $this->organisation = $organisation;

        return $this;
    }

    /**
     * Get organisation
     *
     * @return \App2Bundle\Entity\Users\Organisation
     */
    public function getOrganisation()
    {
        return $this->organisation;
    }

    /**
     * Set user
     *
     * @param \App2Bundle\Entity\Users\User $user
     *
     * @return Patient
     */
    public function setUser(\App2Bundle\Entity\Users\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App2Bundle\Entity\Users\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add bilan
     *
     * @param \App2Bundle\Entity\Bilans\Bilan $bilan
     *
     * @return Patient
     */
    public function addBilan(\App2Bundle\Entity\Bilans\Bilan $bilan)
    {
        $this->bilans[] = $bilan;

        return $this;
    }

    /**
     * Remove bilan
     *
     * @param \App2Bundle\Entity\Bilans\Bilan $bilan
     */
    public function removeBilan(\App2Bundle\Entity\Bilans\Bilan $bilan)
    {
        $this->bilans->removeElement($bilan);
    }

    /**
     * Get bilans
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBilans()
    {
        return $this->bilans;
    }

    /**
     * Add bilansGesteTest
     *
     * @param \App2Bundle\Entity\Bilans\BilanGesteTest $bilansGesteTest
     *
     * @return Patient
     */
    public function addBilansGesteTest(\App2Bundle\Entity\Bilans\BilanGesteTest $bilansGesteTest)
    {
        $this->bilansGesteTest[] = $bilansGesteTest;

        return $this;
    }

    /**
     * Remove bilansGesteTest
     *
     * @param \App2Bundle\Entity\Bilans\BilanGesteTest $bilansGesteTest
     */
    public function removeBilansGesteTest(\App2Bundle\Entity\Bilans\BilanGesteTest $bilansGesteTest)
    {
        $this->bilansGesteTest->removeElement($bilansGesteTest);
    }

    /**
     * Get bilansGesteTest
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBilansGesteTest()
    {
        return $this->bilansGesteTest;
    }

    /**
     * Add bilansKapandji
     *
     * @param \App2Bundle\Entity\Bilans\BilanKapandji $bilansKapandji
     *
     * @return Patient
     */
    public function addBilansKapandji(\App2Bundle\Entity\Bilans\BilanKapandji $bilansKapandji)
    {
        $this->bilansKapandji[] = $bilansKapandji;

        return $this;
    }

    /**
     * Remove bilansKapandji
     *
     * @param \App2Bundle\Entity\Bilans\BilanKapandji $bilansKapandji
     */
    public function removeBilansKapandji(\App2Bundle\Entity\Bilans\BilanKapandji $bilansKapandji)
    {
        $this->bilansKapandji->removeElement($bilansKapandji);
    }

    /**
     * Get bilansKapandji
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBilansKapandji()
    {
        return $this->bilansKapandji;
    }

    /**
     * Add bilansMusculaire
     *
     * @param \App2Bundle\Entity\Bilans\BilanMusculaire $bilansMusculaire
     *
     * @return Patient
     */
    public function addBilansMusculaire(\App2Bundle\Entity\Bilans\BilanMusculaire $bilansMusculaire)
    {
        $this->bilansMusculaire[] = $bilansMusculaire;

        return $this;
    }

    /**
     * Remove bilansMusculaire
     *
     * @param \App2Bundle\Entity\Bilans\BilanMusculaire $bilansMusculaire
     */
    public function removeBilansMusculaire(\App2Bundle\Entity\Bilans\BilanMusculaire $bilansMusculaire)
    {
        $this->bilansMusculaire->removeElement($bilansMusculaire);
    }

    /**
     * Get bilansMusculaire
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBilansMusculaire()
    {
        return $this->bilansMusculaire;
    }

    /**
     * Add bilansArticulaire
     *
     * @param \App2Bundle\Entity\Bilans\BilanArticulaire $bilansArticulaire
     *
     * @return Patient
     */
    public function addBilansArticulaire(\App2Bundle\Entity\Bilans\BilanArticulaire $bilansArticulaire)
    {
        $this->bilansArticulaire[] = $bilansArticulaire;

        return $this;
    }

    /**
     * Remove bilansArticulaire
     *
     * @param \App2Bundle\Entity\Bilans\BilanArticulaire $bilansArticulaire
     */
    public function removeBilansArticulaire(\App2Bundle\Entity\Bilans\BilanArticulaire $bilansArticulaire)
    {
        $this->bilansArticulaire->removeElement($bilansArticulaire);
    }

    /**
     * Get bilansArticulaire
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBilansArticulaire()
    {
        return $this->bilansArticulaire;
    }

    /**
     * Add bilansHabillage.
     *
     * @param \App2Bundle\Entity\Bilans\BilanHabillage $bilansHabillage
     *
     * @return Patient
     */
    public function addBilansHabillage(\App2Bundle\Entity\Bilans\BilanHabillage $bilansHabillage)
    {
        $this->bilansHabillage[] = $bilansHabillage;

        return $this;
    }

    /**
     * Remove bilansHabillage.
     *
     * @param \App2Bundle\Entity\Bilans\BilanHabillage $bilansHabillage
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeBilansHabillage(\App2Bundle\Entity\Bilans\BilanHabillage $bilansHabillage)
    {
        return $this->bilansHabillage->removeElement($bilansHabillage);
    }

    /**
     * Get bilansHabillage.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBilansHabillage()
    {
        return $this->bilansHabillage;
    }

    /**
     * Add bilansCustom.
     *
     * @param \App2Bundle\Entity\BilansCustom\BilanCustom $bilansCustom
     *
     * @return Patient
     */
    public function addBilansCustom(\App2Bundle\Entity\BilansCustom\BilanCustom $bilansCustom)
    {
        $this->bilansCustom[] = $bilansCustom;

        return $this;
    }

    /**
     * Remove bilansCustom.
     *
     * @param \App2Bundle\Entity\BilansCustom\BilanCustom $bilansCustom
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeBilansCustom(\App2Bundle\Entity\BilansCustom\BilanCustom $bilansCustom)
    {
        return $this->bilansCustom->removeElement($bilansCustom);
    }

    /**
     * Get bilansCustom.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBilansCustom()
    {
        return $this->bilansCustom;
    }

    /**
     * Add bilansConseil.
     *
     * @param \App2Bundle\Entity\BilansConseil\BilanConseil $bilansConseil
     *
     * @return Patient
     */
    public function addBilansConseil(\App2Bundle\Entity\BilansConseil\BilanConseil $bilansConseil)
    {
        $this->bilansConseil[] = $bilansConseil;

        return $this;
    }

    /**
     * Remove bilansConseil.
     *
     * @param \App2Bundle\Entity\BilansConseil\BilanConseil $bilansConseil
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeBilansConseil(\App2Bundle\Entity\BilansConseil\BilanConseil $bilansConseil)
    {
        return $this->bilansConseil->removeElement($bilansConseil);
    }

    /**
     * Get bilansConseil.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBilansConseil()
    {
        return $this->bilansConseil;
    }
}
