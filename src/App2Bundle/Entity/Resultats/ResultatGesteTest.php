<?php

namespace App2Bundle\Entity\Resultats;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * ResultatGesteTest
 *
 * @ORM\Table(name="resultat_geste_test")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Resultats\ResultatGesteTestRepository")
 */
class ResultatGesteTest
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    
    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Tests\GesteTest",inversedBy="resultats")
    * @ORM\JoinColumn(nullable=false)
    */
    private $itemTest;
    
    /**
     * @var string
     *
     * @ORM\Column(name="remarque", type="string", nullable= true)
     */
    private $remarque;

    /**
     * @var boolean
     *
     * @ORM\Column(name="value", type="boolean")
     */
    private $value;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cote", type="boolean")
     */
    private $cote;

     /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Bilans\BilanGesteTest",inversedBy="resultats")
    * @ORM\JoinColumn(nullable=false)
    */
    private $bilan;

     public function __construct()
    {
        // $this->kapandjis = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set remarque
     *
     * @param string $remarque
     *
     * @return ResultatGesteTest
     */
    public function setRemarque($remarque)
    {
        $this->remarque = $remarque;

        return $this;
    }

    /**
     * Get remarque
     *
     * @return string
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    /**
     * Set value
     *
     * @param boolean $value
     *
     * @return ResultatGesteTest
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return boolean
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set cote
     *
     * @param boolean $cote
     *
     * @return ResultatGesteTest
     */
    public function setCote($cote)
    {
        $this->cote = $cote;

        return $this;
    }

    /**
     * Get cote
     *
     * @return boolean
     */
    public function getCote()
    {
        return $this->cote;
    }


    /**
     * Set bilan
     *
     * @param \App2Bundle\Entity\Bilans\BilanGesteTest $bilan
     *
     * @return ResultatGesteTest
     */
    public function setBilan(\App2Bundle\Entity\Bilans\BilanGesteTest $bilan)
    {
        $this->bilan = $bilan;

        return $this;
    }

    /**
     * Get bilan
     *
     * @return \App2Bundle\Entity\Bilans\BilanGesteTest
     */
    public function getBilan()
    {
        return $this->bilan;
    }

    /**
     * Set itemTest
     *
     * @param \App2Bundle\Entity\Tests\GesteTest $itemTest
     *
     * @return ResultatGesteTest
     */
    public function setItemTest(\App2Bundle\Entity\Tests\GesteTest $itemTest)
    {
        $this->itemTest = $itemTest;

        return $this;
    }

    /**
     * Get itemTest
     *
     * @return \App2Bundle\Entity\Tests\GesteTest
     */
    public function getItemTest()
    {
        return $this->itemTest;
    }
}
