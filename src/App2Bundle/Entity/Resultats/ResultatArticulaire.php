<?php

namespace App2Bundle\Entity\Resultats;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * ResultatArticulaire
 *
 * @ORM\Table(name="resultat_articulaire")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Resultats\ResultatArticulairepository")
 */
class ResultatArticulaire
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Tests\Articulaire",inversedBy="resultats")
    * @ORM\JoinColumn(nullable=false)
    */
    private $itemTest;
    
    /**
     * @var string
     *
     * @ORM\Column(name="remarque", type="string", nullable= true)
     */
    private $remarque;

    /**
     * @var integer
     *
     * @ORM\Column(name="value", type="integer")
     */
    private $value;


    /**
     * @var boolean
     *
     * @ORM\Column(name="cote", type="boolean")
     */
    private $cote;

    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Bilans\BilanArticulaire" ,inversedBy="resultats")
    * @ORM\JoinColumn(nullable=false)
    */
    private $bilan;

    /**
     * 
     * @ORM\ManyToMany(targetEntity="App2Bundle\Entity\Causes\Cause", mappedBy="resultatsArticulaire", cascade={"persist"})
     */
    private $causes;



     public function __construct()
    {
        $this->causes = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set remarque.
     *
     * @param string|null $remarque
     *
     * @return ResultatArticulaire
     */
    public function setRemarque($remarque = null)
    {
        $this->remarque = $remarque;

        return $this;
    }

    /**
     * Get remarque.
     *
     * @return string|null
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    /**
     * Set value.
     *
     * @param int $value
     *
     * @return ResultatArticulaire
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set cote.
     *
     * @param bool $cote
     *
     * @return ResultatArticulaire
     */
    public function setCote($cote)
    {
        $this->cote = $cote;

        return $this;
    }

    /**
     * Get cote.
     *
     * @return bool
     */
    public function getCote()
    {
        return $this->cote;
    }

    /**
     * Set bilan.
     *
     * @param \App2Bundle\Entity\Bilans\BilanArticulaire $bilan
     *
     * @return ResultatArticulaire
     */
    public function setBilan(\App2Bundle\Entity\Bilans\BilanArticulaire $bilan)
    {
        $this->bilan = $bilan;

        return $this;
    }

    /**
     * Get bilan.
     *
     * @return \App2Bundle\Entity\Bilans\BilanArticulaire
     */
    public function getBilan()
    {
        return $this->bilan;
    }

    /**
     * Add cause.
     *
     * @param \App2Bundle\Entity\Causes\Cause $cause
     *
     * @return ResultatArticulaire
     */
    public function addCause(\App2Bundle\Entity\Causes\Cause $cause)
    {
        $this->causes[] = $cause;

        return $this;
    }

    /**
     * Remove cause.
     *
     * @param \App2Bundle\Entity\Causes\Cause $cause
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCause(\App2Bundle\Entity\Causes\Cause $cause)
    {
        return $this->causes->removeElement($cause);
    }

    /**
     * Get causes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCauses()
    {
        return $this->causes;
    }

    /**
     * Remove all cause
     *
     * @return ResultatArticulaire
     */
    public function removeAllCauses()
    {
        $this->causes = [];
        return $this;
    }
    /**
     * Set itemTest.
     *
     * @param \App2Bundle\Entity\Tests\Articulaire $itemTest
     *
     * @return ResultatArticulaire
     */
    public function setItemTest(\App2Bundle\Entity\Tests\Articulaire $itemTest)
    {
        $this->itemTest = $itemTest;

        return $this;
    }

    /**
     * Get itemTest.
     *
     * @return \App2Bundle\Entity\Tests\Articulaire
     */
    public function getItemTest()
    {
        return $this->itemTest;
    }
}
