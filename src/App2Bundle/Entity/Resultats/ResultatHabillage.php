<?php

namespace App2Bundle\Entity\Resultats;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * ResultatHabillage
 *
 * @ORM\Table(name="resultat_habillage")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Resultats\ResultatHabillageRepository")
 */
class ResultatHabillage
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Tests\HabillageTest",inversedBy="resultats")
    * @ORM\JoinColumn(nullable=false)
    */
    private $itemTest;
   
    
    /**
     * @var string
     *
     * @ORM\Column(name="remarque", type="string", nullable= true)
     */
    private $remarque;

    /**
     * @var boolean
     *
     * @ORM\Column(name="value", type="boolean")
     */
    private $value;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cote", type="boolean")
     */
    private $cote;

     /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Bilans\BilanHabillage",inversedBy="resultats")
    * @ORM\JoinColumn(nullable=false)
    */
    private $bilan;

     public function __construct()
    {
        // $this->kapandjis = new ArrayCollection();
    }


   

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set remarque.
     *
     * @param string|null $remarque
     *
     * @return ResultatHabillage
     */
    public function setRemarque($remarque = null)
    {
        $this->remarque = $remarque;

        return $this;
    }

    /**
     * Get remarque.
     *
     * @return string|null
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    /**
     * Set value.
     *
     * @param bool $value
     *
     * @return ResultatHabillage
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return bool
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set cote.
     *
     * @param bool $cote
     *
     * @return ResultatHabillage
     */
    public function setCote($cote)
    {
        $this->cote = $cote;

        return $this;
    }

    /**
     * Get cote.
     *
     * @return bool
     */
    public function getCote()
    {
        return $this->cote;
    }

    /**
     * Set itemTest.
     *
     * @param \App2Bundle\Entity\Tests\HabillageTest $itemTest
     *
     * @return ResultatHabillage
     */
    public function setItemTest(\App2Bundle\Entity\Tests\HabillageTest $itemTest)
    {
        $this->itemTest = $itemTest;

        return $this;
    }

    /**
     * Get itemTest.
     *
     * @return \App2Bundle\Entity\Tests\HabillageTest
     */
    public function getItemTest()
    {
        return $this->itemTest;
    }

    /**
     * Set bilan.
     *
     * @param \App2Bundle\Entity\Bilans\BilanHabillage $bilan
     *
     * @return ResultatHabillage
     */
    public function setBilan(\App2Bundle\Entity\Bilans\BilanHabillage $bilan)
    {
        $this->bilan = $bilan;

        return $this;
    }

    /**
     * Get bilan.
     *
     * @return \App2Bundle\Entity\Bilans\BilanHabillage
     */
    public function getBilan()
    {
        return $this->bilan;
    }
}
