<?php

namespace App2Bundle\Entity\Resultats;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * ResultatMusculaire
 *
 * @ORM\Table(name="resultat_musculaire")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Resultats\ResultatMusculaireRepository")
 */
class ResultatMusculaire
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Tests\Musculaire",inversedBy="resultats")
    * @ORM\JoinColumn(nullable=false)
    */
    private $itemTest;
   
    
    /**
     * @var string
     *
     * @ORM\Column(name="remarque", type="string", nullable= true)
     */
    private $remarque;

    /**
     * @var integer
     *
     * @ORM\Column(name="value", type="integer")
     */
    private $value;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cote", type="boolean")
     */
    private $cote;

     /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Bilans\BilanMusculaire",inversedBy="resultats")
    * @ORM\JoinColumn(nullable=false)
    */
    private $bilan;

     public function __construct()
    {
        // $this->kapandjis = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set remarque
     *
     * @param string $remarque
     *
     * @return ResultatMusculaire
     */
    public function setRemarque($remarque)
    {
        $this->remarque = $remarque;

        return $this;
    }

    /**
     * Get remarque
     *
     * @return string
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    /**
     * Set value
     *
     * @param integer $value
     *
     * @return ResultatMusculaire
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return integer
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set cote
     *
     * @param boolean $cote
     *
     * @return ResultatMusculaire
     */
    public function setCote($cote)
    {
        $this->cote = $cote;

        return $this;
    }

    /**
     * Get cote
     *
     * @return boolean
     */
    public function getCote()
    {
        return $this->cote;
    }

   

    /**
     * Set bilan
     *
     * @param \App2Bundle\Entity\Bilans\BilanMusculaire $bilan
     *
     * @return ResultatMusculaire
     */
    public function setBilan(\App2Bundle\Entity\Bilans\BilanMusculaire $bilan)
    {
        $this->bilan = $bilan;

        return $this;
    }

    /**
     * Get bilan
     *
     * @return \App2Bundle\Entity\Bilans\BilanMusculaire
     */
    public function getBilan()
    {
        return $this->bilan;
    }

    /**
     * Set itemTest
     *
     * @param \App2Bundle\Entity\Tests\Musculaire $itemTest
     *
     * @return ResultatMusculaire
     */
    public function setItemTest(\App2Bundle\Entity\Tests\Musculaire $itemTest)
    {
        $this->itemTest = $itemTest;

        return $this;
    }

    /**
     * Get itemTest
     *
     * @return \App2Bundle\Entity\Tests\Musculaire
     */
    public function getItemTest()
    {
        return $this->itemTest;
    }
}
