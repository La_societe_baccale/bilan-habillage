<?php

namespace App2Bundle\Entity\Resultats;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * ResultatKapandji
 *
 * @ORM\Table(name="resultat_kapandji")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Resultats\ResultatKapandjiRepository")
 */
class ResultatKapandji
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Tests\KapandjiPosition",inversedBy="resultats")
    * @ORM\JoinColumn(nullable=false)
    */
    private $itemTest;
   
    
    /**
     * @var string
     *
     * @ORM\Column(name="remarque", type="string", nullable= true)
     */
    private $remarque;

    /**
     * @var boolean
     *
     * @ORM\Column(name="value", type="boolean")
     */
    private $value;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cote", type="boolean")
     */
    private $cote;

     /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Bilans\BilanKapandji",inversedBy="resultats")
    * @ORM\JoinColumn(nullable=false)
    */
    private $bilan;

     public function __construct()
    {
        // $this->kapandjis = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set remarque
     *
     * @param string $remarque
     *
     * @return ResultatKapandji
     */
    public function setRemarque($remarque)
    {
        $this->remarque = $remarque;

        return $this;
    }

    /**
     * Get remarque
     *
     * @return string
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    /**
     * Set value
     *
     * @param boolean $value
     *
     * @return ResultatKapandji
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return boolean
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set cote
     *
     * @param boolean $cote
     *
     * @return ResultatKapandji
     */
    public function setCote($cote)
    {
        $this->cote = $cote;

        return $this;
    }

    /**
     * Get cote
     *
     * @return boolean
     */
    public function getCote()
    {
        return $this->cote;
    }

    

    /**
     * Set bilan
     *
     * @param \App2Bundle\Entity\Bilans\BilanKapandji $bilan
     *
     * @return ResultatKapandji
     */
    public function setBilan(\App2Bundle\Entity\Bilans\BilanKapandji $bilan)
    {
        $this->bilan = $bilan;

        return $this;
    }

    /**
     * Get bilan
     *
     * @return \App2Bundle\Entity\Bilans\BilanKapandji
     */
    public function getBilan()
    {
        return $this->bilan;
    }

    /**
     * Set itemTest
     *
     * @param \App2Bundle\Entity\Tests\KapandjiPosition $itemTest
     *
     * @return ResultatKapandji
     */
    public function setItemTest(\App2Bundle\Entity\Tests\KapandjiPosition $itemTest)
    {
        $this->itemTest = $itemTest;

        return $this;
    }

    /**
     * Get itemTest
     *
     * @return \App2Bundle\Entity\Tests\KapandjiPosition
     */
    public function getItemTest()
    {
        return $this->itemTest;
    }
}
