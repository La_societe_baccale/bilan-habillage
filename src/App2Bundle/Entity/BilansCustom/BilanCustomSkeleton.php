<?php

namespace App2Bundle\Entity\BilansCustom;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * BilanCustomSkeleton
 *
 * @ORM\Table(name="bilan_custom_skeleton")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\BilansCustom\BilanCustomSkeletonRepository")
 */
class BilanCustomSkeleton
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string")
     */
    private $nom;

   /**
    * @var \DateTime
    *
    * @ORM\Column(name="date_ajout", type="datetime")
    */
    private $dateAjout;

    /**
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\BilansCustom\BilanCustom", mappedBy="bilanSkeleton", cascade={"all"})
     */
    private $bilans;

    /**
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\BilansCustom\ItemTest", mappedBy="bilanSkeleton", cascade={"all"})
    */
    private $itemTests;

    /**
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\BilansCustom\SubTotal", mappedBy="bilanSkeleton", cascade={"all"})
    */
    private $subtotals;

    /**
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\BilansCustom\SubPart", mappedBy="bilanSkeleton", cascade={"all"})
    */
    private $subparts;

    /**
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\BilansCustom\SubText", mappedBy="bilanSkeleton", cascade={"all"})
    */
    private $subtexts;

    /**
     * @var string
     *
     * @ORM\Column(name="with_cote", type="boolean")
     */
    private $withCote;

    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Users\Organisation",inversedBy="bilanCustomSkeletons")
    * @ORM\JoinColumn(nullable=true)
    */
    private $organisation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable= false)
     */
    private $isActive;

     public function __construct()
    {
        $this->dateAjout = new \Datetime();
        $this->resultats = new ArrayCollection();
        $this->itemTests = new ArrayCollection();
        $this->subtotals = new ArrayCollection();

    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return BilanCustomSkeleton
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set dateAjout.
     *
     * @param \DateTime $dateAjout
     *
     * @return BilanCustomSkeleton
     */
    public function setDateAjout($dateAjout)
    {
        $this->dateAjout = $dateAjout;

        return $this;
    }

    /**
     * Get dateAjout.
     *
     * @return \DateTime
     */
    public function getDateAjout()
    {
        return $this->dateAjout;
    }

    /**
     * Add bilan.
     *
     * @param \App2Bundle\Entity\BilansCustom\BilanCustom $bilan
     *
     * @return BilanCustomSkeleton
     */
    public function addBilan(\App2Bundle\Entity\BilansCustom\BilanCustom $bilan)
    {
        $this->bilans[] = $bilan;

        return $this;
    }

    /**
     * Remove bilan.
     *
     * @param \App2Bundle\Entity\BilansCustom\BilanCustom $bilan
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeBilan(\App2Bundle\Entity\BilansCustom\BilanCustom $bilan)
    {
        return $this->bilans->removeElement($bilan);
    }

    /**
     * Get bilans.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBilans()
    {
        return $this->bilans;
    }

    /**
     * Add itemTest.
     *
     * @param \App2Bundle\Entity\BilansCustom\ItemTest $itemTest
     *
     * @return BilanCustomSkeleton
     */
    public function addItemTest(\App2Bundle\Entity\BilansCustom\ItemTest $itemTest)
    {
        $this->itemTests[] = $itemTest;

        return $this;
    }

    /**
     * Remove itemTest.
     *
     * @param \App2Bundle\Entity\BilansCustom\ItemTest $itemTest
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeItemTest(\App2Bundle\Entity\BilansCustom\ItemTest $itemTest)
    {
        return $this->itemTests->removeElement($itemTest);
    }

    /**
     * Remove all itemTest
     *
     * @return BilanCustomSkeleton
     */
    public function removeAllItemTests()
    {
        $this->itemTests = [];
        
        return $this;
    }


    /**
     * Get itemTests.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItemTests()
    {
        return $this->itemTests;
    }

    /**
     * Set withCote.
     *
     * @param bool $withCote
     *
     * @return BilanCustomSkeleton
     */
    public function setWithCote($withCote)
    {
        $this->withCote = $withCote;

        return $this;
    }

    /**
     * Get withCote.
     *
     * @return bool
     */
    public function getWithCote()
    {
        return $this->withCote;
    }

    /**
     * Add subtotal.
     *
     * @param \App2Bundle\Entity\BilansCustom\SubTotal $subtotal
     *
     * @return BilanCustomSkeleton
     */
    public function addSubtotal(\App2Bundle\Entity\BilansCustom\SubTotal $subtotal)
    {
        $this->subtotals[] = $subtotal;

        return $this;
    }

    /**
     * Remove subtotal.
     *
     * @param \App2Bundle\Entity\BilansCustom\SubTotal $subtotal
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSubtotal(\App2Bundle\Entity\BilansCustom\SubTotal $subtotal)
    {
        return $this->subtotals->removeElement($subtotal);
    }

    /**
     * Get subtotals.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubtotals()
    {
        return $this->subtotals;
    }

    /**
     * Remove all subtotal
     *
     * @return BilanCustomSkeleton
     */
    public function removeAllSubtotals()
    {
        $this->subtotals = [];
        
        return $this;
    }

    /**
     * Set organisation.
     *
     * @param \App2Bundle\Entity\Users\Organisation|null $organisation
     *
     * @return BilanCustomSkeleton
     */
    public function setOrganisation(\App2Bundle\Entity\Users\Organisation $organisation = null)
    {
        $this->organisation = $organisation;

        return $this;
    }

    /**
     * Get organisation.
     *
     * @return \App2Bundle\Entity\Users\Organisation|null
     */
    public function getOrganisation()
    {
        return $this->organisation;
    }

    /**
     * Add subpart.
     *
     * @param \App2Bundle\Entity\BilansCustom\SubPart $subpart
     *
     * @return BilanCustomSkeleton
     */
    public function addSubpart(\App2Bundle\Entity\BilansCustom\SubPart $subpart)
    {
        $this->subparts[] = $subpart;

        return $this;
    }

    /**
     * Remove subpart.
     *
     * @param \App2Bundle\Entity\BilansCustom\SubPart $subpart
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSubpart(\App2Bundle\Entity\BilansCustom\SubPart $subpart)
    {
        return $this->subparts->removeElement($subpart);
    }

    /**
     * Get subparts.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubparts()
    {
        return $this->subparts;
    }

    /**
     * Remove all subparts
     *
     * @return BilanCustomSkeleton
     */
    public function removeAllSubparts()
    {
        $this->subparts = [];
        
        return $this;
    }



    /**
     * Add subtext.
     *
     * @param \App2Bundle\Entity\BilansCustom\SubText $subtext
     *
     * @return BilanCustomSkeleton
     */
    public function addSubtext(\App2Bundle\Entity\BilansCustom\SubText $subtext)
    {
        $this->subtexts[] = $subtext;

        return $this;
    }

    /**
     * Remove subtext.
     *
     * @param \App2Bundle\Entity\BilansCustom\SubText $subtext
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSubtext(\App2Bundle\Entity\BilansCustom\SubText $subtext)
    {
        return $this->subtexts->removeElement($subtext);
    }

    /**
     * Get subtexts.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubtexts()
    {
        return $this->subtexts;
    }

    /**
     * Remove all subtexts
     *
     * @return BilanCustomSkeleton
     */
    public function removeAllSubtexts()
    {
        $this->subtexts = [];
        
        return $this;
    }

    /**
     * Set isActive.
     *
     * @param bool|null $isActive
     *
     * @return BilanCustomSkeleton
     */
    public function setIsActive($isActive = null)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive.
     *
     * @return bool|null
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
}
