<?php

namespace App2Bundle\Entity\BilansCustom;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * SubPart
 *
 * @ORM\Table(name="sub_part")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\BilansCustom\SubPartRepository")
 */
class SubPart
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string")
     */
    private $nom;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordre", type="integer")
     */
    private $ordre;

    /**
     * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\BilansCustom\BilanCustomSkeleton", inversedBy="subparts")
    */
    private $bilanSkeleton;
    
   

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return SubPart
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set ordre.
     *
     * @param int $ordre
     *
     * @return SubPart
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;

        return $this;
    }

    /**
     * Get ordre.
     *
     * @return int
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * Set bilanSkeleton.
     *
     * @param \App2Bundle\Entity\BilansCustom\BilanCustomSkeleton|null $bilanSkeleton
     *
     * @return SubPart
     */
    public function setBilanSkeleton(\App2Bundle\Entity\BilansCustom\BilanCustomSkeleton $bilanSkeleton = null)
    {
        $this->bilanSkeleton = $bilanSkeleton;

        return $this;
    }

    /**
     * Get bilanSkeleton.
     *
     * @return \App2Bundle\Entity\BilansCustom\BilanCustomSkeleton|null
     */
    public function getBilanSkeleton()
    {
        return $this->bilanSkeleton;
    }
}
