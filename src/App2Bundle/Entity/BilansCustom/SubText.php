<?php

namespace App2Bundle\Entity\BilansCustom;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * SubText
 *
 * @ORM\Table(name="sub_text")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\BilansCustom\SubTextRepository")
 */
class SubText
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string")
     */
    private $text;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordre", type="integer")
     */
    private $ordre;

    /**
     * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\BilansCustom\BilanCustomSkeleton", inversedBy="subtexts")
    */
    private $bilanSkeleton;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text.
     *
     * @param string $text
     *
     * @return SubText
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text.
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set ordre.
     *
     * @param int $ordre
     *
     * @return SubText
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;

        return $this;
    }

    /**
     * Get ordre.
     *
     * @return int
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * Set bilanSkeleton.
     *
     * @param \App2Bundle\Entity\BilansCustom\BilanCustomSkeleton|null $bilanSkeleton
     *
     * @return SubText
     */
    public function setBilanSkeleton(\App2Bundle\Entity\BilansCustom\BilanCustomSkeleton $bilanSkeleton = null)
    {
        $this->bilanSkeleton = $bilanSkeleton;

        return $this;
    }

    /**
     * Get bilanSkeleton.
     *
     * @return \App2Bundle\Entity\BilansCustom\BilanCustomSkeleton|null
     */
    public function getBilanSkeleton()
    {
        return $this->bilanSkeleton;
    }
}
