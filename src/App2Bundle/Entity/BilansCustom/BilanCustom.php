<?php

namespace App2Bundle\Entity\BilansCustom;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * BilanCustom
 *
 * @ORM\Table(name="bilan_custom")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\BilansCustom\BilanCustomRepository")
 */
class BilanCustom
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string")
     */
    private $nom;

   /**
    * @var \DateTime
    *
    * @ORM\Column(name="date_ajout", type="datetime")
    */
    private $dateAjout;

    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Patients\Patient",inversedBy="bilansCustom")
    * @ORM\JoinColumn(nullable=true)
    */
    private $patient;
    
    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\BilansCustom\ResultatCustom", mappedBy="bilan", cascade={"all"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $resultats;

    /**
     * 
     * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Bilans\Bilan", inversedBy="bilanCustom")
     * 
     */
    private $bilan;

    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\BilansCustom\BilanCustomSkeleton", inversedBy="bilans")
    */
    private $bilanSkeleton;

    

     public function __construct()
    {
        $this->dateAjout = new \Datetime();
        $this->resultats = new ArrayCollection();
    }


   

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return BilanCustom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set dateAjout.
     *
     * @param \DateTime $dateAjout
     *
     * @return BilanCustom
     */
    public function setDateAjout($dateAjout)
    {
        $this->dateAjout = $dateAjout;

        return $this;
    }

    /**
     * Get dateAjout.
     *
     * @return \DateTime
     */
    public function getDateAjout()
    {
        return $this->dateAjout;
    }

    /**
     * Set patient.
     *
     * @param \App2Bundle\Entity\Patients\Patient|null $patient
     *
     * @return BilanCustom
     */
    public function setPatient(\App2Bundle\Entity\Patients\Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient.
     *
     * @return \App2Bundle\Entity\Patients\Patient|null
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Add resultat.
     *
     * @param \App2Bundle\Entity\BilansCustom\ResultatCustom $resultat
     *
     * @return BilanCustom
     */
    public function addResultat(\App2Bundle\Entity\BilansCustom\ResultatCustom $resultat)
    {
        $this->resultats[] = $resultat;

        return $this;
    }

    /**
     * Remove resultat.
     *
     * @param \App2Bundle\Entity\BilansCustom\ResultatCustom $resultat
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeResultat(\App2Bundle\Entity\BilansCustom\ResultatCustom $resultat)
    {
        return $this->resultats->removeElement($resultat);
    }

    /**
     * Get resultats.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResultats()
    {
        return $this->resultats;
    }

    /**
     * Remove all resultats
     *
     * @return BilanCustom
     */
    public function removeAllResultats()
    {
        $this->resultats = [];
        
        return $this;
    }


    /**
     * Set bilan.
     *
     * @param \App2Bundle\Entity\Bilans\Bilan|null $bilan
     *
     * @return BilanCustom
     */
    public function setBilan(\App2Bundle\Entity\Bilans\Bilan $bilan = null)
    {
        $this->bilan = $bilan;

        return $this;
    }

    /**
     * Get bilan.
     *
     * @return \App2Bundle\Entity\Bilans\Bilan|null
     */
    public function getBilan()
    {
        return $this->bilan;
    }

    /**
     * Set bilanSkeleton.
     *
     * @param \App2Bundle\Entity\BilansCustom\BilanCustomSkeleton|null $bilanSkeleton
     *
     * @return BilanCustom
     */
    public function setBilanSkeleton(\App2Bundle\Entity\BilansCustom\BilanCustomSkeleton $bilanSkeleton = null)
    {
        $this->bilanSkeleton = $bilanSkeleton;

        return $this;
    }

    /**
     * Get bilanSkeleton.
     *
     * @return \App2Bundle\Entity\BilansCustom\BilanCustomSkeleton|null
     */
    public function getBilanSkeleton()
    {
        return $this->bilanSkeleton;
    }
}
