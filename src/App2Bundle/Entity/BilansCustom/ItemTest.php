<?php

namespace App2Bundle\Entity\BilansCustom;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * ItemTest
 *
 * @ORM\Table(name="item_test")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\BilansCustom\ItemTestRepository")
 */
class ItemTest
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string")
     */
    private $nom;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordre", type="integer")
     */
    private $ordre;

    /**
     * @var string
     *
     * @ORM\Column(name="type_value", type="string")
     */
    private $typeValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="min", type="integer", nullable=true)
     */
    private $min;

    /**
     * @var integer
     *
     * @ORM\Column(name="max", type="integer", nullable=true)
     */
    private $max;

    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\BilansCustom\ResultatCustom", mappedBy="itemTest")
     * @ORM\JoinColumn(nullable=true)
     */
    private $resultats;

    /**
     * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\BilansCustom\BilanCustomSkeleton", inversedBy="itemTests")
    */
    private $bilanSkeleton;
    

     public function __construct()
    {
        $this->resultats = new ArrayCollection();
    }
    

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return ItemTest
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set ordre.
     *
     * @param int $ordre
     *
     * @return ItemTest
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;

        return $this;
    }

    /**
     * Get ordre.
     *
     * @return int
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * Add resultat.
     *
     * @param \App2Bundle\Entity\BilansCustom\ResultatCustom $resultat
     *
     * @return ItemTest
     */
    public function addResultat(\App2Bundle\Entity\BilansCustom\ResultatCustom $resultat)
    {
        $this->resultats[] = $resultat;

        return $this;
    }

    /**
     * Remove resultat.
     *
     * @param \App2Bundle\Entity\BilansCustom\ResultatCustom $resultat
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeResultat(\App2Bundle\Entity\BilansCustom\ResultatCustom $resultat)
    {
        return $this->resultats->removeElement($resultat);
    }

    /**
     * Get resultats.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResultats()
    {
        return $this->resultats;
    }

    /**
     * Set bilanSkeleton.
     *
     * @param \App2Bundle\Entity\BilansCustom\BilanCustomSkeleton|null $bilanSkeleton
     *
     * @return ItemTest
     */
    public function setBilanSkeleton(\App2Bundle\Entity\BilansCustom\BilanCustomSkeleton $bilanSkeleton = null)
    {
        $this->bilanSkeleton = $bilanSkeleton;

        return $this;
    }

    /**
     * Get bilanSkeleton.
     *
     * @return \App2Bundle\Entity\BilansCustom\BilanCustomSkeleton|null
     */
    public function getBilanSkeleton()
    {
        return $this->bilanSkeleton;
    }

    /**
     * Set typeValue.
     *
     * @param string $typeValue
     *
     * @return ItemTest
     */
    public function setTypeValue($typeValue)
    {
        $this->typeValue = $typeValue;

        return $this;
    }

    /**
     * Get typeValue.
     *
     * @return string
     */
    public function getTypeValue()
    {
        return $this->typeValue;
    }

    /**
     * Set min.
     *
     * @param int|null $min
     *
     * @return ItemTest
     */
    public function setMin($min = null)
    {
        $this->min = $min;

        return $this;
    }

    /**
     * Get min.
     *
     * @return int|null
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * Set max.
     *
     * @param int|null $max
     *
     * @return ItemTest
     */
    public function setMax($max = null)
    {
        $this->max = $max;

        return $this;
    }

    /**
     * Get max.
     *
     * @return int|null
     */
    public function getMax()
    {
        return $this->max;
    }
}
