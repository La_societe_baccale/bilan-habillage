<?php

namespace App2Bundle\Entity\BilansCustom;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * ItemTest
 *
 * @ORM\Table(name="sub_total")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\BilansCustom\SubTotalRepository")
 */
class SubTotal
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string")
     */
    private $nom;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordre", type="integer")
     */
    private $ordre;

    /**
     * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\BilansCustom\BilanCustomSkeleton", inversedBy="subtotals")
    */
    private $bilanSkeleton;

    /**
     * @ORM\ManyToMany(targetEntity="App2Bundle\Entity\BilansCustom\ItemTest")
     * @ORM\JoinTable(name="subtotal_items",
     *      joinColumns={@ORM\JoinColumn(name="subtotal_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="item_id", referencedColumnName="id")}
     *      )
     */
    private $itemTests;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_full_total", type="boolean", nullable= true)
     */
    private $isFullTotal;
    

     public function __construct()
    {
        $this->itemTests = new ArrayCollection();
    }
    
    public function getMax()
    {
        
        $max = 0;
        if( $this->getIsFullTotal()){
            $items = $this->getBilanSkeleton()->getItemTests();
        }
        else{
            $items = $this->getItemTests();
        }
        foreach ($items as $item) {
            if($item->getTypeValue() == "integer")
            $max = $max + ($item->getMax()-$item->getMin());
        }
        return $max;
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return SubTotal
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set ordre.
     *
     * @param int $ordre
     *
     * @return SubTotal
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;

        return $this;
    }

    /**
     * Get ordre.
     *
     * @return int
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * Set bilanSkeleton.
     *
     * @param \App2Bundle\Entity\BilansCustom\BilanCustomSkeleton|null $bilanSkeleton
     *
     * @return SubTotal
     */
    public function setBilanSkeleton(\App2Bundle\Entity\BilansCustom\BilanCustomSkeleton $bilanSkeleton = null)
    {
        $this->bilanSkeleton = $bilanSkeleton;

        return $this;
    }

    /**
     * Get bilanSkeleton.
     *
     * @return \App2Bundle\Entity\BilansCustom\BilanCustomSkeleton|null
     */
    public function getBilanSkeleton()
    {
        return $this->bilanSkeleton;
    }

    /**
     * Add itemTest.
     *
     * @param \App2Bundle\Entity\BilansCustom\ItemTest $itemTest
     *
     * @return SubTotal
     */
    public function addItemTest(\App2Bundle\Entity\BilansCustom\ItemTest $itemTest)
    {
        $this->itemTests[] = $itemTest;

        return $this;
    }

    /**
     * Remove itemTest.
     *
     * @param \App2Bundle\Entity\BilansCustom\ItemTest $itemTest
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeItemTest(\App2Bundle\Entity\BilansCustom\ItemTest $itemTest)
    {
        return $this->itemTests->removeElement($itemTest);
    }

    /**
     * Get itemTests.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItemTests()
    {
        return $this->itemTests;
    }

    /**
     * Set isFullTotal.
     *
     * @param bool|null $isFullTotal
     *
     * @return SubTotal
     */
    public function setIsFullTotal($isFullTotal = null)
    {
        $this->isFullTotal = $isFullTotal;

        return $this;
    }

    /**
     * Get isFullTotal.
     *
     * @return bool|null
     */
    public function getIsFullTotal()
    {
        return $this->isFullTotal;
    }
}
