<?php

namespace App2Bundle\Entity\BilansCustom;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * ResultatCustom
 *
 * @ORM\Table(name="resultat_custom")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\BilansCustom\ResultatCustompository")
 */
class ResultatCustom
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\BilansCustom\ItemTest",inversedBy="resultats")
    * @ORM\JoinColumn(nullable=false)
    */
    private $itemTest;
    
    /**
     * @var string
     *
     * @ORM\Column(name="remarque", type="string", nullable= true)
     */
    private $remarque;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string")
     */
    private $value;


    /**
     * @var boolean
     *
     * @ORM\Column(name="cote", type="boolean", nullable= true)
     */
    private $cote;

    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\BilansCustom\BilanCustom" ,inversedBy="resultats")
    * @ORM\JoinColumn(nullable=false)
    */
    private $bilan;


     public function __construct()
    {

    }

    

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set remarque.
     *
     * @param string|null $remarque
     *
     * @return ResultatCustom
     */
    public function setRemarque($remarque = null)
    {
        $this->remarque = $remarque;

        return $this;
    }

    /**
     * Get remarque.
     *
     * @return string|null
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    

    /**
     * Set cote.
     *
     * @param bool|null $cote
     *
     * @return ResultatCustom
     */
    public function setCote($cote = null)
    {
        $this->cote = $cote;

        return $this;
    }

    /**
     * Get cote.
     *
     * @return bool|null
     */
    public function getCote()
    {
        return $this->cote;
    }

    /**
     * Set itemTest.
     *
     * @param \App2Bundle\Entity\BilansCustom\ItemTest $itemTest
     *
     * @return ResultatCustom
     */
    public function setItemTest(\App2Bundle\Entity\BilansCustom\ItemTest $itemTest)
    {
        $this->itemTest = $itemTest;

        return $this;
    }

    /**
     * Get itemTest.
     *
     * @return \App2Bundle\Entity\BilansCustom\ItemTest
     */
    public function getItemTest()
    {
        return $this->itemTest;
    }

    /**
     * Set bilan.
     *
     * @param \App2Bundle\Entity\BilansCustom\BilanCustom $bilan
     *
     * @return ResultatCustom
     */
    public function setBilan(\App2Bundle\Entity\BilansCustom\BilanCustom $bilan)
    {
        $this->bilan = $bilan;

        return $this;
    }

    /**
     * Get bilan.
     *
     * @return \App2Bundle\Entity\BilansCustom\BilanCustom
     */
    public function getBilan()
    {
        return $this->bilan;
    }

    /**
     * Set value.
     *
     * @param string $value
     *
     * @return ResultatCustom
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
}
