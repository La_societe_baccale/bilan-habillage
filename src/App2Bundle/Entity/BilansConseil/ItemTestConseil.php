<?php

namespace App2Bundle\Entity\BilansConseil;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * ItemTestConseil
 *
 * @ORM\Table(name="item_test_conseil")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\BilansConseil\ItemTestRepository")
 */
class ItemTestConseil
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="symptome", type="string")
     */
    private $symptome;

    /**
     * @var string
     *
     * @ORM\Column(name="partieCorp", type="string")
     */
    private $partieCorp;

    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\BilansConseil\ResultatConseil", mappedBy="itemTest")
     * @ORM\JoinColumn(nullable=true)
     */
    private $resultats;

    /**
     * 
     * @ORM\ManyToMany(targetEntity="App2Bundle\Entity\BilansConseil\Conseil", inversedBy="itemTests")
     */
    private $conseils;
    
    

     public function __construct()
    {
        $this->resultats = new ArrayCollection();
        $this->conseils = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set symptome.
     *
     * @param string $symptome
     *
     * @return ItemTestConseil
     */
    public function setSymptome($symptome)
    {
        $this->symptome = $symptome;

        return $this;
    }

    /**
     * Get symptome.
     *
     * @return string
     */
    public function getSymptome()
    {
        return $this->symptome;
    }

    /**
     * Set partieCorp.
     *
     * @param string $partieCorp
     *
     * @return ItemTestConseil
     */
    public function setPartieCorp($partieCorp)
    {
        $this->partieCorp = $partieCorp;

        return $this;
    }

    /**
     * Get partieCorp.
     *
     * @return string
     */
    public function getPartieCorp()
    {
        return $this->partieCorp;
    }

    /**
     * Add resultat.
     *
     * @param \App2Bundle\Entity\BilansConseil\ResultatConseil $resultat
     *
     * @return ItemTestConseil
     */
    public function addResultat(\App2Bundle\Entity\BilansConseil\ResultatConseil $resultat)
    {
        $this->resultats[] = $resultat;

        return $this;
    }

    /**
     * Remove resultat.
     *
     * @param \App2Bundle\Entity\BilansConseil\ResultatConseil $resultat
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeResultat(\App2Bundle\Entity\BilansConseil\ResultatConseil $resultat)
    {
        return $this->resultats->removeElement($resultat);
    }

    /**
     * Get resultats.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResultats()
    {
        return $this->resultats;
    }

    /**
     * Add conseil.
     *
     * @param \App2Bundle\Entity\BilansConseil\Conseil $conseil
     *
     * @return ItemTestConseil
     */
    public function addConseil(\App2Bundle\Entity\BilansConseil\Conseil $conseil)
    {
        $this->conseils[] = $conseil;

        return $this;
    }

    /**
     * Remove conseil.
     *
     * @param \App2Bundle\Entity\BilansConseil\Conseil $conseil
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeConseil(\App2Bundle\Entity\BilansConseil\Conseil $conseil)
    {
        return $this->conseils->removeElement($conseil);
    }

    /**
     * Get conseils.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConseils()
    {
        return $this->conseils;
    }
}
