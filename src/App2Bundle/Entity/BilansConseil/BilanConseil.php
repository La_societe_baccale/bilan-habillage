<?php

namespace App2Bundle\Entity\BilansConseil;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * BilanConseil
 *
 * @ORM\Table(name="bilan_conseil")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\BilansConseil\BilanConseilRepository")
 */
class BilanConseil
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string")
     */
    private $nom;

   /**
    * @var \DateTime
    *
    * @ORM\Column(name="date_ajout", type="datetime")
    */
    private $dateAjout;

    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Patients\Patient",inversedBy="bilansConseil")
    * @ORM\JoinColumn(nullable=true)
    */
    private $patient;
    
    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\BilansConseil\ResultatConseil", mappedBy="bilan", cascade={"all"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $resultats;

    /**
     * 
     * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Bilans\Bilan", inversedBy="bilanConseil")
     * 
     */
    private $bilan;

     public function __construct()
    {
        $this->dateAjout = new \Datetime();
        $this->resultats = new ArrayCollection();
    }


   
    /**
     * Remove all resultats
     *
     * @return BilanConseil
     */
    public function removeAllResultats()
    {
        $this->resultats = [];
        
        return $this;
    }

    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return BilanConseil
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set dateAjout.
     *
     * @param \DateTime $dateAjout
     *
     * @return BilanConseil
     */
    public function setDateAjout($dateAjout)
    {
        $this->dateAjout = $dateAjout;

        return $this;
    }

    /**
     * Get dateAjout.
     *
     * @return \DateTime
     */
    public function getDateAjout()
    {
        return $this->dateAjout;
    }

    /**
     * Set patient.
     *
     * @param \App2Bundle\Entity\Patients\Patient|null $patient
     *
     * @return BilanConseil
     */
    public function setPatient(\App2Bundle\Entity\Patients\Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient.
     *
     * @return \App2Bundle\Entity\Patients\Patient|null
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Add resultat.
     *
     * @param \App2Bundle\Entity\BilansConseil\ResultatConseil $resultat
     *
     * @return BilanConseil
     */
    public function addResultat(\App2Bundle\Entity\BilansConseil\ResultatConseil $resultat)
    {
        $this->resultats[] = $resultat;

        return $this;
    }

    /**
     * Remove resultat.
     *
     * @param \App2Bundle\Entity\BilansConseil\ResultatConseil $resultat
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeResultat(\App2Bundle\Entity\BilansConseil\ResultatConseil $resultat)
    {
        return $this->resultats->removeElement($resultat);
    }

    /**
     * Get resultats.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResultats()
    {
        return $this->resultats;
    }

    /**
     * Set bilan.
     *
     * @param \App2Bundle\Entity\Bilans\Bilan|null $bilan
     *
     * @return BilanConseil
     */
    public function setBilan(\App2Bundle\Entity\Bilans\Bilan $bilan = null)
    {
        $this->bilan = $bilan;

        return $this;
    }

    /**
     * Get bilan.
     *
     * @return \App2Bundle\Entity\Bilans\Bilan|null
     */
    public function getBilan()
    {
        return $this->bilan;
    }
}
