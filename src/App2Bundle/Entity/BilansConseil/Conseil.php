<?php

namespace App2Bundle\Entity\BilansConseil;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Conseil
 *
 * @ORM\Table(name="conseil")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\BilansConseil\ConseilRepository")
 */
class Conseil
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string")
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="texte", type="string")
     */
    private $texte;


    /**
     * 
     * @ORM\ManyToMany(targetEntity="App2Bundle\Entity\BilansConseil\ItemTestConseil", mappedBy="conseils", cascade={"persist"})
     */
    private $itemTests;

     public function __construct()
    {
        $this->itemTests = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Conseil
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set texte.
     *
     * @param string $texte
     *
     * @return Conseil
     */
    public function setTexte($texte)
    {
        $this->texte = $texte;

        return $this;
    }

    /**
     * Get texte.
     *
     * @return string
     */
    public function getTexte()
    {
        return $this->texte;
    }

    /**
     * Add itemTest.
     *
     * @param \App2Bundle\Entity\BilansConseil\ItemTestConseil $itemTest
     *
     * @return Conseil
     */
    public function addItemTest(\App2Bundle\Entity\BilansConseil\ItemTestConseil $itemTest)
    {
        $this->itemTests[] = $itemTest;

        return $this;
    }

    /**
     * Remove itemTest.
     *
     * @param \App2Bundle\Entity\BilansConseil\ItemTestConseil $itemTest
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeItemTest(\App2Bundle\Entity\BilansConseil\ItemTestConseil $itemTest)
    {
        return $this->itemTests->removeElement($itemTest);
    }

    /**
     * Get itemTests.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItemTests()
    {
        return $this->itemTests;
    }
}
