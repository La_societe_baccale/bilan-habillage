<?php

namespace App2Bundle\Entity\BilansConseil;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * ResultatConseil
 *
 * @ORM\Table(name="resultat_conseil")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\BilansConseil\ResultatConseilpository")
 */
class ResultatConseil
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\BilansConseil\ItemTestConseil",inversedBy="resultats")
    * @ORM\JoinColumn(nullable=false)
    */
    private $itemTest;
    
    /**
     * @var string
     *
     * @ORM\Column(name="remarque", type="string", nullable= true)
     */
    private $remarque;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string")
     */
    private $value;


    /**
     * @var boolean
     *
     * @ORM\Column(name="cote", type="boolean", nullable= true)
     */
    private $cote;

    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\BilansConseil\BilanConseil" ,inversedBy="resultats")
    * @ORM\JoinColumn(nullable=false)
    */
    private $bilan;


     public function __construct()
    {

    }

    

    

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set remarque.
     *
     * @param string|null $remarque
     *
     * @return ResultatConseil
     */
    public function setRemarque($remarque = null)
    {
        $this->remarque = $remarque;

        return $this;
    }

    /**
     * Get remarque.
     *
     * @return string|null
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    /**
     * Set value.
     *
     * @param string $value
     *
     * @return ResultatConseil
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set cote.
     *
     * @param bool|null $cote
     *
     * @return ResultatConseil
     */
    public function setCote($cote = null)
    {
        $this->cote = $cote;

        return $this;
    }

    /**
     * Get cote.
     *
     * @return bool|null
     */
    public function getCote()
    {
        return $this->cote;
    }

    /**
     * Set itemTest.
     *
     * @param \App2Bundle\Entity\BilansConseil\ItemTestConseil $itemTest
     *
     * @return ResultatConseil
     */
    public function setItemTest(\App2Bundle\Entity\BilansConseil\ItemTestConseil $itemTest)
    {
        $this->itemTest = $itemTest;

        return $this;
    }

    /**
     * Get itemTest.
     *
     * @return \App2Bundle\Entity\BilansConseil\ItemTestConseil
     */
    public function getItemTest()
    {
        return $this->itemTest;
    }

    /**
     * Set bilan.
     *
     * @param \App2Bundle\Entity\BilansConseil\BilanConseil $bilan
     *
     * @return ResultatConseil
     */
    public function setBilan(\App2Bundle\Entity\BilansConseil\BilanConseil $bilan)
    {
        $this->bilan = $bilan;

        return $this;
    }

    /**
     * Get bilan.
     *
     * @return \App2Bundle\Entity\BilansConseil\BilanConseil
     */
    public function getBilan()
    {
        return $this->bilan;
    }
}
