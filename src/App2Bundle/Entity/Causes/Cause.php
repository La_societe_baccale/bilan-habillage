<?php

namespace App2Bundle\Entity\Causes;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Causes
 *
 * @ORM\Table(name="cause")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Causes\CauseRepository")
 */
class Cause
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * 
     * @ORM\ManyToMany(targetEntity="App2Bundle\Entity\Resultats\ResultatArticulaire" , inversedBy="causes",cascade={"persist"})
     */
    private $resultatsArticulaire;

    

    public function __construct() {
        $this->resultatsArticulaire = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Cause
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    

    /**
     * Add resultatsArticulaire.
     *
     * @param \App2Bundle\Entity\Resultats\ResultatArticulaire $resultatsArticulaire
     *
     * @return Cause
     */
    public function addResultatsArticulaire(\App2Bundle\Entity\Resultats\ResultatArticulaire $resultatsArticulaire)
    {
        $this->resultatsArticulaire[] = $resultatsArticulaire;

        return $this;
    }

    /**
     * Remove resultatsArticulaire.
     *
     * @param \App2Bundle\Entity\Resultats\ResultatArticulaire $resultatsArticulaire
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeResultatsArticulaire(\App2Bundle\Entity\Resultats\ResultatArticulaire $resultatsArticulaire)
    {
        return $this->resultatsArticulaire->removeElement($resultatsArticulaire);
    }

    /**
     * Get resultatsArticulaire.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResultatsArticulaire()
    {
        return $this->resultatsArticulaire;
    }
}
