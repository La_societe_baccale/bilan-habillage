<?php

namespace App2Bundle\Entity\Enfilage;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Enfilage
 *
 * @ORM\Table(name="enfilage")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Enfilage\EnfilageRepository")
 */
class Enfilage
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", nullable=true)
     */
    private $nom;
    

    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\Enfilage\ConditionsEnfilages", mappedBy="enfilage", cascade={"all"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $conditionsenfilages;

     public function __construct()
    {
        $this->conditionsenfilages = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set value.
     *
     * @param int|null $value
     *
     * @return Enfilage
     */
    public function setValue($value = null)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return int|null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Add conditionsenfilage.
     *
     * @param \App2Bundle\Entity\Enfilage\ConditionsEnfilages $conditionsenfilage
     *
     * @return Enfilage
     */
    public function addConditionsenfilage(\App2Bundle\Entity\Enfilage\ConditionsEnfilages $conditionsenfilage)
    {
        $this->conditionsenfilages[] = $conditionsenfilage;

        return $this;
    }

    /**
     * Remove conditionsenfilage.
     *
     * @param \App2Bundle\Entity\Enfilage\ConditionsEnfilages $conditionsenfilage
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeConditionsenfilage(\App2Bundle\Entity\Enfilage\ConditionsEnfilages $conditionsenfilage)
    {
        return $this->conditionsenfilages->removeElement($conditionsenfilage);
    }

    /**
     * Get conditionsenfilages.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConditionsenfilages()
    {
        return $this->conditionsenfilages;
    }

    /**
     * Set nom.
     *
     * @param string|null $nom
     *
     * @return Enfilage
     */
    public function setNom($nom = null)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string|null
     */
    public function getNom()
    {
        return $this->nom;
    }
}
