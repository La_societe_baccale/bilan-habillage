<?php

namespace App2Bundle\Entity\Enfilage;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * ConditionsEnfilages
 *
 * @ORM\Table(name="conditions_enfilages")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Enfilage\ConditionsEnfilagesRepository")
 */
class ConditionsEnfilages
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var integer
     *
     * @ORM\Column(name="ordre", type="integer", nullable=true)
     */
    private $ordre;
    
    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Enfilage\ConditionBilan",inversedBy="conditionsenfilage")
    * @ORM\JoinColumn(nullable=false)
    */
    private $condition;

    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Enfilage\Enfilage",inversedBy="conditionsenfilage")
    * @ORM\JoinColumn(nullable=false)
    */
    private $enfilage;

     public function __construct()
    {
        // $this->kapandjis = new ArrayCollection();
    }


    

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ordre.
     *
     * @param int|null $ordre
     *
     * @return ConditionsEnfilages
     */
    public function setOrdre($ordre = null)
    {
        $this->ordre = $ordre;

        return $this;
    }

    /**
     * Get ordre.
     *
     * @return int|null
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * Set condition.
     *
     * @param \App2Bundle\Entity\Enfilage\ConditionBilan $condition
     *
     * @return ConditionsEnfilages
     */
    public function setCondition(\App2Bundle\Entity\Enfilage\ConditionBilan $condition)
    {
        $this->condition = $condition;

        return $this;
    }

    /**
     * Get condition.
     *
     * @return \App2Bundle\Entity\Enfilage\ConditionBilan
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * Set enfilage.
     *
     * @param \App2Bundle\Entity\Enfilage\Enfilage $enfilage
     *
     * @return ConditionsEnfilages
     */
    public function setEnfilage(\App2Bundle\Entity\Enfilage\Enfilage $enfilage)
    {
        $this->enfilage = $enfilage;

        return $this;
    }

    /**
     * Get enfilage.
     *
     * @return \App2Bundle\Entity\Enfilage\Enfilage
     */
    public function getEnfilage()
    {
        return $this->enfilage;
    }
}
