<?php

namespace App2Bundle\Entity\Enfilage;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Preconisation
 *
 * @ORM\Table(name="preconisation")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Enfilage\ConditionsEnfilagesRepository")
 */
class Preconisation
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", nullable=true)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="texte", type="string", nullable=true)
     */
    private $texte;
    
    /**
     * 
     * @ORM\ManyToMany(targetEntity="App2Bundle\Entity\Enfilage\ConditionBilan", mappedBy="preconisations", cascade={"persist"})
     */
    private $conditions;

     public function __construct()
    {
        $this->conditions = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set titre.
     *
     * @param string|null $titre
     *
     * @return Preconisation
     */
    public function setTitre($titre = null)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre.
     *
     * @return string|null
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set texte.
     *
     * @param string|null $texte
     *
     * @return Preconisation
     */
    public function setTexte($texte = null)
    {
        $this->texte = $texte;

        return $this;
    }

    /**
     * Get texte.
     *
     * @return string|null
     */
    public function getTexte()
    {
        return $this->texte;
    }

    // /**
    //  * Add condition.
    //  *
    //  * @param \App2Bundle\Entity\Enfilage\Condition $condition
    //  *
    //  * @return Preconisation
    //  */
    // public function addCondition(\App2Bundle\Entity\Enfilage\Condition $condition)
    // {
    //     $this->conditions[] = $condition;

    //     return $this;
    // }

    // /**
    //  * Remove condition.
    //  *
    //  * @param \App2Bundle\Entity\Enfilage\Condition $condition
    //  *
    //  * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
    //  */
    // public function removeCondition(\App2Bundle\Entity\Enfilage\Condition $condition)
    // {
    //     return $this->conditions->removeElement($condition);
    // }

    // /**
    //  * Get conditions.
    //  *
    //  * @return \Doctrine\Common\Collections\Collection
    //  */
    // public function getConditions()
    // {
    //     return $this->conditions;
    // }

    /**
     * Add condition.
     *
     * @param \App2Bundle\Entity\Enfilage\ConditionBilan $condition
     *
     * @return Preconisation
     */
    public function addCondition(\App2Bundle\Entity\Enfilage\ConditionBilan $condition)
    {
        $this->conditions[] = $condition;

        return $this;
    }

    /**
     * Remove condition.
     *
     * @param \App2Bundle\Entity\Enfilage\ConditionBilan $condition
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCondition(\App2Bundle\Entity\Enfilage\ConditionBilan $condition)
    {
        return $this->conditions->removeElement($condition);
    }

    /**
     * Get conditions.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConditions()
    {
        return $this->conditions;
    }
}
