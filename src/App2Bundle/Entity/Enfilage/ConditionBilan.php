<?php

namespace App2Bundle\Entity\Enfilage;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * ConditionBilan
 *
 * @ORM\Table(name="condition_bilan")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Enfilage\ConditionBilanRepository")
 */
class ConditionBilan
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="bilan", type="string")
     */
    private $bilan;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string")
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="articulation", type="string", nullable=true)
     */
    private $articulation;

     /**
     * @var string
     *
     * @ORM\Column(name="operateur", type="string")
     */
    private $operateur;

    /**
     * @var integer
     *
     * @ORM\Column(name="value", type="integer", nullable=true)
     */
    private $value;

    /**
     * 
     * @ORM\ManyToMany(targetEntity="App2Bundle\Entity\Enfilage\Preconisation", inversedBy="conditions")
     */
    private $preconisations;
    
    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\Enfilage\ConditionsEnfilages", mappedBy="condition", cascade={"all"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $conditionsenfilages;

     /**
     * 
     * @ORM\ManyToMany(targetEntity="App2Bundle\Entity\Enfilage\ConditionBilan", mappedBy="conditionsbilanFrom", cascade={"persist"})
     */
    private $implicationsFrom;

    /**
     * 
     * @ORM\ManyToMany(targetEntity="App2Bundle\Entity\Enfilage\ConditionBilan", mappedBy="conditionsbilanTo", cascade={"persist"})
     */
    private $implicationsTo;

     public function __construct()
    {
        $this->conditionsenfilages = new ArrayCollection();
        $this->preconisations = new ArrayCollection();
        $this->implicationsTo = new ArrayCollection();
        $this->implicationsFrom = new ArrayCollection();

    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Condition
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Set bilan.
     *
     * @param string $bilan
     *
     * @return Condition
     */
    public function setBilan($bilan)
    {
        $this->bilan = $bilan;

        return $this;
    }

    /**
     * Get bilan.
     *
     * @return string
     */
    public function getBilan()
    {
        return $this->bilan;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Condition
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }


    /**
     * Set value.
     *
     * @param int|null $value
     *
     * @return Condition
     */
    public function setValue($value = null)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return int|null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Add preconisation.
     *
     * @param \App2Bundle\Entity\Enfilage\Preconisation $preconisation
     *
     * @return Condition
     */
    public function addPreconisation(\App2Bundle\Entity\Enfilage\Preconisation $preconisation)
    {
        $this->preconisations[] = $preconisation;

        return $this;
    }

    /**
     * Remove preconisation.
     *
     * @param \App2Bundle\Entity\Enfilage\Preconisation $preconisation
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePreconisation(\App2Bundle\Entity\Enfilage\Preconisation $preconisation)
    {
        return $this->preconisations->removeElement($preconisation);
    }

    /**
     * Get preconisations.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPreconisations()
    {
        return $this->preconisations;
    }

    /**
     * Add conditionsenfilage.
     *
     * @param \App2Bundle\Entity\Enfilage\ConditionsEnfilages $conditionsenfilage
     *
     * @return Condition
     */
    public function addConditionsenfilage(\App2Bundle\Entity\Enfilage\ConditionsEnfilages $conditionsenfilage)
    {
        $this->conditionsenfilages[] = $conditionsenfilage;

        return $this;
    }

    /**
     * Remove conditionsenfilage.
     *
     * @param \App2Bundle\Entity\Enfilage\ConditionsEnfilages $conditionsenfilage
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeConditionsenfilage(\App2Bundle\Entity\Enfilage\ConditionsEnfilages $conditionsenfilage)
    {
        return $this->conditionsenfilages->removeElement($conditionsenfilage);
    }

    /**
     * Get conditionsenfilages.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConditionsenfilages()
    {
        return $this->conditionsenfilages;
    }

    /**
     * Set operateur.
     *
     * @param string $operateur
     *
     * @return Condition
     */
    public function setOperateur($operateur)
    {
        $this->operateur = $operateur;

        return $this;
    }

    /**
     * Get operateur.
     *
     * @return string
     */
    public function getOperateur()
    {
        return $this->operateur;
    }

    /**
     * Set articulation.
     *
     * @param string|null $articulation
     *
     * @return ConditionBilan
     */
    public function setArticulation($articulation = null)
    {
        $this->articulation = $articulation;

        return $this;
    }

    /**
     * Get articulation.
     *
     * @return string|null
     */
    public function getArticulation()
    {
        return $this->articulation;
    }

    /**
     * Add implication.
     *
     * @param \App2Bundle\Entity\Enfilage\ConditionBilan $implication
     *
     * @return ConditionBilan
     */
    public function addImplication(\App2Bundle\Entity\Enfilage\ConditionBilan $implication)
    {
        $this->implications[] = $implication;

        return $this;
    }

    /**
     * Remove implication.
     *
     * @param \App2Bundle\Entity\Enfilage\ConditionBilan $implication
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeImplication(\App2Bundle\Entity\Enfilage\ConditionBilan $implication)
    {
        return $this->implications->removeElement($implication);
    }

    /**
     * Get implications.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImplications()
    {
        return $this->implications;
    }

    /**
     * Add implicationsFrom.
     *
     * @param \App2Bundle\Entity\Enfilage\ConditionBilan $implicationsFrom
     *
     * @return ConditionBilan
     */
    public function addImplicationsFrom(\App2Bundle\Entity\Enfilage\ConditionBilan $implicationsFrom)
    {
        $this->implicationsFrom[] = $implicationsFrom;

        return $this;
    }

    /**
     * Remove implicationsFrom.
     *
     * @param \App2Bundle\Entity\Enfilage\ConditionBilan $implicationsFrom
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeImplicationsFrom(\App2Bundle\Entity\Enfilage\ConditionBilan $implicationsFrom)
    {
        return $this->implicationsFrom->removeElement($implicationsFrom);
    }

    /**
     * Get implicationsFrom.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImplicationsFrom()
    {
        return $this->implicationsFrom;
    }

    /**
     * Add implicationsTo.
     *
     * @param \App2Bundle\Entity\Enfilage\ConditionBilan $implicationsTo
     *
     * @return ConditionBilan
     */
    public function addImplicationsTo(\App2Bundle\Entity\Enfilage\ConditionBilan $implicationsTo)
    {
        $this->implicationsTo[] = $implicationsTo;

        return $this;
    }

    /**
     * Remove implicationsTo.
     *
     * @param \App2Bundle\Entity\Enfilage\ConditionBilan $implicationsTo
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeImplicationsTo(\App2Bundle\Entity\Enfilage\ConditionBilan $implicationsTo)
    {
        return $this->implicationsTo->removeElement($implicationsTo);
    }

    /**
     * Get implicationsTo.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImplicationsTo()
    {
        return $this->implicationsTo;
    }

    /**
     * Set testId.
     *
     * @param int $testId
     *
     * @return ConditionBilan
     */
    public function setTestId($testId)
    {
        $this->testId = $testId;

        return $this;
    }

    /**
     * Get testId.
     *
     * @return int
     */
    public function getTestId()
    {
        return $this->testId;
    }
}
