<?php

namespace App2Bundle\Entity\Users;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Organisation
 *
 * @ORM\Table(name="Organisation")
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Users\OrganisationRepository")
 */
class Organisation
{   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string")
     */
    private $nom;

    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\Users\User", mappedBy="organisation", cascade={"all"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $users;

    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\BilansCustom\BilanCustomSkeleton", mappedBy="organisation", cascade={"all"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $bilanCustomSkeletons;


     public function __construct()
    {
        $this->users = new ArrayCollection();
        // $this->kapandjis = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Organisation
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Add user
     *
     * @param \App2Bundle\Entity\Users\User $user
     *
     * @return Organisation
     */
    public function addUser(\App2Bundle\Entity\Users\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \App2Bundle\Entity\Users\User $user
     */
    public function removeUser(\App2Bundle\Entity\Users\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add bilanCustomSkeleton.
     *
     * @param \App2Bundle\Entity\BilansCustom\BilanCustomSkeleton $bilanCustomSkeleton
     *
     * @return Organisation
     */
    public function addBilanCustomSkeleton(\App2Bundle\Entity\BilansCustom\BilanCustomSkeleton $bilanCustomSkeleton)
    {
        $this->bilanCustomSkeletons[] = $bilanCustomSkeleton;

        return $this;
    }

    /**
     * Remove bilanCustomSkeleton.
     *
     * @param \App2Bundle\Entity\BilansCustom\BilanCustomSkeleton $bilanCustomSkeleton
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeBilanCustomSkeleton(\App2Bundle\Entity\BilansCustom\BilanCustomSkeleton $bilanCustomSkeleton)
    {
        return $this->bilanCustomSkeletons->removeElement($bilanCustomSkeleton);
    }

    /**
     * Get bilanCustomSkeletons.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBilanCustomSkeletons()
    {
        return $this->bilanCustomSkeletons;
    }
}
