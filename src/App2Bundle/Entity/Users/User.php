<?php

namespace App2Bundle\Entity\Users;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity(repositoryClass="App2Bundle\Repository\Users\UserRepository")
 * @ORM\Table(name="`user`")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
    * @ORM\ManyToOne(targetEntity="App2Bundle\Entity\Users\Organisation",inversedBy="users")
    * @ORM\JoinColumn(nullable=true)
    */
    private $organisation;

    /**
     * 
     * @ORM\OneToMany(targetEntity="App2Bundle\Entity\Patients\Patient", mappedBy="user", cascade={"all"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $patients;

    public function __construct()
    {
        $this->patients = new ArrayCollection();
        parent::__construct();
        // your own logic
    }

    /**
     * Set organisation
     *
     * @param \App2Bundle\Entity\Users\Organisation $organisation
     *
     * @return User
     */
    public function setOrganisation($organisation)
    {
        $this->organisation = $organisation;

        return $this;
    }

    /**
     * Get organisation
     *
     * @return \App2Bundle\Entity\Users\Organisation
     */
    public function getOrganisation()
    {
        return $this->organisation;
    }

   

    /**
     * Add patient
     *
     * @param \App2Bundle\Entity\Patients\Patient $patient
     *
     * @return User
     */
    public function addPatient(\App2Bundle\Entity\Patients\Patient $patient)
    {
        $this->patients[] = $patient;

        return $this;
    }

    /**
     * Remove patient
     *
     * @param \App2Bundle\Entity\Patients\Patient $patient
     */
    public function removePatient(\App2Bundle\Entity\Patients\Patient $patient)
    {
        $this->patients->removeElement($patient);
    }

    /**
     * Get patients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPatients()
    {
        return $this->patients;
    }
}
