<?php

namespace App2Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App2Bundle\Entity\Patients\Patient;
use App2Bundle\Entity\Tests\GesteTest;
use App2Bundle\Entity\Bilans\Bilan;
use App2Bundle\Form\Bilans\BilanType;
use App2Bundle\Entity\Bilans\BilanHabillage;
use App2Bundle\Form\Bilans\BilanHabillageType;
use App2Bundle\Entity\Tests\HabillageTest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

 

class BilanHabillageController extends Controller
{
    public function createAction(Request $request, $idPatient)
    {
        $em = $this->getDoctrine()->getManager();
        $patient = $em->getRepository(Patient::class)
            ->find($idPatient);
        
        if($request->query->has('idBilan')){
            $bilan = ($_GET["idBilan"])? $em->getRepository(Bilan::class)->find($_GET["idBilan"]):null;
        }
        else{
            $bilan = null;
        }

        $testsHabillage = $this->getDoctrine()
            ->getRepository(HabillageTest::class)
            ->findAll();
        $bilanHabillage = new BilanHabillage();
        $form = $this->createForm(BilanHabillageType::class, $bilanHabillage, array(
        ));
        $form->add('submit', SubmitType::class, array(
            'label' => 'Créer',
            'attr'  => array('class' => 'btn btn-primary'),
        ));
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $resultats = $bilanHabillage->getResultats();
                $bilanHabillage->removeAllResultats();
                 
                if(!is_null($bilan)){
                    $bilanHabillage->setBilan($bilan);
                    $bilanHabillage->setNom($bilan->getNom()." : kapandji");
                }
                else{
                    $bilanHabillage->setPatient($patient);
                    $bilanHabillage->setNom($bilanHabillage->getDateAjout()->format('Y-m-d H:i:s'));
                }
                $em->persist($bilanHabillage);
                foreach ($resultats as $resultat ) {
                    $resultat->setBilan($bilanHabillage);
                    $bilanHabillage->addResultat($resultat);
                }
                $em->persist($bilanHabillage);
                $em->flush();

                return $this->render('App2Bundle:Bilans:get_one.html.twig', array(
                    'bilan' => $bilanHabillage,
                    'type' => 'Habillage',
                    'componentResultat' => "bilanHabillageResultat",
                    'paths' => ['extract'=>"app2_bilan_habillage_extract", "modify"=>"app2_bilan_habillage_update"]
                ));  
                // return $this->render('App2Bundle:BilansHabillage:resultat_habillage.html.twig', array(
                //     'bilanHabillage' => $bilanHabillage
                // ));
        
            }
        }

        return $this->render('App2Bundle:BilansHabillage:create_habillage.html.twig', array(
            'patient' => $patient,
            'form' => $form->createView(),
            'testsHabillage' => $testsHabillage,
            'heure' => new \Datetime()

        ));    
    }
    public function updateAction(Request $request, $idBilan)
    {
        $em = $this->getDoctrine()->getManager();
        $bilanHabillage = $em->getRepository(BilanHabillage::class)
        ->find($idBilan);
        $patient = $bilanHabillage->getPatient();
        
        $testsHabillage = $this->getDoctrine()
            ->getRepository(HabillageTest::class)
            ->findAll();
        $testsHabillageWithValues = [];
        $resultats = $bilanHabillage->getResultats();
        foreach ($testsHabillage as $testHabillage) {
                    $nom = $testHabillage->getNom(); 
        
                    $valueG = null;
                    $valueD = null;
                    $remarqueG = null;
                    $remarqueD = null;
                    //On garde la premiere condition qui correspond au geste test et au bon côté
                    $i = 0;
                    foreach ($resultats as  $resultat) {
                        if ($resultat->getItemTest()->getNom() == $nom && $resultat->getCote() == 0) {
                            $valueG = $resultat->getValue();
                            $remarqueG = $resultat->getRemarque();
                            $i = $i + 1;
                        }
                        if ($resultat->getItemTest()->getNom() == $nom && $resultat->getCote() == 1) {
                            $valueD = $resultat->getValue();
                            $remarqueD = $resultat->getRemarque();
                            $i = $i + 1;
                        }
                        if($i == 2 ){
                            break;
                        }
                    }
                    
                    $testHabillageWithValues = [
                        "nom"=>$nom,
                        "descriptif"=>$resultat->getItemTest()->getDescriptif(),
                        "id"=>$testHabillage->getId(),
                        "value_g"=>(is_null($valueG)) ? null : $valueG,
                        "value_d"=>(is_null($valueD)) ? null : $valueD,
                        "remarque_g"=>(is_null($remarqueG)) ? null : $remarqueG,
                        "remarque_d"=>(is_null($remarqueD)) ? null : $remarqueD
                    ];
                    $testsHabillageWithValues[] = $testHabillageWithValues;
            }

        $form = $this->createForm(BilanHabillageType::class, $bilanHabillage, array(
        ));
        $form->add('submit', SubmitType::class, array(
            'label' => 'Créer',
            'attr'  => array('class' => 'btn btn-primary'),
        ));
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $resultats = $bilanHabillage->getResultats();
                $bilanHabillage->removeAllResultats();
                $em->persist($bilanHabillage);
                foreach ($resultats as $resultat ) {
                    $resultat->setBilan($bilanHabillage);
                    $bilanHabillage->addResultat($resultat);
                }
                $em->persist($bilanHabillage);
                $em->flush();


                return $this->render('App2Bundle:Bilans:get_one.html.twig', array(
                    'bilan' => $bilanHabillage,
                    'type' => 'Habillage',
                    'componentResultat' => "bilanHabillageResultat",
                    'paths' => ['extract'=>"app2_bilan_habillage_extract", "modify"=>"app2_bilan_habillage_update"]
                ));  
        
            }
        }

        return $this->render('App2Bundle:BilansHabillage:create_habillage.html.twig', array(
            'patient' => $patient,
            'form' => $form->createView(),
            'testsHabillage' => $testsHabillageWithValues
        ));    
    }
    public function getOneAction($idBilan)
    {        
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $bilanHabillage = $em
            ->getRepository(BilanHabillage::class)
            ->find($idBilan);
        
        return $this->render('App2Bundle:Bilans:get_one.html.twig', array(
                'bilan' => $bilanHabillage,
                'type' => 'Habillage',
                'componentResultat' => "bilanHabillageResultat",
                'paths' => ['extract'=>"app2_bilan_habillage_extract", "modify"=>"app2_bilan_habillage_update"]
            ));   
    }
    public function extractAction($idBilan)
    {        
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $bilanHabillage = $em
        ->getRepository(BilanHabillage::class)
        ->find($idBilan);
    
        $resultats = $bilanHabillage->getResultats();
        $affichage = [];
        foreach($resultats as $resultat){
            $affichage[]= [ 
                "item_nom"=> $resultat->getItemTest()->getDescriptif(),
                "cote"=> ($resultat->getCote() == "0") ? "gauche" : "droite",
                "valeur"=> ($resultat->getValue() == true) ? "oui" : "non",
                "remarque"=> ($resultat->getRemarque()) ];
        }
        return $this->render('App2Bundle:BilansGesteTest:extract.html.twig', array(
                'bilan' => $bilanHabillage,
                'type' => "Habillage",
                'text_btn_success' => "Afficher les item réussis",
                'affichage'=> $affichage
            ));    
    }

    public function deleteAction($idBilan)
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $bilan = $em
            ->getRepository(BilanHabillage::class)
            ->find($idBilan);
        if (!$bilan) {
            throw $this->createNotFoundException('Pas de bilan trouvé');
        }

        $patient = $bilan->getPatient();
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($bilan);
        $em->flush();
    
        return $this->redirectToRoute('app2_get_one_patient', array('idPatient' => $patient->getId()));
    }

}
