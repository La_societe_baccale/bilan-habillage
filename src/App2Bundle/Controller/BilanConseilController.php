<?php

namespace App2Bundle\Controller;

use App2Bundle\Entity\BilansConseil\BilanConseil;
use App2Bundle\Entity\BilansConseil\ItemTestConseil;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App2Bundle\Form\BilansConseil\BilanConseilType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use App2Bundle\Entity\Patients\Patient;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App2Bundle\Repository\BilansConseil\BilanConseilRepository;



class BilanConseilController extends Controller
{
    public function deleteAction($idBilan)
    {   
        $em = $this->getDoctrine()->getManager();
        $bilanConseil = $em
            ->getRepository(BilanConseil::class)
            ->find($idBilan);
        if (!$bilanConseil) {
            throw $this->createNotFoundException('Pas de bilan trouvé');
        }

        $patient = $bilanConseil->getPatient();
        $em->remove($bilanConseil);
        $em->flush();
    
        return $this->redirectToRoute('app2_get_one_patient', array('idPatient' => $patient->getId()));
    }

    public function getOneAction($idBilan)
    {        
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        
        return $this->render('App2Bundle:BilansConseil:get_one.html.twig', array(
                    'bilan' => $bilanConseil,
                ));   
    }

    public function createAction(Request $request, $idPatient)
    {
        $em = $this->getDoctrine()->getManager();
        $patient = $em->getRepository(Patient::class)
            ->find($idPatient);
        $patient = $em->getRepository(Patient::class)
            ->find($idPatient);
        $itemTests = $this->getDoctrine()
            ->getRepository(ItemTestConseil::class)
            ->findAll();
    
        $bilanConseil = new BilanConseil();
        
        $itemTestToDisplay =  array("sans_partie_corp"=>[]);;

        foreach ($itemTests as $itemTest) {
            if($itemTest->getPartieCorp() == null || $itemTest->getPartieCorp() == ""  ){
                array_push($itemTestToDisplay["sans_partie_corp"],$itemTest);
            }
            else{
                if(array_key_exists($itemTest->getPartieCorp(), $itemTestToDisplay)){
                    array_push($itemTestToDisplay[$itemTest->getPartieCorp()],$itemTest);
                }
                else{
                    $itemTestToDisplay[$itemTest->getPartieCorp()] = [$itemTest];
                }
            }
        }
        
        foreach ($itemTestToDisplay as $key => $value ) {
            // sort($itemTestToDisplay[$key]);
            usort($itemTestToDisplay[$key], function($a, $b) {return ($a->getSymptome() > $b->getSymptome())? +1 : -1;});

        }
       
        $form = $this->createForm(BilanConseilType::class, $bilanConseil, array(
        ));
        $form->add('submit', SubmitType::class, array(
            'label' => 'Créer',
            'attr'  => array('class' => 'btn btn-primary'),
        ));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $resultats = $bilanConseil->getResultats();
                $bilanConseil->removeAllResultats();
               
                $bilanConseil->setPatient($patient);
                $bilanConseil->setNom($bilanConseil->getDateAjout()->format('Y-m-d H:i:s'));
                
                $em->persist($bilanConseil);
                foreach ($resultats as $resultat ) {
                    $resultat->setBilan($bilanConseil);
                    $bilanConseil->addResultat($resultat);
                }
                $em->persist($bilanConseil);
                $em->flush();
                $listConseil = [];
                foreach ($resultats as $resultat) {
                    if($resultat->getValue() == true){
                        $conseils = $resultat->getItemTest()->getConseils();
                        foreach ($conseils as $conseil) {
                            $listConseil[$conseil->getId()] = $conseil->getTexte();
                        }
                    }
                }

                return $this->render('App2Bundle:BilansConseil:get_one.html.twig', array(
                    'bilan' => $bilanConseil,
                    'listConseil' => $listConseil
                )); 
                
            }
        }

        return $this->render('App2Bundle:BilansConseil:create.html.twig', array(
            'form' => $form->createView(),
            'itemTestToDisplay' => $itemTestToDisplay
        ));    
    }
}
