<?php

namespace App2Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App2Bundle\Entity\Patients\Patient;
use App2Bundle\Entity\Tests\Articulaire;
use App2Bundle\Entity\Causes\Cause;
use App2Bundle\Entity\Bilans\Bilan;
use App2Bundle\Form\Bilans\BilanType;
use App2Bundle\Entity\Bilans\BilanGesteTest;
use App2Bundle\Entity\Bilans\BilanArticulaire;
use App2Bundle\Form\Bilans\BilanArticulaireType;
use App2Bundle\Entity\Tests\KapandjiPosition;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use App2Bundle\Form\Bilans\BilanForCreateBeforeType;


class BilanArticulaireController extends Controller
{
    //Controller pour que l'utilisateur puisse choisir les bilans sur lesquels baser son nouveau bilan
    public function createBeforeAction(Request $request, $idPatient)
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $patient = $em->getRepository(Patient::class)
            ->find($idPatient);
        
        $BilanFromImplicationGenerator = $this->container->get('app.bilanfromimplicationgenerator');
        //listeBilan la liste des noms de bilans pour pouvoir construire le formulaire sous la forme ["musculaire","articulaire"] 
        //sans le nom du formulaire sur lequel on travaille
        $listeBilan = $BilanFromImplicationGenerator->getBilansFrom($idPatient,"articulaire");
        //Si pas de bilan From on passe à la création du bilan
        if($listeBilan == false){
            $options = array('idPatient' => $patient->getId());
            return $this->redirectToRoute('app2_bilan_articulaire_create', $options);  
        }
        //On regarde s'il s'agit d'un bilan global
        if($request->query->has('idBilan')){
            $bilan = ($_GET["idBilan"])? $em->getRepository(Bilan::class)->find($_GET["idBilan"]):null;
        }
        else{
            $bilan = null;
        }
        
        //On crée le formulaire 
        $form = $this->createForm(BilanForCreateBeforeType::class,null,[
            "id_patient" => $idPatient,
            "liste_bilans" => $listeBilan 
        ]);
        //Le formulaire est vide on passe à la création de bilan
        if(count($form->all()) == 1){
            $options = array('idPatient' => $patient->getId());
            return $this->redirectToRoute('app2_bilan_articulaire_create', $options);  
        }
        //On analyse la réponse  
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) 
            {
                $data = $form->getData();
                $j = 0;
                $bilans = [];
                $bilans_ignore = [];
                //On stocke les noms des bilan ignorés en enlevant les 8 premier caractère de l'inpu correspondant à "ignore_"
                //$bilans_ignore est donc une liste sous la forme ["musculaire","articulaire"]
                foreach ($data as $key => $value){
                    if (stripos($key, 'ignorer') !== true && $value === false) {
                        $bilans_ignore[] = substr($key, 8); 
                    }
                }
                //On stocke les noms des bilan ignorés en enlevant les 8 premier caractère de l'inpu correspondant à "bilan_"
                //$bilans est donc une liste sous la forme ["musculaire"=>idbilan,"articulaire"=>idbilan]
                foreach ($data as $key => $value){
                    if (stripos($key, 'bilan') !== false && in_array(substr($key, 6),$bilans_ignore)) {
                        $bilans[$key] = $value;
                    }
                }
                //Comme options pour la route suivant on envoie l'id du patient et les bilans from sous la forme
                //['idPatient' =>idpatient, "bilan_musculaire"=>idbilan,"bilan_articulaire"=>idbilan]
                //On garde les bilan_ au début des variables pour les identifier po-lus facilement ensuite
                $options = array('idPatient' => $patient->getId());
                $options = array_merge($options,$bilans);

                if(!is_null($bilan)){
                    $options = array_merge($options,array('idBilan' => $bilan->getId()));
                }
                return $this->redirectToRoute('app2_bilan_articulaire_create', $options);  
            }                 
        }
        return $this->render('App2Bundle:Layout:jumbotron_classic_form.html.twig', array(
            'form' => $form->createView(),
        ));   
    }

    public function createAction(Request $request, $idPatient)
    {
        $em = $this->getDoctrine()->getManager();
        $patient = $em->getRepository(Patient::class)
            ->find($idPatient);
        $causes = $em->getRepository(Cause::class)
            ->findAll();
        //on regarde si ce bilan fait parti d'un bilan global
        if($request->query->has('idBilan')){
            $bilan = ($_GET["idBilan"])? $em->getRepository(Bilan::class)->find($_GET["idBilan"]):null;
        }
        else{
            $bilan = null;
        }
        
        $bilanArticulaire = new BilanArticulaire();
        
        $BilanFromImplicationGenerator = $this->container->get('app.bilanfromimplicationgenerator');
        //arrayIdBilansFrom ressemblera à ["musculaire"=>2,...] on vire le "bilan_"
        $arrayIdBilansFrom = $BilanFromImplicationGenerator->handleQuery($request->query->all());
        $implicationTo = $BilanFromImplicationGenerator->getAllImplicationsFrom("articulaire",$arrayIdBilansFrom);

        $articulaires = $em 
            ->getRepository(Articulaire::class)
            ->findAll();
        //On instancit le tableau affichage 
        $affichage = array(
            'épaule'=> array(),
            'coude'=> array()
        );
        foreach ($articulaires as $articulaire) {  
            $nom = $articulaire->getNom(); 
            $articulation = $articulaire->getArticulation();
            $conditionG = null;
            $conditionD = null;
            //On garde la premiere condition qui correspond au geste test et au bon côté
            foreach ($implicationTo as  $value) {
                if ($value["articulation"] == $articulation && $value["nom"] == $nom && $value["cote"] == 0) {
                    $conditionG = $value;
                    break;
                }
            }
            foreach ($implicationTo as  $value) {
                if ($value["articulation"] == $articulation && $value["nom"] == $nom && $value["cote"] == 1) {
                    $conditionD = $value;
                    break;
                }
            }           
                $articulaireWithConditions = [
                    "nom"=>$nom,
                    "id"=>$articulaire->getId(),
                    "angle_max" => $articulaire->getAngleMax(),
                    "condition_g"=>(is_null($conditionG)) ? null : $conditionG,
                    "condition_d"=>(is_null($conditionD)) ? null : $conditionD
                ];
                array_push($affichage[$articulation], $articulaireWithConditions);

        }

        $form = $this->createForm(BilanArticulaireType::class, $bilanArticulaire, array(
        ));
        $form->add('submit', SubmitType::class, array(
            'label' => 'Créer',
            'attr'  => array('class' => 'btn btn-primary'),
        ));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $resultats = $bilanArticulaire->getResultats();
                $bilanArticulaire->removeAllResultats();
                if(!is_null($bilan)){
                    $bilanArticulaire->setBilan($bilan);
                    $bilanArticulaire->setNom($bilan->getNom()." : articulaire");
                }
                else{
                    $bilanArticulaire->setPatient($patient);
                    $bilanArticulaire->setNom($bilanArticulaire->getDateAjout()->format('Y-m-d H:i:s'));
                }
                $em->persist($bilanArticulaire);

                foreach ($resultats as $resultat ) {
                    if(is_null($resultat->getValue())){
                        $resultat->setValue($resultat->getItemTest()->getAngleMax());
                    }
                    $causes = $resultat->getCauses();
                    $resultat->removeAllCauses();
                    $resultat->setBilan($bilanArticulaire);
                    $bilanArticulaire->addResultat($resultat);
                    $em->persist($bilanArticulaire);
                    foreach ($causes as $cause) {
                        $cause->addResultatsArticulaire($resultat);
                        $resultat->addCause($cause);
                    }
                }
                $em->persist($bilanArticulaire);
                $em->flush();
                
                return $this->render('App2Bundle:Bilans:get_one.html.twig', array(
                    'bilan' => $bilanArticulaire,
                    'type' => 'Articulaire',
                    'componentResultat' => "bilanArticulaireResultat",
                    'paths' => ['extract'=>"app2_bilan_articulaire_extract", "modify"=>"app2_bilan_articulaire_update"]
                ));    
            }
        }

        return $this->render('App2Bundle:BilansArticulaire:create.html.twig', array(
            'form' => $form->createView(),
            'articulairesReFormat' => $affichage,
            'causes'=> $causes,
            'patient' => $patient,
            'global' => $bilan,
            'heure' => new \Datetime()

        ));    
    }

    public function updateAction(Request $request, $idBilan)
    {
        $em = $this->getDoctrine()->getManager();
        $bilanArticulaire = $em->getRepository(BilanArticulaire::class)
            ->find($idBilan);
        $patient = $bilanArticulaire->getPatient();
        
        
        $causes = $em->getRepository(Cause::class)
            ->findAll();
        
        $articulaires = $em 
            ->getRepository(Articulaire::class)
            ->findAll();
        //On instancit le tableau affichage 
        $affichage = array(
            'épaule'=> array(),
            'coude'=> array()
        );
        $resultats = $bilanArticulaire->getResultats();
        foreach ($articulaires as $articulaire) {  
            $nom = $articulaire->getNom(); 
            $articulation = $articulaire->getArticulation();
            $valueG = null;
            $valueD = null;
            $remarqueG = null;
            $remarqueD = null;
            $causesG = [];
            $causesD = [];
            //On garde la premiere condition qui correspond au geste test et au bon côté
            $i =0;
            foreach ($resultats as  $resultat) {
                if ($resultat->getItemTest()->getArticulation() == $articulation && $resultat->getItemTest()->getNom() == $nom && $resultat->getCote() == 0) {
                    $valueG = $resultat->getValue();
                    $remarqueG = $resultat->getRemarque();
                    foreach ($resultat->getCauses() as $cause) {
                        $causesG[] = $cause->getId();
                    }
                    $i = $i + 1;
                }
                if ($resultat->getItemTest()->getArticulation() == $articulation && $resultat->getItemTest()->getNom() == $nom && $resultat->getCote() == 1) {
                    $valueD = $resultat->getValue();
                    $remarqueD = $resultat->getRemarque();
                    foreach ($resultat->getCauses() as $cause) {
                        $causesD[] = $cause->getId();
                    }
                    $i = $i + 1;
                }
                if($i == 2){
                  break;
                }
            }           
                $articulaireWithConditions = [
                    "nom"=>$nom,
                    "id"=>$articulaire->getId(),
                    "angle_max" => $articulaire->getAngleMax(),
                    "value_g"=>(is_null($valueG)) ? null : $valueG,
                    "value_d"=>(is_null($valueD)) ? null : $valueD,
                    "remarque_g"=>(is_null($remarqueG)) ? null : $remarqueG,
                    "remarque_d"=>(is_null($remarqueD)) ? null : $remarqueD,
                    "cause_g"=>(empty($causesG)) ? null : $causesG,
                    "cause_d"=>(empty($causesD)) ? null : $causesD
                ];
                array_push($affichage[$articulation], $articulaireWithConditions);

        }

        $form = $this->createForm(BilanArticulaireType::class, $bilanArticulaire, array(
        ));
        $form->add('submit', SubmitType::class, array(
            'label' => 'Modifier',
            'attr'  => array('class' => 'btn btn-primary'),
        ));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $resultats = $bilanArticulaire->getResultats();
                $bilanArticulaire->removeAllResultats();
                $em->persist($bilanArticulaire);

                foreach ($resultats as $resultat ) {
                    if(is_null($resultat->getValue())){
                        $resultat->setValue($resultat->getItemTest()->getAngleMax());
                    }
                    $causes = $resultat->getCauses();
                    $resultat->removeAllCauses();
                    $resultat->setBilan($bilanArticulaire);
                    $bilanArticulaire->addResultat($resultat);
                    $em->persist($bilanArticulaire);

                    foreach ($causes as $cause) {
                        $cause->removeResultatsArticulaire($resultat);
                        $cause->addResultatsArticulaire($resultat);
                        $resultat->addCause($cause);
                        $em->persist($bilanArticulaire);
                    }
                }
                $em->persist($bilanArticulaire);
                $em->flush();
                
                return $this->render('App2Bundle:Bilans:get_one.html.twig', array(
                    'bilan' => $bilanArticulaire,
                    'type' => 'Articulaire',
                    'componentResultat' => "bilanArticulaireResultat",
                    'paths' => ['extract'=>"app2_bilan_articulaire_extract", "modify"=>"app2_bilan_articulaire_update"]
                ));   
            }
        }

        return $this->render('App2Bundle:BilansArticulaire:create.html.twig', array(
            'form' => $form->createView(),
            'articulairesReFormat' => $affichage,
            'causes'=> $causes,
            'modify' => true,
            'patient' => $patient
        ));    
    }
    

    public function getOneAction($idBilan)
    {        
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $bilanArticulaire = $em
            ->getRepository(BilanArticulaire::class)
            ->find($idBilan);
        
            return $this->render('App2Bundle:Bilans:get_one.html.twig', array(
                'bilan' => $bilanArticulaire,
                'type' => 'Articulaire',
                'componentResultat' => "bilanArticulaireResultat",
                'paths' => ['extract'=>"app2_bilan_articulaire_extract", "modify"=>"app2_bilan_articulaire_update"]
            ));   
    }

    public function extractAction($idBilan)
    {        
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $bilanArticulaire = $em
            ->getRepository(BilanArticulaire::class)
            ->find($idBilan);

        $resultats = $bilanArticulaire->getResultats();
        $affichage = [];
        foreach($resultats as $resultat){
            $affichage[]= [ 
                "item_nom"=> $resultat->getItemTest()->getNom(),
                "item_articulation"=> $resultat->getItemTest()->getArticulation(),
                "cote"=> ($resultat->getCote() == "0") ? "gauche" : "droite",
                "valeur"=> $resultat->getValue(),
                "angle_max"=> $resultat->getItemTest()->getAngleMax(),
                "remarque"=> $resultat->getRemarque()];
        }
        return $this->render('App2Bundle:BilansGesteTest:extract.html.twig', array(
                'bilan' => $bilanArticulaire,
                'type' => 'Articulaire',
                'text_btn_success' => "Afficher les mouvements à la valuer maximale",
                'affichage'=> $affichage,
                'articulation' => true
            ));    
    }


    public function deleteAction($idBilan)
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $bilan = $em
            ->getRepository(BilanArticulaire::class)
            ->find($idBilan);
        if (!$bilan) {
            throw $this->createNotFoundException('Pas de bilan trouvé');
        }

        $patient = $bilan->getPatient();
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($bilan);
        $em->flush();
    
        return $this->redirectToRoute('app2_get_one_patient', array('idPatient' => $patient->getId()));
    }

    public function compareAction(Request $request, $idPatient)
    {   
        
        $em = $this->getDoctrine()->getEntityManager();
        $patient = $em->getRepository(Patient::class)
            ->find($idPatient);
        $bilansArticulaire = $em
            ->getRepository(BilanArticulaire::class)
            ->findBy(array('patient'=> $patient->getId()));
        $bilans = $em
        ->getRepository(Bilan::class)
        ->findBy(array('patient'=> $patient->getId()));

        $bilanGlobauxWithArticulaire = array();
        foreach ($bilans as $bilan) {
            $test = $bilan->getBilanArticulaire();
            if(!is_null($test)){
                array_push($bilanGlobauxWithArticulaire,$test);
            }
        }

        // 21/05/20 pas besoin d'utiliser un classement pour l'affichage avec les checkbox mais pas le meilleur système non plus...
        $choices = array(
            "Bilan Globaux" => array(),
            "Autres" => array()
        );
        foreach ($bilansArticulaire as $bilan) {
            $choices["Autres"] = array_merge($choices["Autres"],array($bilan->getNom()=> $bilan->getId()));
        }
        foreach ($bilanGlobauxWithArticulaire as $bilan) {
                $choices["Bilan Globaux"] = array_merge($choices["Bilan Globaux"],array($bilan->getNom()=> $bilan->getId()));
        }
       
        $form = $this->createFormBuilder()
            ->add('bilans', ChoiceType::class, array(
                'choices'  => $choices,
                'expanded' => true,
                'multiple' => true,
                'label' => "Bilans à comparer"                ))
            ->add('compare', SubmitType::class, array('label' => 'Comparer'))
            ->getForm();
            
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $data =$form->getData();
                
                $bilansToCompare = [];
                $resultatsToCompare = [];
                foreach ($data['bilans'] as $dataIdBilan) {
                    // print_r($dataIdBilan);
                    $bilansToCompare[] = $em->getRepository(BilanArticulaire::class)
                    ->find($dataIdBilan);
                }
                foreach ($bilansToCompare as $bilan) {
                    $resultatsToCompare[] = $bilan->getResultats();
                }
                //On itère sur les résultats contenus dans le dernier bilan
                $affichage = [];
                
                foreach(array_pop($resultatsToCompare) as $resultatDernierBilan){
                    $articulation = $resultatDernierBilan->getItemTest()->getArticulation();
                    $nom = $resultatDernierBilan->getItemTest()->getNom();
                    $cote = $resultatDernierBilan->getCote();
                    //Si l'articulation n'est pas encore dans l'affichage on le rajoute
                    if(!array_key_exists($articulation,$affichage)){
                        $affichage[$articulation]=[];
                    }
                    //Si le geste test n'est pas encore dans l'affichage on le rajoute
                    if(!array_key_exists($nom,$affichage[$articulation])){
                        $affichage[$articulation][$nom]=[];
                        $affichage[$articulation][$nom]["angle_max"] = $resultatDernierBilan->getItemTest()->getAngleMax();
                    }
                    //Si le cote n'est pas encore dans l'affichage on rajoutera la valeur du dernier bilan à la fin
                    if(!array_key_exists((int)$cote,$affichage[$articulation][$nom])){
                        $affichage[$articulation][$nom][$cote]=[];
                    }
                    //Le dernier bilan n'est plus dans tocompare car array_pop
                    foreach($resultatsToCompare as $resultats){
                        foreach($resultats as $resultat) {
                            if($resultat->getItemTest() == $resultatDernierBilan->getItemTest() && 
                            (int)$resultat->getCote() == (int)$resultatDernierBilan->getCote()){
                                array_push($affichage[$articulation][$nom][$cote],$resultat->getValue());
                            }
                        }
                    }
                    array_push($affichage[$articulation][$nom][$cote], $resultatDernierBilan->getValue());

                }
                    return $this->render('App2Bundle:BilansArticulaire:compare.html.twig', array(
                        'affichage' => $affichage,
                        'bilans' => $bilansToCompare,
                        'patient' => $patient,
                        'test' => "ddd"
                    ));    

                    // return $this->render('App2Bundle:BilansArticulaire:compare.html.twig', array(
                    //     'affichage' => $affichage,
                    //     'bilan1' => $bilan1,
                    //     'bilan2' => $bilan2,
                    //     'patient' => $patient
                    // ));                       
                }
        }
        return $this->render('App2Bundle:Layout:jumbotron_classic_form.html.twig', array(
            'form' => $form->createView()
        ));   
          
    }
}
