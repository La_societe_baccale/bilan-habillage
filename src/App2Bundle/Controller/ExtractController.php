<?php

namespace App2Bundle\Controller;

use App2Bundle\Entity\Bilans\BilanArticulaire;
use App2Bundle\Entity\Bilans\BilanGesteTest;
use App2Bundle\Entity\Bilans\BilanHabillage;
use App2Bundle\Entity\Bilans\BilanKapandji;
use App2Bundle\Entity\Bilans\BilanMusculaire;
use App2Bundle\Entity\BilansCustom\BilanCustom;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App2Bundle\Entity\BilansCustom\BilanCustomSkeleton;
use App2Bundle\Form\BilansCustom\BilanCustomSkeletonType;
use App2Bundle\Form\BilansCustom\BilanCustomType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use App2Bundle\Entity\Patients\Patient;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App2Bundle\Repository\BilansCustom\BilanCustomRepository;



class ExtractController extends Controller
{
    public function prepareAction(Request $request, $idPatient)
    {   
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $patient = $this->getDoctrine()
            ->getRepository(Patient::class)
            ->find($idPatient);
            
        $bilansCustomSkeleton = $em
            ->getRepository(BilanCustomSkeleton::class)
            ->findBy(['organisation' => $user->getOrganisation(),'isActive' => true]);
        
        $form = $this->createFormBuilder()
            ->add("geste_test", IntegerType::class,[
                'label'=> false,
                'data' => 0,
                'attr' => ['class' => 'input_nb_geste_test form_to_add'],])
            ->add("articulaire", IntegerType::class,[
                'label'=> false,
                'data' => 0,
                'attr' => ['class' => 'input_nb_articulaire form_to_add'],])
            ->add("musculaire", IntegerType::class,[
                'label'=> false,
                'data' => 0,
                'attr' => ['class' => 'input_nb_musculaire form_to_add'],])
            ->add("kapandji", IntegerType::class,[
                'label'=> false,
                'data' => 0,
                'attr' => ['class' => 'input_nb_kapandji form_to_add'],])
            ;
        foreach ($bilansCustomSkeleton as $bilanCustomSkeleton) {
            $form = $form->add('custom_'.$bilanCustomSkeleton->getId(), IntegerType::class,[
                'label'=> false,
                'data' => 0,
                'attr' => ['class' => 'form_to_add input_nb_'.$bilanCustomSkeleton->getId()],])
            ;
        }
        $form->add('submit', SubmitType::class, array(
            'label' => 'Créer',
            'attr'  => array('class' => 'btn btn-primary'),
        ));
        $form = $form->getForm();
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            $bilanToDisplay = []; 

            if ($form->isValid()) {
                $data =$form->getData();
                foreach ($data as $key => $value) {
                    if($value !== 0){
                        if(substr($key,0,7) !== "custom_"){
                            if($key=="geste_test"){
                                $bilanToAdd = $em->getRepository(BilanGesteTest::class)->findBy(
                                  array('patient' => $patient), array('dateAjout' => 'ASC'),
                                  $value == -1 ?null: $value,
                                   0);
                                $bilanToDisplay["geste-test"] = $bilanToAdd;
                            }
                            else if ($key=="articulaire"){
                                $bilanToAdd = $em->getRepository(BilanArticulaire::class)->findBy(
                                  array('patient' => $patient), array('dateAjout' => 'ASC'),
                                  $value == -1 ?null: $value,
                                   0);
                                $bilanToDisplay["articulaire"] = $bilanToAdd;
                                }
                            else if ($key=="musculaire"){
                                $bilanToAdd = $em->getRepository(BilanMusculaire::class)->findBy(
                                  array('patient' => $patient), array('dateAjout' => 'ASC'),
                                  $value == -1 ?null: $value,
                                   0);
                                $bilanToDisplay["musculaire"] = $bilanToAdd;
                                }
                            else if ($key=="kapandji"){
                                $bilanToAdd = $em->getRepository(BilanKapandji::class)->findBy(
                                  array('patient' => $patient), array('dateAjout' => 'ASC'),
                                  $value == -1 ?null: $value,
                                   0);
                                $bilanToDisplay["kapandji"] = $bilanToAdd;
                            }
                            else if ($key=="habillage"){
                                $bilanToAdd = $em->getRepository(BilanHabillage::class)->findBy(
                                  array('patient' => $patient), array('dateAjout' => 'ASC'),
                                  $value == -1 ?null: $value,
                                   0);
                                $bilanToDisplay["habillage"] = $bilanToAdd;
                            }
                        }
                        else{
                            $idSkeleton = substr($key, 7); 
                            $bilanToAdd = $em->getRepository(BilanCustom::class)->findBy(
                                array('patient' => $patient, 'bilanSkeleton'=>$idSkeleton), array('dateAjout' => 'ASC'),
                                $value == -1 ?null: $value,
                                 0);
                            $bilanToDisplay[$idSkeleton] = $bilanToAdd;
                        }
                    }
                    
                }
                return $this->render('App2Bundle:Layout:test.html.twig', [
                    'array' => $bilanToDisplay,
                    'test' => $idSkeleton
                ]);
                // return $this->render('App2Bundle:Extract:display.html.twig', [
                //     'form' => $form->createView(),
                //     'bilansCustomSkeleton' => $bilansCustomSkeleton
                // ]);
            }
        }
        
        return $this->render('App2Bundle:Extract:prepare.html.twig', [
            'form' => $form->createView(),
            'bilansCustomSkeleton' => $bilansCustomSkeleton
        ]);
    }
    //  public function prepareAction(Request $request, $idPatient)
    // {   
    //     $em = $this->getDoctrine()->getManager();
    //     $user = $this->getUser();
    //     $patient = $this->getDoctrine()
    //         ->getRepository(Patient::class)
    //         ->find($idPatient);
            
    //     $bilansCustomSkeleton = $em
    //         ->getRepository(BilanCustomSkeleton::class)
    //         ->findBy(['organisation' => $user->getOrganisation(),'isActive' => true]);
     
    //     if ($request->isMethod('POST')) {
    //         $form->handleRequest($request);

    //         $bilanToDisplay = []; 

    //         if ($form->isValid()) {
    //             $data =$form->getData();
    //             foreach ($data as $key => $value) {
    //                 if($value !== false){
    //                     // var_dump($test); exit;
    //                     if(substr($key , -11)== "_geste_test"){
    //                         if(substr($key  , 0,11)=="last_three_"){
    //                             $bilanToAdd = $em->getRepository(BilanGesteTest::class)->findBy(
    //                                 array('patient' => $patient), array('dateAjout' => 'ASC'), 3, 0);
    //                         }
    //                         else if(substr($key  , 0,9)=="last_two_"){
    //                             $bilanToAdd = $em->getRepository(BilanGesteTest::class)->findBy(
    //                                 array('patient' => $patient), array('dateAjout' => 'ASC'), 2, 0);
    //                         }
    //                         else if(substr($key  , 0,5)=="last_"){
    //                             $bilanToAdd = $em->getRepository(BilanGesteTest::class)->findBy(
    //                                 array('patient' => $patient), array('dateAjout' => 'ASC'), 2, 0);
    //                         }
    //                         else if(substr($key  , 0,4)=="all_"){
    //                             $bilanToAdd = $em->getRepository(BilanGesteTest::class)->findBy(
    //                                 array('patient' => $patient), array('dateAjout' => 'ASC'));
    //                         }
    //                         $bilanToDisplay["geste_test"] = $bilanToAdd;
    //                     }     
    //                     else if(substr($key , -11)== "_musculaire"){
    //                         if(substr($key  , 0,11)=="last_three_"){
    //                             $bilanToAdd = $em->getRepository(BilanMusculaire::class)->findBy(
    //                                 array('patient' => $patient), array('dateAjout' => 'ASC'), 3, 0);
    //                         }
    //                         else if(substr($key  , 0,9)=="last_two_"){
    //                             $bilanToAdd = $em->getRepository(BilanMusculaire::class)->findBy(
    //                                 array('patient' => $patient), array('dateAjout' => 'ASC'), 2, 0);
    //                         }
    //                         else if(substr($key  , 0,5)=="last_"){
    //                             $bilanToAdd = $em->getRepository(BilanMusculaire::class)->findBy(
    //                                 array('patient' => $patient), array('dateAjout' => 'ASC'), 2, 0);
    //                         }
    //                         else if(substr($key  , 0,4)=="all_"){
    //                             $bilanToAdd = $em->getRepository(BilanMusculaire::class)->findBy(
    //                                 array('patient' => $patient), array('dateAjout' => 'ASC'));
    //                         }
    //                         $bilanToDisplay["musculaire"] = $bilanToAdd;
    //                     }     
    //                     else if(substr($key , -11)== "_articulaire"){
    //                         if(substr($key  , 0,11)=="last_three_"){
    //                             $bilanToAdd = $em->getRepository(BilanArticulaire::class)->findBy(
    //                                 array('patient' => $patient), array('dateAjout' => 'ASC'), 3, 0);
    //                         }
    //                         else if(substr($key  , 0,9)=="last_two_"){
    //                             $bilanToAdd = $em->getRepository(BilanArticulaire::class)->findBy(
    //                                 array('patient' => $patient), array('dateAjout' => 'ASC'), 2, 0);
    //                         }
    //                         else if(substr($key  , 0,5)=="last_"){
    //                             $bilanToAdd = $em->getRepository(BilanArticulaire::class)->findBy(
    //                                 array('patient' => $patient), array('dateAjout' => 'ASC'), 2, 0);
    //                         }
    //                         else if(substr($key  , 0,4)=="all_"){
    //                             $bilanToAdd = $em->getRepository(BilanArticulaire::class)->findBy(
    //                                 array('patient' => $patient), array('dateAjout' => 'ASC'));
    //                         }
    //                         $bilanToDisplay["articulaire"] = $bilanToAdd;
    //                     }
    //                     else {
    //                         if(substr($key  , 0,11)=="last_three_"){
    //                             $bilanToAdd = $em->getRepository(BilanArticulaire::class)->findBy(
    //                                 array('patient' => $patient), array('dateAjout' => 'ASC'), 3, 0);
    //                         }
    //                         else if(substr($key  , 0,9)=="last_two_"){
    //                             $bilanToAdd = $em->getRepository(BilanArticulaire::class)->findBy(
    //                                 array('patient' => $patient), array('dateAjout' => 'ASC'), 2, 0);
    //                         }
    //                         else if(substr($key  , 0,5)=="last_"){
    //                             $bilanToAdd = $em->getRepository(BilanArticulaire::class)->findBy(
    //                                 array('patient' => $patient), array('dateAjout' => 'ASC'), 2, 0);
    //                         }
    //                         else if(substr($key  , 0,4)=="all_"){
    //                             $bilanToAdd = $em->getRepository(BilanArticulaire::class)->findBy(
    //                                 array('patient' => $patient), array('dateAjout' => 'ASC'));
    //                         }
    //                         $bilanToDisplay["articulaire"] = $bilanToAdd;
    //                     }     

    //                 }
    //             }
    //             return $this->render('App2Bundle:Layout:test.html.twig', [
    //                 'array' => $bilanToDisplay,
    //                 'test' => $test
    //             ]);
    //             // return $this->render('App2Bundle:Extract:display.html.twig', [
    //             //     'form' => $form->createView(),
    //             //     'bilansCustomSkeleton' => $bilansCustomSkeleton
    //             // ]);
    //         }
    //     }
        
    //     return $this->render('App2Bundle:Extract:prepare.html.twig', [
    //         'form' => $form->createView(),
    //         'bilansCustomSkeleton' => $bilansCustomSkeleton
    //     ]);
    // }
}
