<?php

namespace App2Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App2Bundle\Entity\Patients\Patient;
use App2Bundle\Entity\Tests\GesteTest;
use App2Bundle\Entity\Bilans\Bilan;
use App2Bundle\Form\Bilans\BilanType;
use App2Bundle\Entity\Bilans\BilanGesteTest;
use App2Bundle\Form\Bilans\BilanGesteTestType;
use App2Bundle\Entity\Tests\KapandjiPosition;
use App2Bundle\Entity\Bilans\BilanKapandji;
use App2Bundle\Form\Bilans\BilanKapandjiType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;



class BilanController extends Controller
{
    public function createAction(Request $request, $idPatient)
    {
        $em = $this->getDoctrine()->getManager();
        $patient = $em->getRepository(Patient::class)
            ->find($idPatient);

        $bilan = new Bilan();
        $form = $this->createForm(BilanType::class, $bilan, array(
            'patient' => $patient
        ));
        $form->add('submit', SubmitType::class, array(
            'label' => 'Créer',
            'attr'  => array('class' => 'btn btn-primary'),
        ));
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $bilan->setPatient($patient);
                $em->persist($bilan);
                $em->flush();

                return $this->render('App2Bundle:Bilans:resume.html.twig', array(
                    'bilan' => $bilan,
                    'patient' => $patient
                ));
        
            }
        }

        return $this->render('App2Bundle:Bilans:create.html.twig', array(
            'form' => $form->createView(),
        ));    
    }


    public function getOneAction($idBilan)
    {        
        $user = $this->getUser();
        $em = $this->getDoctrine()->getEntityManager();
        $bilan = $em
            ->getRepository(Bilan::class)
            ->find($idBilan);
        
        
        return $this->render('App2Bundle:Bilans:resume.html.twig', array(
                'bilan' => $bilan,
                'patient' => $bilan->getPatient()
            ));    
    }

    public function deleteAction($idBilan)
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $bilan = $em
            ->getRepository(Bilan::class)
            ->find($idBilan);
        if (!$bilan) {
            throw $this->createNotFoundException('Pas de bilan trouvé');
        }

        $patient = $bilan->getPatient();
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($bilan);
        $em->flush();
    
        return $this->redirectToRoute('app2_get_one_patient', array('idPatient' => $patient->getId()));
    }
}
