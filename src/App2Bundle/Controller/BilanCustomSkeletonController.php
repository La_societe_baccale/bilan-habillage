<?php

namespace App2Bundle\Controller;

use App2Bundle\Entity\BilansCustom\BilanCustom;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App2Bundle\Entity\BilansCustom\BilanCustomSkeleton;
use App2Bundle\Form\BilansCustom\BilanCustomSkeletonType;
use App2Bundle\Form\BilansCustom\BilanCustomType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use App2Bundle\Entity\Patients\Patient;



class BilanCustomSkeletonController extends Controller
{
    public function deleteAction($idBilanSkeleton)
    {   
        $em = $this->getDoctrine()->getManager();
        $bilanCustomSkeleton = $em
            ->getRepository(BilanCustomSkeleton::class)
            ->find($idBilanSkeleton);
        if (!$bilanCustomSkeleton) {
            throw $this->createNotFoundException('Pas de bilan trouvé');
        }
        foreach ($bilanCustomSkeleton->getSubtotals() as $st) {
            $em->remove($st);
        }
        $em->flush();

        $em->remove($bilanCustomSkeleton);
        $em->flush();
    
        return $this->redirectToRoute('app2_bilan_custom_list');
    }

    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $organisation = $user->getOrganisation();

        $bilan = new BilanCustomSkeleton();
        $form = $this->createForm(BilanCustomSkeletonType::class, $bilan, array(
        ));
        $form->add('submit', SubmitType::class, array(
            'label' => 'Créer',
            'attr'  => array('class' => 'btn btn-primary'),
        ));
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $bilan->setOrganisation($organisation);
                $bilan->setIsActive(false);

                $itemTests = $bilan->getItemTests();
                $subTotals = $bilan->getSubtotals();
                $subTexts = $bilan->getSubtexts();
                $subParts = $bilan->getSubparts();

                $bilan->removeAllItemTests();
                $bilan->removeAllSubtotals();
                $bilan->removeAllSubtexts();
                $bilan->removeAllSubparts();

                $em->persist($bilan);

                foreach ($itemTests as $item ) {
                    $item->setBilanSkeleton($bilan);
                    $bilan->addItemTest($item);
                }
                $em->persist($bilan);
                if($subTexts !== null){
                    foreach ($subTexts as $sub ) {
                        $sub->setBilanSkeleton($bilan);
                        $bilan->addSubtext($sub);
                    }
                    $em->persist($bilan);
                }

                if($subParts !== null){
                    foreach ($subParts as $sub ) {
                        $sub->setBilanSkeleton($bilan);
                        $bilan->addSubpart($sub);
                    }
                    $em->persist($bilan);
                }

                if($subTotals !== null){
                    $lastSubTotalOrdre = -1;
                    foreach ($subTotals as $subTotal ) {
                        if($subTotal->getIsFullTotal() == false){
                            foreach ($itemTests as $item ) {
                                if($item->getOrdre() < $subTotal->getOrdre() && $item->getOrdre() > $lastSubTotalOrdre ){
                                    $subTotal->addItemTest($item);
                                }
                            }
                            $lastSubTotalOrdre = $subTotal->getOrdre();
                        }
                        $subTotal->setBilanSkeleton($bilan);
                        $bilan->addSubtotal($subTotal);
                    }
                }

                $em->persist($bilan);
                $em->flush();

                $itemAndSubTotal = $bilan->getItemTests()->toArray();
                $itemAndSubTotal = array_merge($itemAndSubTotal, $bilan->getSubtotals()->toArray());
                $itemAndSubTotal = array_merge($itemAndSubTotal, $bilan->getSubparts()->toArray());
                $itemAndSubTotal = array_merge($itemAndSubTotal, $bilan->getSubtexts()->toArray());
                usort($itemAndSubTotal, function($a, $b) {return intval($a->getOrdre()) - intval($b->getOrdre());});
                
                return $this->render('App2Bundle:BilansCustomSkeleton:presentation_skeleton.html.twig', array(
                    'bilan' => $bilan,
                    'itemAndSubTotal' => $itemAndSubTotal
                ));
        
            }
        }

        return $this->render('App2Bundle:BilansCustomSkeleton:new.html.twig', array(
            'form' => $form->createView(),
        ));    
    }
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $organisation = $user->getOrganisation();
        $bilanCustomSkeletons = $organisation->getBilanCustomSkeletons();
        return $this->render('App2Bundle:BilansCustomSkeleton:list_skeleton.html.twig', array(
            'bilans' => $bilanCustomSkeletons,
        ));   
    }
    public function oneAction($idBilan)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $organisation = $user->getOrganisation();
        $bilan = $em
            ->getRepository(BilanCustomSkeleton::class)
            ->find($idBilan);
        $itemAndSubTotal = $bilan->getItemTests()->toArray();
        $itemAndSubTotal = array_merge($itemAndSubTotal, $bilan->getSubtotals()->toArray());
        $itemAndSubTotal = array_merge($itemAndSubTotal, $bilan->getSubparts()->toArray());
        $itemAndSubTotal = array_merge($itemAndSubTotal, $bilan->getSubtexts()->toArray());
        usort($itemAndSubTotal, function($a, $b) {return intval($a->getOrdre()) - intval($b->getOrdre());});

        return $this->render('App2Bundle:BilansCustomSkeleton:presentation_skeleton.html.twig', array(
            'bilan' => $bilan,
            'itemAndSubTotal' => $itemAndSubTotal
        )); 
    }
    public function changeActifAction($idBilan)
    {
        $em = $this->getDoctrine()->getManager();
        $bilan = $em
        ->getRepository(BilanCustomSkeleton::class)
        ->find($idBilan);
        $bilan->setIsActive(!$bilan->getIsActive());
        $em->persist($bilan);
        $em->flush();
        // $response = $this->forward('App2Bundle:BilanCustom:list', []);
        // return $response;
        return $this->redirect($this->generateUrl('app2_bilan_custom_list'));
    }
}
