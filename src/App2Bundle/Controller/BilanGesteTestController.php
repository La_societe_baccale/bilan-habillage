<?php

namespace App2Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App2Bundle\Entity\Patients\Patient;
use App2Bundle\Entity\Tests\GesteTest;
use App2Bundle\Entity\Bilans\Bilan;
use App2Bundle\Form\Bilans\BilanType;
use App2Bundle\Entity\Bilans\BilanGesteTest;
use App2Bundle\Form\Bilans\BilanForCreateBeforeType;
use App2Bundle\Form\Bilans\BilanGesteTestType;
use App2Bundle\Entity\Tests\KapandjiPosition;
use App2Bundle\Entity\Bilans\BilanKapandji;
use App2Bundle\Form\Bilans\BilanKapandjiType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
// use App2Bundle\Service\BilanFromImplicationFormGenerator;




class BilanGesteTestController extends Controller
{
    //TODO ENVOYER ID BILAN GLOBAL ENTRE POST CREATEBEFOREACTION ET CREATEACTION
    
    //Controller pour que l'utilisateur puisse choisir les bilans sur lesquels baser son nouveau bilan
    public function createBeforeAction(Request $request, $idPatient)
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $patient = $em->getRepository(Patient::class)
            ->find($idPatient);
        
        $BilanFromImplicationGenerator = $this->container->get('app.bilanfromimplicationgenerator');
        //listeBilan la liste des noms de bilans pour pouvoir construire le formulaire sous la forme ["musculaire","articulaire"] 
        //sans le nom du formulaire sur lequel on travaille
        $listeBilan = $BilanFromImplicationGenerator->getBilansFrom($idPatient,"geste-test");

        //Si pas de bilan From on passe à la création du bilan
        if($listeBilan == false){
            $options = array('idPatient' => $patient->getId());
            return $this->redirectToRoute('app2_bilan_geste_test_create', $options);  
        }
        //On regarde s'il s'agit d'un bilan global
         if($request->query->has('idBilan')){
            $bilan = ($_GET["idBilan"])? $em->getRepository(Bilan::class)->find($_GET["idBilan"]):null;
        }
        else{
            $bilan = null;
        }
        //On crée le formulaire 
        $form = $this->createForm(BilanForCreateBeforeType::class,null,[
            "id_patient" => $idPatient,
            "liste_bilans" => $listeBilan 
        ]);
        //Le formulaire est vide on passe à la création de bilan
        if(count($form->all()) == 1){
            $options = array('idPatient' => $patient->getId());
            return $this->redirectToRoute('app2_bilan_geste_test_create', $options);  
        }

        //On analyse la réponse  
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) 
            {
                $data = $form->getData();
                $j = 0;
                $bilans = [];
                $bilans_ignore = [];
                //On stocke les noms des bilan ignorés en enlevant les 8 premier caractère de l'inpu correspondant à "ignore_"
                //$bilans_ignore est donc une liste sous la forme ["musculaire","articulaire"]
                foreach ($data as $key => $value){
                    if (stripos($key, 'ignorer') !== true && $value === false) {
                        $bilans_ignore[] = substr($key, 8); 
                    }
                }
                //On stocke les noms des bilan ignorés en enlevant les 8 premier caractère de l'inpu correspondant à "bilan_"
                //$bilans est donc une liste sous la forme ["musculaire"=>idbilan,"articulaire"=>idbilan]
                foreach ($data as $key => $value){
                    if (stripos($key, 'bilan') !== false && in_array(substr($key, 6),$bilans_ignore)) {
                        $bilans[$key] = $value;
                    }
                }
                //Comme options pour la route suivant on envoie l'id du patient et les bilans from sous la forme
                //['idPatient' =>idpatient, "bilan_musculaire"=>idbilan,"bilan_articulaire"=>idbilan]
                //On garde les bilan_ au début des variables pour les identifier po-lus facilement ensuite
                $options = array('idPatient' => $patient->getId());
                $options = array_merge($options,$bilans);

                if(!is_null($bilan)){
                    $options = array_merge($options,array('idBilan' => $bilan->getId()));
                }
                return $this->redirectToRoute('app2_bilan_geste_test_create', $options);  
            }   
        } 
        return $this->render('App2Bundle:Layout:jumbotron_classic_form.html.twig', array(
            'form' => $form->createView(),
        ));   
          
    }

    public function createAction(Request $request, $idPatient)
    {
        $em = $this->getDoctrine()->getManager();
        $patient = $em->getRepository(Patient::class)
            ->find($idPatient);
        //on regarde si ce bilan fait parti d'un bilan global
        if($request->query->has('idBilan')){
            $bilan = ($_GET["idBilan"])? $em->getRepository(Bilan::class)->find($_GET["idBilan"]):null;
        }
        else{
            $bilan = null;
        }
        //Creation du nouveau bilan
        $bilanGesteTest = new BilanGesteTest();
        //On va chercher les gestes test
       
        $BilanFromImplicationGenerator = $this->container->get('app.bilanfromimplicationgenerator');
        //arrayIdBilansFrom ressemblera à ["musculaire"=>2,...] on vire le "bilan_"
        $arrayIdBilansFrom = $BilanFromImplicationGenerator->handleQuery($request->query->all());
        $implicationTo = $BilanFromImplicationGenerator->getAllImplicationsFrom("geste-test",$arrayIdBilansFrom);

        $gesteTests = $em 
        ->getRepository(GesteTest::class)
        ->findAll();
        $form = $this->createForm(BilanGesteTestType::class, $bilanGesteTest, array(
        ));
        $form->add('submit', SubmitType::class, array(
            'label' => 'Créer',
            'attr'  => array('class' => 'btn btn-primary'),
        ));
        $gesteTestsWithConditions = [];
        foreach ($gesteTests as $gesteTest) {
            $nom = $gesteTest->getNom(); 

            $conditionG = null;
            $conditionD = null;
            //On garde la premiere condition qui correspond au geste test et au bon côté
            foreach ($implicationTo as  $value) {
                if ($value["nom"] == $nom && $value["cote"] == 0) {
                    $conditionG = $value;
                    break;
                }
            }
            foreach ($implicationTo as  $value) {
                if ($value["nom"] == $nom && $value["cote"] == 1) {
                    $conditionD = $value;
                    break;
                }
            }
            $gesteTestWithConditions = [
                "nom"=>$nom,
                "id"=>$gesteTest->getId(),
                "condition_g"=>(is_null($conditionG)) ? null : $conditionG,
                "condition_d"=>(is_null($conditionD)) ? null : $conditionD
            ];
            $gesteTestsWithConditions[] = $gesteTestWithConditions;
        }

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $resultats = $bilanGesteTest->getResultats();
                $bilanGesteTest->removeAllResultats();
                if(!is_null($bilan)){
                    $bilanGesteTest->setBilan($bilan);
                    $bilanGesteTest->setNom($bilan->getNom()." : geste test");
                }
                else{
                    $bilanGesteTest->setPatient($patient);
                    $bilanGesteTest->setNom($bilanGesteTest->getDateAjout()->format('Y-m-d H:i:s'));
                }
                $em->persist($bilanGesteTest);
                foreach ($resultats as $resultat ) {
                    $resultat->setBilan($bilanGesteTest);
                    $bilanGesteTest->addResultat($resultat);
                }
                $em->persist($bilanGesteTest);
                $em->flush();

                return $this->render('App2Bundle:Bilans:get_one.html.twig', array(
                    'bilan' => $bilanGesteTest,
                    'type' => 'Geste-Test',
                    'componentResultat' => "bilanGesteTestResultat",
                    'paths' => ['extract'=>"app2_bilan_geste_test_extract", "modify"=>"app2_bilan_geste_test_update"],
                ));   
        
            }
        }
        
        return $this->render('App2Bundle:BilansGesteTest:create.html.twig', array(
            'form' => $form->createView(),
            'gesteTests' => $gesteTestsWithConditions,
            'patient' => $patient,
            'global' =>$bilan,
            'heure' => new \Datetime()

        ));    
    }

    public function updateAction(Request $request, $idBilan)
    {
        $em = $this->getDoctrine()->getManager();
        

        $bilanGesteTest = $em->getRepository(BilanGesteTest::class)
            ->find($idBilan);
        $patient =  $bilanGesteTest->getPatient();
        
        $gesteTests = $em 
            ->getRepository(GesteTest::class)
            ->findAll();
        
        $gesteTestsWithValues = [];
        $resultats = $bilanGesteTest->getResultats();
        foreach ($gesteTests as $gesteTest) {
                $nom = $gesteTest->getNom(); 
    
                $valueG = null;
                $valueD = null;
                $remarqueG = null;
                $remarqueD = null;
                //On garde la premiere condition qui correspond au geste test et au bon côté
                $i = 0;
                foreach ($resultats as  $resultat) {
                    if ($resultat->getItemTest()->getNom() == $nom && $resultat->getCote() == 0) {
                        $valueG = $resultat->getValue();
                        $remarqueG = $resultat->getRemarque();
                        $i = $i + 1;
                    }
                    if ($resultat->getItemTest()->getNom() == $nom && $resultat->getCote() == 1) {
                        $valueD = $resultat->getValue();
                        $remarqueD = $resultat->getRemarque();
                        $i = $i + 1;
                    }
                    if($i == 2 ){
                        break;
                    }
                }
                
                $gesteTestWithValues = [
                    "nom"=>$nom,
                    "id"=>$gesteTest->getId(),
                    "value_g"=>(is_null($valueG)) ? null : $valueG,
                    "value_d"=>(is_null($valueD)) ? null : $valueD,
                    "remarque_g"=>(is_null($remarqueG)) ? null : $remarqueG,
                    "remarque_d"=>(is_null($remarqueD)) ? null : $remarqueD
                ];
                $gesteTestsWithValues[] = $gesteTestWithValues;
        }
        $form = $this->createForm(BilanGesteTestType::class, $bilanGesteTest, array(
        ));
        $form->add('submit', SubmitType::class, array(
            'label' => 'Modifier',
            'attr'  => array('class' => 'btn btn-primary'),
        ));
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $resultats = $bilanGesteTest->getResultats();
                $bilanGesteTest->removeAllResultats();
                $em->persist($bilanGesteTest);
                foreach ($resultats as $resultat ) {
                    $resultat->setBilan($bilanGesteTest);
                    $bilanGesteTest->addResultat($resultat);
                }
                $em->persist($bilanGesteTest);
                $em->flush();


                return $this->render('App2Bundle:Bilans:get_one.html.twig', array(
                    'bilan' => $bilanGesteTest,
                    'type' => 'Geste-Test',
                    'componentResultat' => "bilanGesteTestResultat",
                    'paths' => ['extract'=>"app2_bilan_geste_test_extract", "modify"=>"app2_bilan_geste_test_update"]
                ));   
        
            }
        }

        return $this->render('App2Bundle:BilansGesteTest:create.html.twig', array(
            'form' => $form->createView(),
            'gesteTests' => $gesteTestsWithValues,
            'modify' => true,
            'patient' => $patient
        ));    
    }

    public function getOneAction($idBilan)
    {        
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $bilanGesteTest = $em
            ->getRepository(BilanGesteTest::class)
            ->find($idBilan);        
        return $this->render('App2Bundle:Bilans:get_one.html.twig', array(
                'bilan' => $bilanGesteTest,
                'type' => 'Geste-Test',
                'componentResultat' => "bilanGesteTestResultat",
                'paths' => ['extract'=>"app2_bilan_geste_test_extract", "modify"=>"app2_bilan_geste_test_update"]
            ));   
    }
    public function extractAction($idBilan)
    {        
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $bilanGesteTest = $em
            ->getRepository(BilanGesteTest::class)
            ->find($idBilan);

        $resultats = $bilanGesteTest->getResultats();
        $affichage = [];
        foreach($resultats as $resultat){
            $affichage[]= [ 
                "item_nom"=> $resultat->getItemTest()->getNom(),
                "cote"=> ($resultat->getCote() == "0") ? "gauche" : "droite",
                "valeur"=> ($resultat->getValue() == true) ? "oui" : "non",
                "remarque"=> ($resultat->getRemarque()) ];
        }
        return $this->render('App2Bundle:BilansGesteTest:extract.html.twig', array(
                'bilan' => $bilanGesteTest,
                'type' => "Geste-Test",
                'text_btn_success' => "Afficher les gestes test réussis",
                'affichage'=> $affichage
            ));    
    }


    public function deleteAction($idBilan)
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $bilan = $em
            ->getRepository(BilanGesteTest::class)
            ->find($idBilan);
        if (!$bilan) {
            throw $this->createNotFoundException('Pas de bilan trouvé');
        }

        $patient = $bilan->getPatient();
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($bilan);
        $em->flush();
    
        return $this->redirectToRoute('app2_get_one_patient', array('idPatient' => $patient->getId()));
    }

    //TODO check que l'utilisateur fournesse aux moins 2 bilans
    public function compareAction(Request $request, $idPatient)
    {   
        
        $em = $this->getDoctrine()->getEntityManager();
        $patient = $em->getRepository(Patient::class)
            ->find($idPatient);
        $bilansGesteTest = $em
            ->getRepository(BilanGesteTest::class)
            ->findBy(array('patient'=> $patient->getId()));
        $bilans = $em
        ->getRepository(Bilan::class)
        ->findBy(array('patient'=> $patient->getId()));

        $bilanGlobauxWithGesteTest = array();
        foreach ($bilans as $bilan) {
            $test = $bilan->getBilanGesteTest();
            if(!is_null($test)){
                array_push($bilanGlobauxWithGesteTest,$test);
            }
        }

        // 21/05/20 pas besoin d'utiliser un classement pour l'affichage avec les checkbox mais pas le meilleur système non plus...
        $choices = array(
            "Bilan Globaux" => array(),
            "Autres" => array()
        );
        foreach ($bilansGesteTest as $bilan) {
            $choices["Autres"] = array_merge($choices["Autres"],array($bilan->getNom()=> $bilan->getId()));
        }
        foreach ($bilanGlobauxWithGesteTest as $bilan) {
                $choices["Bilan Globaux"] = array_merge($choices["Bilan Globaux"],array($bilan->getNom()=> $bilan->getId()));
        }
       
        $form = $this->createFormBuilder()
            ->add('bilans', ChoiceType::class, array(
                'choices'  => $choices,
                'expanded' => true,
                'multiple' => true,
                'label' => "Bilans à comparer"                ))
            ->add('compare', SubmitType::class, array('label' => 'Comparer'))
            ->getForm();
            
            if ($request->isMethod('POST')) {
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $data =$form->getData();
                    
                    $bilansToCompare = [];
                    $resultatsToCompare = [];
                    foreach ($data['bilans'] as $dataIdBilan) {
                        // print_r($dataIdBilan);
                        $bilansToCompare[] = $em->getRepository(BilanGesteTest::class)
                        ->find($dataIdBilan);
                    }
                    foreach ($bilansToCompare as $bilan) {
                        $resultatsToCompare[] = $bilan->getResultats();
                    }
                    //On itère sur les résultats contenus dans le dernier bilan
                    $affichage = [];
                    
                    foreach(array_pop($resultatsToCompare) as $resultatDernierBilan){
                        $nomItem = $resultatDernierBilan->getItemTest()->getNom();
                        $cote = $resultatDernierBilan->getCote();
                        //Si le geste test n'est pas encore dans l'affichage on le rajoute
                        if(!array_key_exists($nomItem,$affichage)){
                            $affichage[$nomItem]=[];
                        }
                        //Si le cote n'est pas encore dans l'affichage on rajoutera la valeur du dernier bilan à la fin
                        if(!array_key_exists((int)$cote,$affichage[$nomItem])){
                            $affichage[$nomItem][$cote]=[];
                        }
                        //Le dernier bilan n'est plus dans tocompare car array_pop
                        foreach($resultatsToCompare as $resultats){
                            foreach($resultats as $resultat) {
                                if($resultat->getItemTest() == $resultatDernierBilan->getItemTest() && 
                                (int)$resultat->getCote() == (int)$resultatDernierBilan->getCote()){
                                    array_push($affichage[$nomItem][$cote],$resultat->getValue());
                                }
                            }
                        }
                        array_push($affichage[$nomItem][$cote], $resultatDernierBilan->getValue());

                    }
                    
                    return $this->render('App2Bundle:Bilans:compare_binary.html.twig', array(
                        'affichage' => $affichage,
                        'bilans' => $bilansToCompare,
                        'patient' => $patient,
                        'columns' => ['Geste-test'],
                        'nomBilan' => "geste-test"
                    ));                     
                }
            }
        return $this->render('App2Bundle:Layout:jumbotron_classic_form.html.twig', array(
            'form' => $form->createView()
        ));   
          
    }
}
