<?php

namespace App2Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App2Bundle\Entity\Patients\Patient;
use App2Bundle\Entity\BilansCustom\BilanCustomSkeleton;
use App2Bundle\Form\Patients\PatientType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class PatientController extends Controller
{
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $patient = new Patient();
        $codePatient= random_int(10000,99999 );
        $form = $this->createForm(PatientType::class, $patient, array(
            'user' => $user,
            'organisation' => $user->getOrganisation()
        ));
        $form->add('submit', SubmitType::class, array(
            'label' => 'Créer',
            'attr'  => array('class' => 'btn btn-primary'),
        ));
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {

                $patient->setUser($user);
                $patient->setOrganisation($user->getOrganisation());

                $em->persist($patient);
                $em->flush();


                return $this->redirectToRoute('app2_get_one_patient', array('idPatient' => $patient->getId()));

        
            }
        }

        return $this->render('App2Bundle:Patient:create.html.twig', array(
      'form' => $form->createView(), 'codePatient' =>$codePatient));

        
        
    }

    public function getOneAction($idPatient)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $patient = $this->getDoctrine()
            ->getRepository(Patient::class)
            ->find($idPatient);
        $bilansCustomSkeleton = $em
            ->getRepository(BilanCustomSkeleton::class)
            ->findBy(['organisation' => $user->getOrganisation(),'isActive' => true]);
        $bilansCustom = $patient->getBilansCustom();
        $bilansCustomDisplayArray = [];
        
        foreach ($bilansCustomSkeleton as $bilanCustomSkeleton) {
            $arrayBilansPatient = [];
            foreach ($bilansCustom as $bilanCustom) {
                if($bilanCustom->getBilanSkeleton()->getId() == $bilanCustomSkeleton->getId()){
                    array_push($arrayBilansPatient, $bilanCustom);
                }
            }
            $arrayToPush = [
                "skeleton" => $bilanCustomSkeleton,
                "bilansPatient" => $arrayBilansPatient
            ];
            array_push($bilansCustomDisplayArray, $arrayToPush);
        }

        return $this->render('App2Bundle:Patient:one.html.twig', array(
            'user' => $user,
            'patient'=>$patient,
            'bilansCustomDisplay' => $bilansCustomDisplayArray
        ));
    }

    public function getMyAction()
    {
        $user = $this->getUser();
        $patients = $user->getPatients();
        return $this->render('App2Bundle:Patient:multiple.html.twig', array(
            'user' => $user,
            'patients'=>$patients,
            'title'=> "Vos Patients"
        ));
    }
    

}
