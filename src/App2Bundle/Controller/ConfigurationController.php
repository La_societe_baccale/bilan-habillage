<?php

namespace App2Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ConfigurationController extends Controller
{
    public function indexAction()
    {
        $user = $this->getUser();
        return $this->render('App2Bundle:Configuration:configuration.html.twig', array('user' => $user));
    }
}
