<?php

namespace App2Bundle\Controller;

use App2Bundle\Entity\BilansCustom\BilanCustom;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App2Bundle\Entity\BilansCustom\BilanCustomSkeleton;
use App2Bundle\Form\BilansCustom\BilanCustomSkeletonType;
use App2Bundle\Form\BilansCustom\BilanCustomType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use App2Bundle\Entity\Patients\Patient;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App2Bundle\Repository\BilansCustom\BilanCustomRepository;



class BilanCustomController extends Controller
{
    public function deleteAction($idBilan)
    {   
        $em = $this->getDoctrine()->getManager();
        $bilanCustom = $em
            ->getRepository(BilanCustom::class)
            ->find($idBilan);
        if (!$bilanCustom) {
            throw $this->createNotFoundException('Pas de bilan trouvé');
        }

        $patient = $bilanCustom->getPatient();
        $em->remove($bilanCustom);
        $em->flush();
    
        return $this->redirectToRoute('app2_get_one_patient', array('idPatient' => $patient->getId()));
    }

    public function getOneAction($idBilan)
    {        
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $bilanCustom = $em
            ->getRepository(BilanCustom::class)
            ->find($idBilan);    
        $bilanSkeleton = $bilanCustom->getBilanSkeleton();

        $itemAndSubTotal = $bilanSkeleton->getItemTests()->toArray();
        $itemAndSubTotal = array_merge($itemAndSubTotal, $bilanSkeleton->getSubtotals()->toArray());
        $itemAndSubTotal = array_merge($itemAndSubTotal, $bilanSkeleton->getSubparts()->toArray());
        $itemAndSubTotal = array_merge($itemAndSubTotal, $bilanSkeleton->getSubtexts()->toArray());
        usort($itemAndSubTotal, function($a, $b) {return intval($a->getOrdre()) - intval($b->getOrdre());});

        return $this->render('App2Bundle:BilansCustom:get_one.html.twig', array(
                    'bilan' => $bilanCustom,
                    'bilanSkeleton' => $bilanSkeleton,
                    'itemAndSubTotal' => $itemAndSubTotal
                ));   
    }

    public function createAction(Request $request, $idPatient, $idBilanSkeleton)
    {
        $em = $this->getDoctrine()->getManager();
        $patient = $em->getRepository(Patient::class)
            ->find($idPatient);
        $bilanSkeleton = $em->getRepository(BilanCustomSkeleton::class)
            ->find($idBilanSkeleton);
    
        $bilanCustom = new BilanCustom();
        $itemAndSubTotal = $bilanSkeleton->getItemTests()->toArray();
        $itemAndSubTotal = array_merge($itemAndSubTotal, $bilanSkeleton->getSubtotals()->toArray());
        $itemAndSubTotal = array_merge($itemAndSubTotal, $bilanSkeleton->getSubparts()->toArray());
        $itemAndSubTotal = array_merge($itemAndSubTotal, $bilanSkeleton->getSubtexts()->toArray());
        usort($itemAndSubTotal, function($a, $b) {return intval($a->getOrdre()) - intval($b->getOrdre());});
        
        $form = $this->createForm(BilanCustomType::class, $bilanCustom, array(
        ));
        $form->add('submit', SubmitType::class, array(
            'label' => 'Créer',
            'attr'  => array('class' => 'btn btn-primary'),
        ));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $resultats = $bilanCustom->getResultats();
                $bilanCustom->removeAllResultats();
               
                $bilanCustom->setPatient($patient);
                $bilanCustom->setBilanSkeleton($bilanSkeleton);
                $bilanCustom->setNom($bilanCustom->getDateAjout()->format('Y-m-d H:i:s'));
                
                $em->persist($bilanCustom);
                foreach ($resultats as $resultat ) {
                    $resultat->setBilan($bilanCustom);
                    $bilanCustom->addResultat($resultat);
                }
                $em->persist($bilanCustom);
                $em->flush();

                return $this->render('App2Bundle:BilansCustom:get_one.html.twig', array(
                    'bilan' => $bilanCustom,
                    'bilanSkeleton' => $bilanSkeleton,
                    'itemAndSubTotal' => $itemAndSubTotal
                )); 
                
            }
        }

        return $this->render('App2Bundle:BilansCustom:create.html.twig', array(
            'form' => $form->createView(),
            'bilanSkeleton' => $bilanSkeleton,
            'itemAndSubTotal' => $itemAndSubTotal,
            'isUpdate' => false,
        ));    
    }

    public function updateAction(Request $request, $idBilan)
    {
        $em = $this->getDoctrine()->getManager();
        $bilanCustom = $em
            ->getRepository(BilanCustom::class)
            ->find($idBilan);    
        $bilanSkeleton = $bilanCustom->getBilanSkeleton();
    
        $itemAndSubTotal = $bilanSkeleton->getItemTests()->toArray();
        $itemAndSubTotal = array_merge($itemAndSubTotal, $bilanSkeleton->getSubtotals()->toArray());
        $itemAndSubTotal = array_merge($itemAndSubTotal, $bilanSkeleton->getSubparts()->toArray());
        $itemAndSubTotal = array_merge($itemAndSubTotal, $bilanSkeleton->getSubtexts()->toArray());
        usort($itemAndSubTotal, function($a, $b) {return intval($a->getOrdre()) - intval($b->getOrdre());});
        
        $form = $this->createForm(BilanCustomType::class, $bilanCustom, array(
        ));
        $form->add('submit', SubmitType::class, array(
            'label' => 'Créer',
            'attr'  => array('class' => 'btn btn-primary'),
        ));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $resultats = $bilanCustom->getResultats();
                $bilanCustom->removeAllResultats();
               
                // $bilanCustom->setPatient($patient);
                $bilanCustom->setBilanSkeleton($bilanSkeleton);
                $bilanCustom->setNom($bilanCustom->getDateAjout()->format('Y-m-d H:i:s'));
                
                $em->persist($bilanCustom);
                foreach ($resultats as $resultat ) {
                    $resultat->setBilan($bilanCustom);
                    $bilanCustom->addResultat($resultat);
                }
                $em->persist($bilanCustom);
                $em->flush();

                return $this->render('App2Bundle:BilansCustom:get_one.html.twig', array(
                    'bilan' => $bilanCustom,
                    'bilanSkeleton' => $bilanSkeleton,
                    'itemAndSubTotal' => $itemAndSubTotal
                )); 
                
            }
        }

        return $this->render('App2Bundle:BilansCustom:create.html.twig', array(
            'form' => $form->createView(),
            'bilanSkeleton' => $bilanSkeleton,
            'itemAndSubTotal' => $itemAndSubTotal,
            'bilan' => $bilanCustom,
            'isUpdate' => true,
        ));    
    }

    public function compareAction(Request $request, $idPatient , $idBilanSkeleton)
    {   
        
        $em = $this->getDoctrine()->getManager();
        $patient = $em->getRepository(Patient::class)
            ->find($idPatient);
        $bilanSkeleton = $em->getRepository(BilanCustomSkeleton::class)
            ->find($idBilanSkeleton);

        // $bilansCustom = $em
        //     ->getRepository(BilanGesteTest::class)
        //     ->findBy(array('patient'=> $patient->getId(), 'bilanSkeleton' =>  $bilanSkeleton->getId()));
    
        $itemAndSubTotal = $bilanSkeleton->getItemTests()->toArray();
        $itemAndSubTotal = array_merge($itemAndSubTotal, $bilanSkeleton->getSubtotals()->toArray());
        $itemAndSubTotal = array_merge($itemAndSubTotal, $bilanSkeleton->getSubparts()->toArray());
        $itemAndSubTotal = array_merge($itemAndSubTotal, $bilanSkeleton->getSubtexts()->toArray());
        usort($itemAndSubTotal, function($a, $b) {return intval($a->getOrdre()) - intval($b->getOrdre());});
        
       
        $form = $this->createFormBuilder()
            ->add('bilans', EntityType::class, array(
                // 'choices'  => $bilansCustom,
                'class' => 'App2Bundle:BilansCustom\BilanCustom',
                'choice_label' => 'nom',
                'expanded' => true,
                'multiple' => true,
                'label' => "Bilans à comparer",
                'query_builder' => function (BilanCustomRepository $er) use ( $idPatient, $idBilanSkeleton) {
                    return $er->createQueryBuilder('u')
                        ->andWhere('u.patient = :idPatient')
                        ->andWhere('u.bilanSkeleton = :idSkeleton')
                        ->setParameter('idPatient', $idPatient)
                        ->setParameter('idSkeleton', $idBilanSkeleton);
                }                ))
            ->add('compare', SubmitType::class, array('label' => 'Comparer'))
            ->getForm();
            
            if ($request->isMethod('POST')) {
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $data =$form->getData();      
                    
                    return $this->render('App2Bundle:BilansCustom:compare.html.twig', array(
                        'bilansToCompare' => $data['bilans'],
                        'itemAndSubTotal' => $itemAndSubTotal,
                        'bilanSkeleton' => $bilanSkeleton,
                        'patient' => $patient                         
                    ));                     
                }
            }
        return $this->render('App2Bundle:Layout:jumbotron_classic_form.html.twig', array(
            'form' => $form->createView()
        ));   
          
    }
}
