<?php

namespace App2Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App2Bundle\Entity\Patients\Patient;
use App2Bundle\Entity\Tests\GesteTest;
use App2Bundle\Entity\Bilans\Bilan;
use App2Bundle\Form\Bilans\BilanType;
use App2Bundle\Entity\Bilans\BilanKapandji;
use App2Bundle\Form\Bilans\BilanKapandjiType;
use App2Bundle\Entity\Tests\KapandjiPosition;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;



class BilanKapandjiController extends Controller
{
    public function createAction(Request $request, $idPatient)
    {
        $em = $this->getDoctrine()->getManager();
        $patient = $em->getRepository(Patient::class)
            ->find($idPatient);
        
        if($request->query->has('idBilan')){
            $bilan = ($_GET["idBilan"])? $em->getRepository(Bilan::class)->find($_GET["idBilan"]):null;
        }
        else{
            $bilan = null;
        }

        $kapandjiPositions = $this->getDoctrine()
            ->getRepository(KapandjiPosition::class)
            ->findAll();
        $bilanKapandji = new BilanKapandji();
        $form = $this->createForm(BilanKapandjiType::class, $bilanKapandji, array(
        ));
        $form->add('submit', SubmitType::class, array(
            'label' => 'Créer',
            'attr'  => array('class' => 'btn btn-primary'),
        ));
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $resultats = $bilanKapandji->getResultats();
                $bilanKapandji->removeAllResultats();
                 
                if(!is_null($bilan)){
                    $bilanKapandji->setBilan($bilan);
                    $bilanKapandji->setNom($bilan->getNom()." : kapandji");
                }
                else{
                    $bilanKapandji->setPatient($patient);
                    $bilanKapandji->setNom($bilanKapandji->getDateAjout()->format('Y-m-d H:i:s'));
                }
                $em->persist($bilanKapandji);
                foreach ($resultats as $resultat ) {
                    $resultat->setBilan($bilanKapandji);
                    $bilanKapandji->addResultat($resultat);
                }
                $em->persist($bilanKapandji);
                $em->flush();


                return $this->render('App2Bundle:Bilans:get_one.html.twig', array(
                    'bilan' => $bilanKapandji,
                    'type' => 'Kapandji',
                    'componentResultat' => "bilanGesteTestResultat",
                    'paths' => ['extract'=>"app2_bilan_kapandji_extract", "modify"=>"app2_bilan_kapandji_update"]
                ));     
        
            }
        }

        return $this->render('App2Bundle:BilansKapandji:create_kapandji.html.twig', array(
            'form' => $form->createView(),
            'kapandjiPositions' => $kapandjiPositions,
            'heure' => new \Datetime()
        ));    
    }
    public function updateAction(Request $request, $idBilan)
    {
        $em = $this->getDoctrine()->getManager();
        $bilanKapandji = $em->getRepository(bilanKapandji::class)
        ->find($idBilan);
        $patient = $bilanKapandji->getPatient();
    
        $kapandjiPositions = $this->getDoctrine()
            ->getRepository(KapandjiPosition::class)
            ->findAll();
        
        $resultats = $bilanKapandji->getResultats();
        foreach ($kapandjiPositions as $kapandjiPosition) {
                $nom = $kapandjiPosition->getNom(); 
    
                $valueG = null;
                $valueD = null;
                $remarqueG = null;
                $remarqueD = null;
                //On garde la premiere condition qui correspond au geste test et au bon côté
                $i = 0;
                foreach ($resultats as  $resultat) {
                    if ($resultat->getItemTest()->getNom() == $nom && $resultat->getCote() == 0) {
                        $valueG = $resultat->getValue();
                        $remarqueG = $resultat->getRemarque();
                        $i = $i + 1;
                    }
                    if ($resultat->getItemTest()->getNom() == $nom && $resultat->getCote() == 1) {
                        $valueD = $resultat->getValue();
                        $remarqueD = $resultat->getRemarque();
                        $i = $i + 1;
                    }
                    if($i == 2 ){
                        break;
                    }
                }
                
                $kapandjiPositionWithValues = [
                    "nom"=>$nom,
                    "id"=>$kapandjiPosition->getId(),
                    "value_g"=>(is_null($valueG)) ? null : $valueG,
                    "value_d"=>(is_null($valueD)) ? null : $valueD,
                    "remarque_g"=>(is_null($remarqueG)) ? null : $remarqueG,
                    "remarque_d"=>(is_null($remarqueD)) ? null : $remarqueD
                ];
                $kapandjiPositionsWithValues[] = $kapandjiPositionWithValues;
        }
        $form = $this->createForm(BilanKapandjiType::class, $bilanKapandji, array(
        ));
        $form->add('submit', SubmitType::class, array(
            'label' => 'Créer',
            'attr'  => array('class' => 'btn btn-primary'),
        ));
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $resultats = $bilanKapandji->getResultats();
                $bilanKapandji->removeAllResultats();
                $em->persist($bilanKapandji);
                foreach ($resultats as $resultat ) {
                    $resultat->setBilan($bilanKapandji);
                    $bilanKapandji->addResultat($resultat);
                }
                $em->persist($bilanKapandji);
                $em->flush();


                return $this->render('App2Bundle:Bilans:get_one.html.twig', array(
                    'bilan' => $bilanKapandji,
                    'type' => 'Kapandji',
                    'componentResultat' => "bilanKapandjiResultat",
                    'paths' => ['extract'=>"app2_bilan_kapandji_extract", "modify"=>"app2_bilan_kapandji_update"]
                ));     
        
            }
        }

        return $this->render('App2Bundle:BilansKapandji:create_kapandji.html.twig', array(
            'form' => $form->createView(),
            'kapandjiPositions' => $kapandjiPositionsWithValues,
            'modify' => true
        ));    
    }
    public function getOneAction($idBilan)
    {        
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $bilanKapandji = $em
            ->getRepository(BilanKapandji::class)
            ->find($idBilan);
        

        return $this->render('App2Bundle:Bilans:get_one.html.twig', array(
                'bilan' => $bilanKapandji,
                'type' => 'Kapandji',
                'componentResultat' => "bilanKapandjiResultat",
                'paths' => ['extract'=>"app2_bilan_kapandji_extract", "modify"=>"app2_bilan_kapandji_update"]
            ));     
    }

    public function extractAction($idBilan)
    {        
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $bilanKapandji = $em
            ->getRepository(BilanKapandji::class)
            ->find($idBilan);

        $resultats = $bilanKapandji->getResultats();
        $affichage = [];
        foreach($resultats as $resultat){
            $affichage[]= [ 
                "item_nom"=> $resultat->getItemTest()->getNom(),
                "cote"=> ($resultat->getCote() == "0") ? "gauche" : "droite",
                "valeur"=> ($resultat->getValue() == true) ? "oui" : "non",
                "remarque"=> ($resultat->getRemarque()) ];
        }
        return $this->render('App2Bundle:BilansGesteTest:extract.html.twig', array(
                'bilan' => $bilanKapandji,
                'type' => "Kapandji",
                'text_btn_success' => "Afficher les positions réussies",
                'affichage'=> $affichage
            ));    
    }

    public function deleteAction($idBilan)
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $bilan = $em
            ->getRepository(BilanKapandji::class)
            ->find($idBilan);
        if (!$bilan) {
            throw $this->createNotFoundException('Pas de bilan trouvé');
        }

        $patient = $bilan->getPatient();
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($bilan);
        $em->flush();
    
        return $this->redirectToRoute('app2_get_one_patient', array('idPatient' => $patient->getId()));
    }

    public function compareAction(Request $request, $idPatient)
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $patient = $em->getRepository(Patient::class)
            ->find($idPatient);
        $bilansKapandji = $em
            ->getRepository(BilanKapandji::class)
            ->findBy(array('patient'=> $patient->getId()));
        $bilans = $em
        ->getRepository(Bilan::class)
        ->findBy(array('patient'=> $patient->getId()));

        $bilanGlobauxWithKapandji = array();
        foreach ($bilans as $bilan) {
            $test = $bilan->getBilanKapandji();
            if(!is_null($test)){
                array_push($bilanGlobauxWithKapandji,$test);
            }
        }

        // 21/05/20 pas besoin d'utiliser un classement pour l'affichage avec les checkbox mais pas le meilleur système non plus...
        $choices = array(
            "Bilan Globaux" => array(),
            "Autres" => array()
        );
        foreach ($bilansKapandji as $bilan) {
            $choices["Autres"] = array_merge($choices["Autres"],array($bilan->getNom()=> $bilan->getId()));
        }
        foreach ($bilanGlobauxWithKapandji as $bilan) {
                $choices["Bilan Globaux"] = array_merge($choices["Bilan Globaux"],array($bilan->getNom()=> $bilan->getId()));
        }
       
        $form = $this->createFormBuilder()
            ->add('bilans', ChoiceType::class, array(
                'choices'  => $choices,
                'expanded' => true,
                'multiple' => true,
                'label' => "Bilans à comparer"                ))
            ->add('compare', SubmitType::class, array('label' => 'Comparer'))
            ->getForm();
            
            if ($request->isMethod('POST')) {
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $data =$form->getData();
                    
                    $bilansToCompare = [];
                    $resultatsToCompare = [];
                    foreach ($data['bilans'] as $dataIdBilan) {
                        // print_r($dataIdBilan);
                        $bilansToCompare[] = $em->getRepository(BilanKapandji::class)
                        ->find($dataIdBilan);
                    }
                    foreach ($bilansToCompare as $bilan) {
                        $resultatsToCompare[] = $bilan->getResultats();
                    }
                    //On itère sur les résultats contenus dans le dernier bilan
                    $affichage = [];
                    
                    foreach(array_pop($resultatsToCompare) as $resultatDernierBilan){
                        $nomItem = $resultatDernierBilan->getItemTest()->getNom();
                        $cote = $resultatDernierBilan->getCote();
                        //Si le geste test n'est pas encore dans l'affichage on le rajoute
                        if(!array_key_exists($nomItem,$affichage)){
                            $affichage[$nomItem]=[];
                        }
                        //Si le cote n'est pas encore dans l'affichage on rajoutera la valeur du dernier bilan à la fin
                        if(!array_key_exists((int)$cote,$affichage[$nomItem])){
                            $affichage[$nomItem][$cote]=[];
                        }
                        //Le dernier bilan n'est plus dans tocompare car array_pop
                        foreach($resultatsToCompare as $resultats){
                            foreach($resultats as $resultat) {
                                if($resultat->getItemTest() == $resultatDernierBilan->getItemTest() && 
                                (int)$resultat->getCote() == (int)$resultatDernierBilan->getCote()){
                                    array_push($affichage[$nomItem][$cote],$resultat->getValue());
                                }
                            }
                        }
                        array_push($affichage[$nomItem][$cote], $resultatDernierBilan->getValue());

                    }
                    
                    return $this->render('App2Bundle:Bilans:compare_binary.html.twig', array(
                        'affichage' => $affichage,
                        'bilans' => $bilansToCompare,
                        'patient' => $patient,
                        'columns' => ['Position'],
                        'nomBilan' => "kapandji"
                    ));                       
                }
            }
        return $this->render('App2Bundle:BilansGesteTest:compare_choice.html.twig', array(
            'form' => $form->createView()
        ));   
          
    }
}
