<?php

namespace App2Bundle\Service;

use App2Bundle\Entity\Patients\Patient;
use App2Bundle\Entity\Bilans\Implication;
use App2Bundle\Entity\Bilans\Bilan;
use App2Bundle\Entity\Bilans\BilanGesteTest;
use App2Bundle\Entity\Bilans\BilanHabillage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class BilanFromImplicationGenerator
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function checkIfConditionOK($valueResultat, $operateur,$valueCondition)
    {
        //Comparaison en fonction de l'opérateur
        $check = false;
       if($operateur == "=" && $valueResultat == $valueCondition){
            $check = true;
       }
       elseif($operateur == ">" && intval($valueResultat) > intval($valueCondition)){
            $check = true;
       }
       elseif($operateur == "<" && intval($valueResultat) < intval($valueCondition)){
            $check = true;
        }
        return $check;
    }


    //On va chercher les bilans qui ont des implications FROM liées à ce bilan
    public function getBilansFrom($idPatient,$nomBilan)
    {
        $em = $this->entityManager;
        $patient = $em->getRepository(Patient::class)
            ->find($idPatient);
        $bilansFrom = $em->getRepository(Implication::class)
            ->getBilanFromForOneBilan($nomBilan);
        $listeBilan = [];
        //Pour chaque bilan From on ajoute le nom du bilan à listeBilan sauf si il est déjà présent ou que
        //ce bilan s'implique lui même
        foreach ($bilansFrom as $bilan) {
            if(!in_array($bilan["bilan"], $listeBilan) && $bilan["bilan"]!== $nomBilan){
                $listeBilan[] = $bilan["bilan"];
            }
        }
        //On retourne la liste des noms de bilans pour pouvoir construire le formulaire sous la forme ["musculaire","articulaire"]
        if(count($listeBilan)>0){
            return $listeBilan;

        }
        //On retourne false s'il n'y a pas de bilan qui implique ce bilan pour passer l'étape 
        else{
            return false;
        }
    }

    //On va chercher les implications qui s'appliquent à ce bilan
    public function getAllImplicationsFrom($bilanTo,$arrayIdFrom){
        $em = $this->entityManager;
        //je demande les implications de ce type de bilan 
        $implications = $em->getRepository(Implication::class)
            ->getAllImplicationsFrom($bilanTo);
        $bilansFrom = [];

        if(in_array("habillage",array_keys($arrayIdFrom))){
            //on ale bilan habillage complet (avec résultat) sur lequel on se base
            $bilansFrom["habillage"] = $em->getRepository(BilanHabillage::class)->findWithResultats($arrayIdFrom["habillage"]);
        }

        $implicationTo = [];
        //On parcourt les implications
        // echo $bilansFrom["habillage"];
        if(!empty($bilansFrom["habillage"])){
            foreach ($implications as $implication) {
                
                //Valeurs initialisée à true passe à fausse si une condition n'est pas remplie
                $is_validated_G = true;
                $is_validated_D = true;
                //On parcourt les conditions from
                foreach ($implication->getConditionsBilanFrom() as $conditionFrom) {

                    //on teste chacune des condition froms si l'une des condtion n'est pas rempli alors is_validated = false
                    //si is_validated = true à la fin on ajoute la condition to à l'array
                    //SI la condition s'appuie sur un bilan non fourni on passe à false
                    if(in_array($conditionFrom->getBilan(), array_keys($arrayIdFrom))){
                        //On à les résultats du bilan
                        $resultats = $bilansFrom[$conditionFrom->getBilan()][0]->getResultats()->toArray();


                        //On ne garde que les deux résultats qui  nous intéresse droite et gauche pour le nom et l'articulation si spécifiée
                        $resultatGD = array_filter($resultats, function($v) use($conditionFrom){
                            if($conditionFrom->getArticulation() == null){
                                return $v->getItemTest()->getNom() == $conditionFrom->getNom();
                            }
                            else{
                                return $v->getItemTest()->getNom() == $conditionFrom->getNom() && $v->getItemTest()->getArticulation() == $conditionFrom->getArticulation() ;
                            }
                        });
                        //On ne garde que le résultat Gauche
                        $resultatG = array_values(array_filter($resultatGD, function($v) {
                            return $v->getCote() == 0;
                        }));
                        //On ne garde que le résultat Droit
                        $resultatD = array_values(array_filter($resultatGD, function($v) {
                            return $v->getCote() == 1;
                        }));
                    //Si il n'y a pas résultat (item ayant été supprimé toujours en condition en BDD)
                    if(count($resultatG)==0){ 
                        $is_validated_G = false;
                        }
                    else{
                        $resultatG=$resultatG[0];
                        // si la condition n'est pas validé on passe en is_validated = false
                        if(!self::checkIfConditionOK($resultatG->getValue(), $conditionFrom->getOperateur(),$conditionFrom->getValue())){
                            $is_validated_G = false;
                        } 
                    }
                    //Si il n'y a pas résultat (item ayant été supprimé toujours en condition en BDD)
                    if(count($resultatD)==0){ 
                            $is_validated_D = false;
                        }
                        else{
                            $resultatD=$resultatD[0];                    
                            if(!self::checkIfConditionOK($resultatD->getValue(), $conditionFrom->getOperateur(),$conditionFrom->getValue())){
                                $is_validated_D = false;
                            } 
                        }

                    }
                    else{
                        $is_validated_G = false;
                        $is_validated_D = false;
                    }

                }
                if( $is_validated_G == true ){
                    foreach ($implication->getConditionsBilanTo() as $value) {
                        if($value->getBilan() == $bilanTo ){
                            array_push($implicationTo,array("nom"=> $value->getNom(), "articulation"=> (is_null($value->getArticulation())) ? null : $value->getArticulation() , "cote"=> 0,"operateur"=> $value->getOperateur(),"value"=> $value->getValue()));
                        }
                    }  
                }
                //On gère le cas d'un binaire, comme sur le bilan geste test dans ce cas la on change 0->1 ou 1->0
                elseif($is_validated_G == false && $bilanTo == "geste-test" ) {
                    foreach ($implication->getConditionsBilanTo() as $value) {
                        if($value->getBilan() == $bilanTo && $value->getOperateur() == "=" ){
                            array_push($implicationTo,array("nom"=> $value->getNom(), "articulation"=> (is_null($value->getArticulation())) ? null : $value->getArticulation(), "cote"=> 0,"operateur"=> $value->getOperateur(),"value"=> intval(!boolval($value->getValue())) ));
                        }
                    }  
                }

                if( $is_validated_D == true ){
                    foreach ($implication->getConditionsBilanTo() as $value) {
                        if($value->getBilan() == $bilanTo ){
                            array_push($implicationTo,array("nom"=> $value->getNom(), "articulation"=> (is_null($value->getArticulation())) ? null : $value->getArticulation(), "cote"=> 1,"operateur"=> $value->getOperateur(),"value"=> $value->getValue()));
                        }
                    }  
                }
                elseif($is_validated_D == false && $bilanTo == "geste-test") {
                    foreach ($implication->getConditionsBilanTo() as $value) {
                        if($value->getBilan() == $bilanTo && $value->getOperateur() == "=" ){
                            array_push($implicationTo,array("nom"=> $value->getNom(), "articulation"=> (is_null($value->getArticulation())) ? null : $value->getArticulation(), "cote"=> 1,"operateur"=> $value->getOperateur(),"value"=> intval(!boolval($value->getValue())) ));
                        }
                    }  
                }
                

            }
        }


        return $implicationTo;
       
    }

    //Lors de l'action create cette fonction récupère dans l'url les bilans from à lier avec ce bilan
    public function handleQuery($query)
    {
        $arrayIdBilan = [];
        //On parcourt la query en regardant les variables qui commence par "bilan_"
        foreach ($query as $key => $value){
            if (stripos($key, 'bilan_') !== false && $value > 0) {
                //On ajoute l'id dans l'array avec comme key le nom du bilan: nom de la variable moins les 6 premiers caractères
                $arrayIdBilan[substr($key, 6)] = $value;
            }
        } 
        return $arrayIdBilan;
    }

    public function giveOptionsToCreateController($idPatient,$data)
    {
       $options = null;
        
    }
}
