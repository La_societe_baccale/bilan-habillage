var Encore = require('@symfony/webpack-encore');

Encore
    // directory where compiled assets will be stored
    .setOutputPath('web/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/web/build/')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
     */
    .addEntry('app', './assets/js/app.js')
    // .addEntry('extract-geste-test', './assets/js/extract-geste-test.js')
    .addEntry('one-patient', './assets/js/one-patient.js')
    .addEntry('bilan-global', './assets/js/bilan-global.js')
    .addEntry('new-bilan-custom', './assets/js/new-bilan-custom.js')
    .addEntry('create-bilan-custom', './assets/js/create-bilan-custom.js')
    .addEntry('create-bilan-conseil', './assets/js/create-bilan-conseil.js')
    .addEntry('get-one-bilan-custom', './assets/js/get-one-bilan-custom.js')
    .addEntry('compare-bilan-custom', './assets/js/compare-bilan-custom.js')
    .addEntry('prepare-extract', './assets/js/prepare-extract.js')


    //.addEntry('page1', './assets/js/page1.js')
    //.addEntry('page2', './assets/js/page2.js')
    // .addStyleEntry('css/app', './assets/css/app.scss')

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you use Sass/SCSS files
    .enableSassLoader()

    // uncomment if you're having problems with a jQuery plugin
    .autoProvideVariables({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
    })
;

module.exports = Encore.getWebpackConfig();