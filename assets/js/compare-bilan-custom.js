jQuery(document).ready(function() {
    $(".js-item").each(function( index ) {
        fullFillItem($( this ))
      });     
    calculateAllSubtotals()
    calculateEvolution()
})
function calculateEvolution(){
    $('.js-evolution').each(function(){
        let siblings = $(this).siblings(".js-to-compare")

        let valueStart = siblings.first().data("value")
        let valueEnd = siblings.last().data("value")
        let change = parseInt(valueEnd)-parseInt(valueStart)
        if(change !== 0){
            if(change>0){
                $(this).css("color","green");
                $(this).html('+'+change)

            }
            else{
                $(this).css("color","red");
                $(this).html(change)
            }
        }
    })
}
function calculateAllSubtotals(){
    var previous = null;
    var subtotalElements = $('.js-subtotal').sort(function (a, b) {
        var idBilanA =parseInt( $(a).data('id-bilan'));
        var idBilanB =parseInt( $(b).data('id-bilan'));
        var ordreA =parseInt( $(a).data('ordre'));
        var ordreB =parseInt( $(b).data('ordre'));
        if(idBilanA > idBilanB){
            return 1
        }
        else if (idBilanA < idBilanB){
            return - 1
        }
        else{
            return (ordreA < ordreB) ? -1 : (ordreA > ordreB) ? 1 : 0;
        }
     });
     subtotalElements.each(function(  ) {
        let lastSubtotalOrdre = -1
        if (previous) {
            if(previous.data("id-bilan") == $(this).data("id-bilan")){
                lastSubtotalOrdre = previous.data("ordre")
            }
            else{
                lastSubtotalOrdre = -1
            }
        }
        if($("#result").data('with-cote') == 1){
            calculateSubtotalWithCote($( this ), lastSubtotalOrdre)
            if($(this).data("cote")=="1"){
                previous = $( this )
            }
        }
        else{
            calculateSubtotal($( this ), lastSubtotalOrdre)
            previous = $( this )
        }
      });
}
function calculateSubtotal($element, lastSubtotalOrdre) {
    var sum = 0;
    var total = 0;
    $(".js-resultat").each(function(){
        if($element.data("id-bilan")== $(this).data("id-bilan")){
            if($element.data("is-full-total") == 1){
                sum += +$(this).data("value");
            }
            else{
                if($( this ).data("ordre") > lastSubtotalOrdre && $( this ).data("ordre") < $element.data("ordre")){
                    sum += +$(this).data("value");
                } 
            }
        }
    });
    $(".js-item").each(function(){
        if($element.data("id-bilan")== $(this).data("id-bilan")){
            if($element.data("is-full-total") == 1){
                total += +$(this).data("max"); 
            }
            else{
                if($( this ).data("ordre") > lastSubtotalOrdre && $( this ).data("ordre") < $element.data("ordre")){
                    total += +$(this).data("max");
                } 
            }
        }
    });
    $element.html(sum+"/"+total)
    $element.attr( "data-value", sum);
}

function calculateSubtotalWithCote($element, lastSubtotalOrdre) {
    var sum = 0;
    var total = 0;
    if($element.data("id-bilan")== 45 && $element.data("ordre")==4 ){
        console.log(lastSubtotalOrdre)
    }
    $(".js-resultat").each(function(){
        if($element.data("id-bilan")== $(this).data("id-bilan")){
            if($element.data("is-full-total") == 1 ){
                if($element.data("cote")==  $(this).data("cote") ){
                    sum += +$(this).data("value");
                }
            }
            else{
                if($( this ).data("ordre") > lastSubtotalOrdre && $( this ).data("ordre") < $element.data("ordre")){
                    if($element.data("cote")==  $(this).data("cote") ){
                        sum += +$(this).data("value");
                    }
                } 
            }
        }
    });
    $(".js-item").each(function(){
        if($element.data("id-bilan")== $(this).data("id-bilan")){
            if($element.data("is-full-total") == 1 ){
                if($element.data("cote")==  $(this).data("cote") ){
                    total += +$(this).data("max");
                }
            }
            else{
                if($( this ).data("ordre") > lastSubtotalOrdre && $( this ).data("ordre") < $element.data("ordre")){
                    if($element.data("cote")==  $(this).data("cote") ){
                        total += +$(this).data("max");
                    }
                } 
            }
        }
    });
    $element.html(sum+"/"+total)
    $element.attr( "data-value", sum);
}

function fullFillItem($element){
    let result = null;
    if($element.data("cote")==null){
        result = $( "[class='js-resultat'][data-id-item='"+$element.data("id-item")+"'][data-id-bilan='"+$element.data("id-bilan")+"']" );
    }
    else{
        result = $( "[class='js-resultat'][data-id-item='"+$element.data("id-item")+"'][data-id-bilan='"+$element.data("id-bilan")+"'][data-cote='"+$element.data("cote")+"']" );
    }
    $element.html(result.data("value")+"/"+$element.data("max"))
    $element.attr( "data-value", result.data("value"));
}