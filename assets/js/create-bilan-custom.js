jQuery(document).ready(function() {
    $(".js-item").each(function( index ) {
        addItemForm($( this ))
      });
      calculateAllSubtotals()
      $('input').change(function() {
        calculateAllSubtotals()
    });
    $(".resultat-item-test").hide()
    $(".resultat-cote").hide()
})
function calculateAllSubtotals(){
    var previous = null;
    $(".js-subtotal").each(function(  ) {
        let lastSubtotalOrdre = -1
        if (previous) {
            lastSubtotalOrdre = previous.data("ordre")
        }
        if($(".container-form").data('with-cote') == 1){
            calculateSubtotalWithCote($( this ), lastSubtotalOrdre)
            if($(this).data("cote")=="droite"){
                previous = $( this )
            }
        }
        else{
            calculateSubtotal($( this ), lastSubtotalOrdre)
            previous = $( this )
        }
      });
}

function calculateSubtotal($element, lastSubtotalOrdre) {
    var sum = 0;
    var total = 0;
    $(".js-item").each(function(){
        if($element.data("is-full-total") == 1){
            sum += +$(this).find("input").val();
            total += +$(this).find("input").attr('max'); 
        }
        else{
            if($( this ).data("ordre") > lastSubtotalOrdre && $( this ).data("ordre") < $element.data("ordre")){
                sum += +$(this).find("input").val();
                total += +$(this).find("input").attr('max');
            } 
        }
        
    });
    $element.html(sum+"/"+total)
}
function calculateSubtotalWithCote($element, lastSubtotalOrdre) {
    var sum = 0;
    var total = 0;
    $(".js-item").each(function(){
            if($element.data("is-full-total") == 1 ){
                if($element.data("cote")=="gauche"){
                    sum += +$(this).find(".js-item-gauche").find("input").val();
                    total += +$(this).find(".js-item-gauche").find("input").attr('max'); 
                }
                else{
                    sum += +$(this).find(".js-item-droite").find("input").val();
                    total += +$(this).find(".js-item-droite").find("input").attr('max'); 
                }
            }
            else{
                if($( this ).data("ordre") > lastSubtotalOrdre && $( this ).data("ordre") < $element.data("ordre")){
                    if($element.data("cote")=="gauche"){
                        sum += +$(this).find(".js-item-gauche").find("input").val();
                        total += +$(this).find(".js-item-gauche").find("input").attr('max'); 
                    }
                    else{
                        sum += +$(this).find(".js-item-droite").find("input").val();
                        total += +$(this).find(".js-item-droite").find("input").attr('max'); 
                    }
                } 
            }
    });
    $element.html(sum+"/"+total)
}

function addItemForm($element) {
    var prototype = $(".container-form").data('prototype');
    var newForm = prototype;
    let formtype = "text";
    if($element.data('type-value')== "integer"){
        formtype = "number";
    }

    let input = null;
    let selectItem = null;

    if($(".container-form").data('with-cote') == 1){
        let newForm2 = newForm.valueOf();
        //Si on a un item pour la droite et pour la gauche on multiplie par 2 l'ordre à gauche
        // et on multiplie par 2 +1 pour la droite comme ca chaque input à le bon 
         newForm = newForm.replace(/__name__/g, parseInt($element.data('ordre'))*2 );
         newForm2 = newForm2.replace(/__name__/g, parseInt($element.data('ordre'))*2+1 );
         $("#js-item-"+$element.data('ordre')+"-gauche").append(newForm)
         $("#js-item-"+$element.data('ordre')+"-droite").append(newForm2)
         input = $("#js-item-"+$element.data('ordre')).find('input')
         selectItem =  $("#js-item-"+$element.data('ordre')).find('select.resultat-item-test')
         $("#js-item-"+$element.data('ordre')).find('select.resultat-cote').first().val(0)
         $("#js-item-"+$element.data('ordre')).find('select.resultat-cote').last().val(1)
    }
    else{
        newForm = newForm.replace(/__name__/g, $element.data('ordre') );
        $("#js-item-"+$element.data('ordre')+"-value").append(newForm)
        selectItem =  $("#js-item-"+$element.data('ordre')).find('select')
        input = $("#js-item-"+$element.data('ordre')).find('input')
    }
    selectItem.val($element.data('id'))
    input.attr('type',formtype)
    input.attr('max',$element.data('max'))
    input.attr('min',$element.data('min'))
    if($("#old-result").length>0){
        //On est dans le cas de l'update du coup on va chercher le résultat précédent
        if($("#old-result").data("with-cote")== "1"){
            input.each(function(){
                console.log($(this).closest("td").data("cote"))
                $(this).val($( "[class='js-resultat'][data-id-item='"+$element.data("id")+"'][data-cote='"+$(this).closest("td").data("cote") +"']" ).data("value"))
            })
        }
        else{
            input.val($( "[class='js-resultat'][data-id-item='"+$element.data("id")+"']" ).data("value"))
        }
    }
    else{
        input.val($element.data('max'))
    }
}
