jQuery(document).ready(function() {
    $(".js-item").each(function( index ) {
        fullFillItem($( this ))
      });
      calculateAllSubtotals()
     
})
 
function calculateAllSubtotals(){
    var previous = null;
    $(".js-subtotal").each(function(  ) {
        let lastSubtotalOrdre = -1
        if (previous) {
            lastSubtotalOrdre = previous.data("ordre")
        }
        if($("#result").data('with-cote') == 1){
            calculateSubtotalWithCote($( this ), lastSubtotalOrdre)
            if($(this).data("cote")=="1"){
                previous = $( this )
            }
        }
        else{
            calculateSubtotal($( this ), lastSubtotalOrdre)
            previous = $( this )
        }
      });
}
function calculateSubtotal($element, lastSubtotalOrdre) {
    var sum = 0;
    var total = 0;
    $(".js-resultat").each(function(){
        console.log($( this ).data("ordre"))
        console.log(lastSubtotalOrdre)
        console.log($element.data("ordre"))


        if($element.data("is-full-total") == 1){
            sum += +$(this).data("value");
        }
        else{
            if($( this ).data("ordre") > lastSubtotalOrdre && $( this ).data("ordre") < $element.data("ordre")){
                sum += +$(this).data("value");
            } 
        }
    });
    $(".js-item").each(function(){
        if($element.data("is-full-total") == 1){
            total += +$(this).data("max"); 
        }
        else{
            if($( this ).data("ordre") > lastSubtotalOrdre && $( this ).data("ordre") < $element.data("ordre")){
                total += +$(this).data("max");
            } 
        }
    });
    $element.html(sum+"/"+total)
}

function calculateSubtotalWithCote($element, lastSubtotalOrdre) {
    var sum = 0;
    var total = 0;
    
    $(".js-resultat").each(function(){
            if($element.data("is-full-total") == 1 ){
                if($element.data("cote")==  $(this).data("cote") ){
                    sum += +$(this).data("value");
                }
            }
            else{
                if($( this ).data("ordre") > lastSubtotalOrdre && $( this ).data("ordre") < $element.data("ordre")){
                    if($element.data("cote")==  $(this).data("cote") ){
                        sum += +$(this).data("value");
                    }
                } 
            }
    });
    $(".js-item").each(function(){
        if($element.data("is-full-total") == 1 ){
            if($element.data("cote")==  $(this).data("cote") ){
                total += +$(this).data("max");
            }
        }
        else{
            if($( this ).data("ordre") > lastSubtotalOrdre && $( this ).data("ordre") < $element.data("ordre")){
                if($element.data("cote")==  $(this).data("cote") ){
                    total += +$(this).data("max");
                }
            } 
        }
    });
    $element.html(sum+"/"+total)
}

function fullFillItem($element){
    let result = null;
    if($element.data("cote")==null){
        result = $( "[class='js-resultat'][data-id-item='"+$element.data("id-item")+"']" );
    }
    else{
        result = $( "[class='js-resultat'][data-id-item='"+$element.data("id-item")+"'][data-cote='"+$element.data("cote")+"']" );
    }
    $element.html(result.data("value")+"/"+$element.data("max"))
}