jQuery(document).ready(function() {
    $('input.form_to_add').hide()
    $('tr.line-input').each(function( index ) {
        handleLine($(this))       
      });
})

function handleLine($element) {
  let inputLineList = $element.find("input[data-bilan='"+$element.data('bilan')+"']")
  inputLineList.each(function( index ) {
    $(this).change(function() {
      let classInput = $(this).closest("tr").data("class-input")
      if(this.checked) {
        inputLineList.not($(this)).prop('checked', false);
        $("."+classInput).val($(this).val())
      }
      else{
        $("."+classInput).val(0)
      }
    });     
  });
}
