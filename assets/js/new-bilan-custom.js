var $collectionHolder;
// setup an "add a item" link
var $addItemButton = $('<button type="button" class="btn btn-secondary add_item_link">Ajouter un item à tester</button>');
var $addSubTotalButton = $('<button type="button" class="btn btn-secondary add_subtotal_link">Ajouter un sous-total</button>');
var $addTotalButton = $('<button type="button" class="btn btn-secondary add_total_link">Ajouter un total général</button>');
var $addSubPartButton = $('<button type="button" class="btn btn-secondary add_subtotal_link">Ajouter une sous-partie</button>');
var $addSubTextButton = $('<button type="button" class="btn btn-secondary add_subtotal_link">Ajouter un texte</button>');
var $deleteItemButton = $('<button type="button" class="btn btn-danger delete_item">Supprimer le dernier item</button>');


jQuery(document).ready(function() {
    $collectionHolder = $('ul.item');
    // add the "add a item" anchor and li to the item ul
    $collectionHolder.after($addItemButton);
    $collectionHolder.after($addSubPartButton);
    $collectionHolder.after($addSubTextButton);

    // count the current form inputs we have, use that as the new
    // index when inserting a new item 
    $collectionHolder.data('index', $collectionHolder.find('.item-subtotal-box').length);

    $addItemButton.on('click', function(e) {
        // add a new item form (see next code block)
        addItemForm($collectionHolder);
    });
    $addSubPartButton.on('click', function(e) {
        // add a new item form (see next code block)
        addSubPartForm($collectionHolder);
    });
    $addSubTextButton.on('click', function(e) {
        // add a new item form (see next code block)
        addSubTextForm($collectionHolder);
    });

    $formRowFormatValues = $('select.format-values').parent();
    $formRowMin = $('input#app2bundle_bilan_custom_skeleton_min').parent();
    $formRowMax = $('input#app2bundle_bilan_custom_skeleton_max').parent();

    checkDisplayAndFillValues($formRowMin,$formRowMax,$formRowFormatValues)
    //Lorsque l'on change "Les réponses des items testés sont-elle toutes du même format"
    $("select#app2bundle_bilan_custom_skeleton_type_values").change(function() {
        checkDisplayAndFillValues($formRowMin,$formRowMax,$formRowFormatValues)
    });
    $('#app2bundle_bilan_custom_skeleton_format_values').change(function() {
        checkDisplayAndFillValues($formRowMin,$formRowMax,$formRowFormatValues)
    });
    $('input.min-max-all-items').change(function() {
        checkDisplayAndFillValues($formRowMin,$formRowMax,$formRowFormatValues)
    });
});
function checkDisplayAndFillValues($formRowMin,$formRowMax,$formRowFormatValues){
    $addSubTotalButton.remove()
    $addTotalButton.remove()
    if($("select#app2bundle_bilan_custom_skeleton_type_values").val() == 1) {
        $formRowFormatValues.show()
        $('input.min-max-item').parent().hide()
        if($('#app2bundle_bilan_custom_skeleton_format_values').val() == "integer"){
            $formRowMin.show()
            $formRowMax.show()
            $("input.min-item").val($("input.min-all-items").val())
            $("input.max-item").val($("input.max-all-items").val())
            $collectionHolder.after($addSubTotalButton);
            $collectionHolder.after($addTotalButton);
            $addSubTotalButton.on('click', function(e) {
                addSubTotalForm($collectionHolder);
            });
            $addTotalButton.on('click', function(e) {
                addTotalForm($collectionHolder);
            });
        }
        else{
            $formRowMin.hide()
            $formRowMax.hide()
        }
        $('select.format-value-item').val($('select.format-values').val())
        //on cache chaque format pour chaque question
        $('select.format-value-item').parent().hide()
        $('input.min-max-item').parent().hide()
    }
    else{
        $formRowFormatValues.hide()
        $formRowMin.hide()
        $formRowMax.hide()
        $('select.format-value-item').each(function( index ) {
            $( this ).parent().show()
            if( $( this ).val() == "integer"){
                console.log('integer')
                $( this ).parent().next().show().next().show()
            }
            else{
                console.log('pas integer')

                $( this ).parent().next().hide().next().hide()
            }
          });
        
    }
}
function addItemForm($collectionHolder) {
    // // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // // get the new index
    var index = $collectionHolder.data('index');

    var newForm = prototype;
    if(index == 0){
        $('button.add_item_link').after($deleteItemButton)
        $deleteItemButton.on('click', function(e) {
            // add a new item form (see next code block)
            deleteItemForm($collectionHolder);
        });
    }
    // // Replace '__name__' in the prototype's HTML to
    // // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);
    // // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);
    // // Display the form 
    $collectionHolder.append(newForm);
    checkDisplayAndFillValues($formRowMin,$formRowMax,$formRowFormatValues)
    $('select.format-value-item').change(function() {
        checkDisplayAndFillValues($formRowMin,$formRowMax,$formRowFormatValues)
    });
    $("input#app2bundle_bilan_custom_skeleton_itemTests_"+index+"_ordre").val(index)
}
function addSubTotalForm($collectionHolder) {
    // // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype-subtotal');

    // // get the new index
    var index = $collectionHolder.data('index');
    var newForm = prototype;
    if(index == 0){
        $('button.add_item_link').after($deleteItemButton)
        $deleteItemButton.on('click', function(e) {
            // add a new item form (see next code block)
            deleteItemForm($collectionHolder);
        });
    }
    // // Replace '__name__' in the prototype's HTML to
    // // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);
    // // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);
    // // Display the form 
    $collectionHolder.append(newForm);
    checkDisplayAndFillValues($formRowMin,$formRowMax,$formRowFormatValues)
    
    $("input#app2bundle_bilan_custom_skeleton_subtotals_"+index+"_ordre").val(index)
    $("input#app2bundle_bilan_custom_skeleton_subtotals_"+index+"_isFullTotal").val(0)

}
function addSubPartForm($collectionHolder) {
    // // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype-subpart');

    // // get the new index
    var index = $collectionHolder.data('index');
    var newForm = prototype;
    if(index == 0){
        $('button.add_item_link').after($deleteItemButton)
        $deleteItemButton.on('click', function(e) {
            // add a new item form (see next code block)
            deleteItemForm($collectionHolder);
        });
    }
    // // Replace '__name__' in the prototype's HTML to
    // // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);
    // // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);
    // // Display the form 
    $collectionHolder.append(newForm);
    checkDisplayAndFillValues($formRowMin,$formRowMax,$formRowFormatValues)
    
    $("input#app2bundle_bilan_custom_skeleton_subparts_"+index+"_ordre").val(index)
}
function addSubTextForm($collectionHolder) {
    // // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype-subtext');

    // // get the new index
    var index = $collectionHolder.data('index');
    var newForm = prototype;
    if(index == 0){
        $('button.add_item_link').after($deleteItemButton)
        $deleteItemButton.on('click', function(e) {
            // add a new item form (see next code block)
            deleteItemForm($collectionHolder);
        });
    }
    // // Replace '__name__' in the prototype's HTML to
    // // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);
    // // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);
    // // Display the form 
    $collectionHolder.append(newForm);
    checkDisplayAndFillValues($formRowMin,$formRowMax,$formRowFormatValues)
    
    $("input#app2bundle_bilan_custom_skeleton_subtexts_"+index+"_ordre").val(index)
}
function addTotalForm($collectionHolder) {
    // // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype-subtotal');

    // // get the new index
    var index = $collectionHolder.data('index');
    var newForm = prototype;
    if(index == 0){
        $('button.add_item_link').after($deleteItemButton)
        $deleteItemButton.on('click', function(e) {
            // add a new item form (see next code block)
            deleteItemForm($collectionHolder);
        });
    }
    // // Replace '__name__' in the prototype's HTML to
    // // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);
    // // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);
    // // Display the form 
    $collectionHolder.append(newForm);
    checkDisplayAndFillValues($formRowMin,$formRowMax,$formRowFormatValues)
    
    $("input#app2bundle_bilan_custom_skeleton_subtotals_"+index+"_ordre").val(index)
    $("input#app2bundle_bilan_custom_skeleton_subtotals_"+index+"_isFullTotal").val(1)

}
function deleteItemForm($collectionHolder) {
    $collectionHolder.children().last().remove()  
    var index = $collectionHolder.data('index');
    if(index == 1){
        $deleteItemButton.remove()
    }
    $collectionHolder.data('index', index - 1);
}