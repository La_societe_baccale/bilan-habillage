// assets/js/app.js

require('../css/app.scss');

console.log('Hello Webpack Encore');


// app.js

const $ = require('jquery');
// this "modifies" the jquery module: adding behavior to it
// the bootstrap module doesn't export/return anything
require('bootstrap');

// or you can include specific pieces
// require('bootstrap/js/dist/tooltip');
// require('bootstrap/js/dist/popover');

$(document).ready(function() {
    $('[data-toggle="popover"]').popover({
        html: true,
        trigger: 'click',
        placement: 'bottom',
        content: function () { 
            if ($(this).data('img') !== undefined) {
                return '<img class="width-all" src="' + $(this).data('img') + '" />';
            }
            else{
                return $(this).data('content');
            }
             
        }
      });
});
